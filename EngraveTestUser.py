#! /usr/bin/env python3

import sys
import codecs
import re
import xml.etree.ElementTree as ET

if len( sys.argv ) != 3 :
    sys.exit()    

if sys.argv[1].strip() :
    filePath = "./src/net/passone/edubank/common/CUser.java"
    with codecs.open( filePath, "r", "utf-8" ) as file :
        content = file.read()

    content = re.sub( r'userid\s*=\s*"\w*"', 'userid="%s"' % sys.argv[1], content )

    with codecs.open( filePath, "w", "utf-8" ) as file :
        content = file.write( content )


if sys.argv[2].strip() :
    filePath = "./res/layout/login.xml"
    namespaceUrl = 'http://schemas.android.com/apk/res/android'
    tree = ET.register_namespace( 'android', namespaceUrl )
    tree = ET.parse( filePath )
    root = tree.getroot()
    elems = root.findall( './/EditText/[@android:id="@+id/edit_pwd"]',
                          namespaces=dict(android=namespaceUrl) )
    for elem in elems :
        elem.set( "android:text", sys.argv[2] )
    tree.write( filePath )
