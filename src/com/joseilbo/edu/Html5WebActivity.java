package com.joseilbo.edu;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.HTML5WebView;
import com.joseilbo.edu.common.Util;

public class Html5WebActivity extends XActivity {
	Cursor cursor;
	String uid, _url;
	HTML5WebView mWebView;
	private final Handler handler = new Handler();
	ImageButton btn_write, btn_help, btn_list;
	AlertDialog alert;
	private Map<String, Object> map = new HashMap<String, Object>();
	protected static WaitDialog progressDialog;
	Html5WebActivity self = null;
	String menu;
	boolean isTab = false;
	Activity activity;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self = this;
		activity = self;
		Bundle bun = getIntent().getExtras();
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		_url = bun.getString("url");
		mWebView = new HTML5WebView(this);
		
		if (savedInstanceState != null) {
			mWebView.restoreState(savedInstanceState);
		} else {
			mWebView.loadUrl(_url);
		}
		setContentView(mWebView.getLayout());

		initialize();
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.leftImageButton:
			onBackPressed();
			break;
		case R.id.rightImageButton:
			mWebView.reload();
			break;
		}
		return;
	}

	public void initWebview() {
		loadWebView(_url);
	}

	public void initialize() {
	}

	public void putAll(Map<? extends String, ? extends Object> arg0) {
		map.putAll(arg0);
	}

	private void loadWebView(String url) {
		mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.addJavascriptInterface(new AndroidBridge(), "android");
		mWebView.setHorizontalScrollBarEnabled(true);
		mWebView.setVerticalScrollBarEnabled(false);
		mWebView.getSettings().setBuiltInZoomControls(false);
		mWebView.setWebViewClient(new webViewClient()); 
		mWebView.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onJsConfirm(WebView view, String url, String message, final android.webkit.JsResult result) {
				new AlertDialog.Builder(activity)
						.setTitle(getText(R.string.app_name))
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										result.confirm();
									}
								})
						.setNegativeButton(android.R.string.cancel,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										result.cancel();
									}
								}).create().show();

				return true;
			};

			public boolean onJsAlert(WebView view, String url, final String message, JsResult result) {
				final JsResult r = result;
				if (null == self)
					return false;

				new AlertDialog.Builder(activity)
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										if (message.contains("로그인") || message.contains("Session")) {
											mWebView.stopLoading();
											logout();
										}
										r.confirm();
									}
								}).setCancelable(false).create().show();
				return true;
			}

			public void onProgressChanged(WebView view, int newProgress) {
			}
		});
		mWebView.setDownloadListener(new DownloadListener() {
			public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
				startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url)));
			}
		});

		mWebView.loadUrl(url);
	}

	private class webViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.startsWith("tel:")) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
			} else {
				_url = url;
				view.loadUrl(url);
			}
			return true;
		}

		public void onLoadResource(WebView view, String url) {
		}

		public void onPageFinished(WebView view, String url) {
			try {
				if (url.endsWith(".mp4")) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					Uri uri = Uri.parse(url);
					i.setDataAndType(uri, "video/mp4");
					startActivity(i);
				}
			} catch (Exception e) {
			}
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Util.ToastMessage(activity, getResources().getString(R.string.api_http_alert));
		}
	}

	public void onStart() {
		super.onStart();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		onBackPressed();
		super.onStop();
	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();
		
		super.onResume();
	}

	private class AndroidBridge {
		@JavascriptInterface
		public void callAndroid(final String arg) {
			handler.post(new Runnable() {
				public void run() {
				}
			});
		}
	}

	@Override
	public void onBackPressed() {
		mWebView.loadData("", "text/html", "utf-8");
		finish();
		return;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	public void goPlayVideo(String url, String title) {
		
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mWebView.loadData("", "text/html", "utf-8");
	}
}
