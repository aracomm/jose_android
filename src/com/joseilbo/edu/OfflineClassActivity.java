package com.joseilbo.edu;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Preference;
import com.joseilbo.edu.common.Util;

public class OfflineClassActivity extends XActivity {
	Cursor cursor;
	String uid, _url;
	WebView mWebView;
	private final Handler handler = new Handler();
	ImageButton btn_write, btn_help, btn_list;
	AlertDialog alert;
	private Map<String, Object> map = new HashMap<String, Object>();
	protected static WaitDialog progressDialog;
	OfflineClassActivity self = null;
	String menu;

	protected static int dialogCount = 0;
	private String payUrl = "";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self = this;
		setContentView(R.layout.titlewebview2);

		setTitleButton((ImageButton) findViewById(R.id.leftImageButton), R.drawable.btn_home_on2);
		// setTitleButton((ImageButton)findViewById(R.id.rightSearchImageButton),R.drawable.btn_search_white);

		setTitle("오프라인 교육");
		
		Preference pref = new Preference(this, Util.MT_PREFS);
		_url = pref.read("direct_url", "");
//		_url = getIntent().getStringExtra("url");
		if (_url == null || _url.equals("")) {
			_url = Constants.OfflineClassUrl + "?mem_id=" + CUser.userid;
		} else {
			pref.write("direct_url", "");
		}

		initWebview();
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.leftImageButton:
			//이전기능을 같이 사용하는 경우. - by jjun72. 2015/12/04
			//onBackPressed();
			
			//무조건 홈으로 가도록. 이전기능은 시스템의 onBackPressed()로 사용하는 경우. - by jjun72. 2015/12/04
			((Activity) getParent()).startActivity(new Intent(self, MainActivity.class));
			finish();
			break;
		case R.id.rightSearchImageButton:
			String _searchUrl = Constants.CourseSearchUrl;
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra("url", _searchUrl));
			break;
		}
	}

	public void initWebview() {
		LinearLayout rootLayout = (LinearLayout) findViewById(R.id.root_layout);
		mWebView = new WebView(getParent());
		rootLayout.addView(mWebView);
		
		loadWebView(_url);
	}

	public void putAll(Map<? extends String, ? extends Object> arg0) {
		map.putAll(arg0);
	}

	private void loadWebView(String url) {
		
		mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		mWebView.getSettings().setJavaScriptEnabled(true); 
		mWebView.addJavascriptInterface(new AndroidBridge(), "android");
		mWebView.setHorizontalScrollBarEnabled(true); // 세로 scroll 제거
		mWebView.setVerticalScrollBarEnabled(false); // 가로 scroll 제거
		mWebView.getSettings().setBuiltInZoomControls(false);
		mWebView.setWebViewClient(new webViewClient()); 
		mWebView.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onJsConfirm(WebView view, String url,
					String message, final android.webkit.JsResult result) {
				new AlertDialog.Builder(getParent())
						.setTitle(getText(R.string.app_name))
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										result.confirm();
									}
								})
						.setNegativeButton(android.R.string.cancel,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										result.cancel();
									}
								}).create().show();

				return true;
			};

			public boolean onJsAlert(WebView view, String url,
					final String message, JsResult result) {
				final JsResult r = result;
				if (null == self)
					return false;

				new AlertDialog.Builder(getParent())
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										if (message.contains("로그인") || message.contains("Session")) {
											CUser.mno = "";
											mWebView.stopLoading();
											logout();
										}
										r.confirm();
									}
								}).setCancelable(false).create().show();
				return true;
			}

			public void onProgressChanged(WebView view, int newProgress) {
			}
		});
		mWebView.setDownloadListener(new DownloadListener() {
			public void onDownloadStart(String url, String userAgent,
					String contentDisposition, String mimetype,
					long contentLength) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(url));
				startActivity(intent);
			}
		});

		mWebView.loadUrl(url);
	}

	private class webViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.startsWith("http:") || url.startsWith("https:")) {
				if (url.contains(Constants.baseUrl) && CUser.userid != "") {
					url += "&mem_id=" + CUser.userid;
				}
				
				_url = url;
				view.loadUrl(url);
				return true;
			}
			
			if (url.startsWith("tel:")) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(intent);
				return false;
			} else if(url.startsWith("market:")) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(intent);
				return false;
			}
			else if(url.startsWith("intent:")) {
				try {
					Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
					if (getPackageManager().resolveActivity(intent, 0) == null) {
						String packageName = intent.getPackage();
						startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "market://search?q=pname:" + packageName ) ) );
						return true;
					}
					
					((Activity)getParent()).startActivity(intent);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return true;
			} else {
				try {
					Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
					if (getPackageManager().resolveActivity(intent, 0) == null) {
						String packageName = intent.getPackage();
						startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "market://search?q=pname:" + packageName ) ) );
						return true;
					}
					((Activity)getParent()).startActivity(intent);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return true;
			}
		}

		public void onLoadResource(WebView view, String url) {
			try {
				if (progressDialog == null) {
					progressDialog = WaitDialog.show(getParent(), "", "", true, true, null);
				}
			} catch (Exception e) {
			}
			(new Handler()).postDelayed(new Runnable() {
				public void run() {
					try {
						if (progressDialog != null
								&& progressDialog.isShowing()) {
							progressDialog.dismiss();
							progressDialog = null;
						}
					} catch (Exception e) {
					}
				}
			}, 2000);
		}

		public void onPageFinished(WebView view, String url) {
			try {
				if (progressDialog != null && progressDialog.isShowing()) {
					progressDialog.dismiss();
					progressDialog = null;
				}

				if (url.endsWith(".mp4")) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					Uri uri = Uri.parse(url);
					i.setDataAndType(uri, "video/mp4");
					startActivity(i);
				}
			} catch (Exception e) {
			}
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			Util.ToastMessage(getParent(),
					getResources().getString(R.string.api_http_alert));
		}
	}

	public void onStart() {
		super.onStart();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();

		super.onResume();
	}

	private class AndroidBridge {
		@JavascriptInterface
		public void callAndroid(final String arg) {
			handler.post(new Runnable() {
				public void run() {
					if (arg.equals("home")) {
						finish();
						((Activity) getParent()).startActivity(new Intent(self, MainActivity.class));
					}
				}
			});
		}

		@JavascriptInterface
		public void goOffLinePay(final String url) {
			handler.post(new Runnable() {
				public void run() {
					String _url = Constants.MobilePaymentUrl + url;
					mWebView.loadUrl(_url);
				}
			});
		}
		
		@JavascriptInterface
		public void goLogin(final String url) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					payUrl = Constants.baseUrl + url;
					Intent i = new Intent(self, LoginActivity.class);
					i.putExtra(Constants.REQUEST_FIELD, Constants.REQUEST_OFFLINE_ACTIVITY);
					((Activity)getParent()).startActivityForResult(i, Constants.REQUEST_OFFLINE_ACTIVITY);
				}
			});
		}

		@JavascriptInterface
		public void goStudy(final String url, final String title) {
			handler.post(new Runnable() {
				public void run() {
					if (Util.isWifiConnected(self)) {
						goPlayVideo(url, title);
					} else {
					}
				}
			});
		}

		@JavascriptInterface
		public void go_home() { 
			handler.post(new Runnable() {
				public void run() {
					onBackPressed();
				}
			});
		}

		@JavascriptInterface
		public void telCounsel() {
			handler.post(new Runnable() {
				public void run() {
					String tel = Constants.tel_guestcenter;
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tel));
					startActivity(intent);
				}
			});
		}
	}
	
	@Override
	public void onBackPressed() {
		if (!mWebView.canGoBack()) {
			finish();
			((Activity) getParent()).startActivity(new Intent(self, MainActivity.class));
		} else {
			mWebView.goBack();
		}

		return;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		payUrl += "&mem_id="+CUser.userid;
		mWebView.loadUrl(payUrl);
	}

	public void goPlayVideo(String url, String title) {
	}
}
