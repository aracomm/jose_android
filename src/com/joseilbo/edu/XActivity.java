package com.joseilbo.edu;

import java.util.UUID;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.CookieSyncManager;

import com.joseilbo.edu.adapter.Adapter;
import com.joseilbo.edu.adapter.DBmanager;
import com.joseilbo.edu.adapter.HttpHelper;
import com.joseilbo.edu.common.Environments;
import com.joseilbo.edu.common.IntentModelActivity;

public class XActivity extends IntentModelActivity implements OnClickListener {
public Context ctx;
private Uri mImageCaptureUri;
protected Adapter _api;
SQLiteDatabase db;
DBmanager db_manager;	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		_api= new Adapter();
		CookieSyncManager.createInstance(this);
		super.onCreate(savedInstanceState);
	}
	
	public void goActivity(Intent intent, boolean clearTop)  {		
		XActivityGroup parent = ((XActivityGroup)getParent());
		View view = parent.group.getLocalActivityManager().startActivity(String.valueOf(UUID.randomUUID()), intent).getDecorView();
		
		if (!clearTop)
			parent.group.addView(view);	
		else 
			parent.group.clearTopView(view);
	}	 

	public void loadPhoto() {
		   Intent intent = new Intent(Intent.ACTION_PICK);
		   intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
		   startActivityForResult(intent, 0);
	}

	public void onClick(View view) {
	}

	@Override
	protected void onResume() {
		HttpHelper hh = new HttpHelper();
		if(hh.getCookies().size()<=0)
			loadUserInfo();
		
		super.onResume();
	}
	
	@Override
	 protected void onActivityResult(int requestCode, int resultCode, Intent data)  {
		 if(resultCode != RESULT_OK)  {
			 return;
		 }
	 }
	
	public void logout() {
		db_manager = new DBmanager(this,"UserInfo.db");
		db = db_manager.getWritableDatabase();
		String sql = "delete from userinfo";
		db.execSQL(sql);
		Environments.SAVE_ACCOUNT_INFO=false;
		Environments.save(this);
		db.close();
		finish();
		startActivity(new Intent(self,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
	}
}
