package com.joseilbo.edu;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Preference;
import com.joseilbo.edu.common.Util;

public class MainActivity extends XActivity {
	MainActivity self;
	Context context;

	String planner_tel = "", kakaoid = "";
	boolean isInstalled = false;
	
	// WebView variables
	String uid, _url, _title;
	WebView mWebView;
	private final Handler handler = new Handler();
	private boolean isOnline = true;
	AlertDialog alert;
	protected static WaitDialog progressDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		Util.setStrictMode();
		self = this;
		context = this;
		
		//event assign
		((ImageView) findViewById(R.id.iv_home)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_study_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_call_upper_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_setting_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_search_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_introduce_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_login_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_notice_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_call_main2)).setOnClickListener(self);
		
		//login check
		if (CUser.mno.equals("")) {
			((ImageView) findViewById(R.id.btn_login_main2)).setImageResource(R.drawable.footer_btn2);
		} else {
			((ImageView) findViewById(R.id.btn_login_main2)).setImageResource(R.drawable.footer_btn3);
		}

		//start main
		loadWebView(Constants.MobileMainUrl);
	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();
		
		mWebView.reload();
		super.onResume();
	}

	@Override
	public void onClick(View view) {
		super.onClick(view);
		switch (view.getId()) {
		case R.id.iv_home:
			mWebView.loadUrl(Constants.MobileMainUrl);
			break;
		
		case R.id.btn_study_main2:
			if (CUser.mno.equals("")) {
				Intent i = new Intent(this, LoginActivity.class);
				startActivity(i);
			} else {
				startActivity(new Intent(self, MyClassActivity.class));
			}
			break;
			
		case R.id.btn_call_upper_main2:
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.tel_guestcenter)));
			break;
			
		case R.id.btn_setting_main2:
			startActivity(new Intent(this, SettingActivity.class));
			break;
			
		case R.id.btn_search_main2:
			String _searchUrl = Constants.CourseSearchUrl + "?mem_id=" + CUser.userid;
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra( "url", _searchUrl).putExtra("title", "검색"));
			break;
			
		case R.id.btn_introduce_main2:
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra("url", Constants.MobileOfficeIntro).putExtra("title", "재무교육원 소개"));
			break;
			
		case R.id.btn_login_main2:
			if (CUser.mno.equals("")) {
				startActivity(new Intent(this, LoginActivity.class));
			} else {
				Util.alertYesNo(self, R.string.app_name, R.string.app_logout, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								finish();
								(new Handler()).post(new Runnable() {
									public void run() {
										CUser.username = "";
										CUser.userpw = "";
										CUser.mno = "";
										CUser.userid = "";
										logout();
										
										((ImageView) findViewById(R.id.btn_login_main2)).setImageResource(R.drawable.footer_btn2);
									}
								});
							}
						}, null);
			}
			break;
			
		case R.id.btn_notice_main2:
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra( "url", Constants.NoticeListUrl).putExtra("title", "공지사항"));
			break;
			
		case R.id.btn_call_main2:
			startActivity(new Intent(this, SupportActivity.class));
			break;
		}
	}

	@Override
	public void onBackPressed() {
		if (!mWebView.canGoBack()) {
			Util.alertYesNo(self, R.string.app_name, R.string.app_exit, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					finish();
					(new Handler()).post(new Runnable() {
						public void run() {
							CUser.userid = "";
							CUser.username = "";
							CUser.userpw = "";
						}
					});
				}
			}, null);
		} else {
			mWebView.goBack();
		}
		return;
	}
	
	///////////////////
	// WebView code
	private void loadWebView(String url) {
		mWebView = (WebView) findViewById(R.id.webView_main2);
		mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.addJavascriptInterface(new AndroidBridge(), "android");
		mWebView.setHorizontalScrollBarEnabled(true); // 세로 scroll 제거
		mWebView.setVerticalScrollBarEnabled(false); // 가로 scroll 제거
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.getSettings().setUseWideViewPort(true);
		mWebView.getSettings().setLoadWithOverviewMode(true);
		mWebView.setWebViewClient(new webViewClient());
		mWebView.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onJsConfirm(WebView view, String url, String message, final android.webkit.JsResult result) {
				new AlertDialog.Builder(self)
						.setTitle(getText(R.string.app_name))
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										result.confirm();
									}
								})
						.setNegativeButton(android.R.string.cancel,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										result.cancel();
									}
								}).create().show();

				return true;
			};

			public boolean onJsAlert(WebView view, String url, final String message, JsResult result) {
				final JsResult r = result;
				if (null == self)
					return false;

				new AlertDialog.Builder(self)
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										if (message.contains("로그인") || message.contains("Session")) {
											CUser.mno = "";
											mWebView.stopLoading();
											logout();
											
											((ImageView) findViewById(R.id.btn_login_main2)).setImageResource(R.drawable.footer_btn2);
										}
										r.confirm();
									}
								}).setCancelable(false).create().show();
				return true;
			}

			public void onProgressChanged(WebView view, int newProgress) {
			}
		});
		
		mWebView.setDownloadListener(new DownloadListener() {
			public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
				startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url)));
			}
		});
		
		Uri externalData = getIntent().getData();
		if (externalData != null) {
			String externalUrl = externalData.getQueryParameter("url");
			
			if(externalUrl != null && !externalUrl.trim().equals("")) {
				mWebView.loadUrl(externalUrl);
				return;
			}
		} 
		
		mWebView.loadUrl(url);
		return;
	}

	private class webViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Util.debug(url);
			
			if (url != null && !url.equals("about:blank")) {
				String url_scheme_nm = url.substring(0, 10);

				if (url_scheme_nm.contains("http://") || url_scheme_nm.contains("https://")) {
					if (!url.contains("mem_id") && !url.contains(CUser.userid)) {
						if(url.contains("?"))
							url += "&mem_id=" + CUser.userid ;
						else 
							url += "?mem_id=" + CUser.userid ;
					}
					
					if (url.indexOf("isLine=off") > 0) {
						Util.debug("isOnline : false");
						isOnline = false;
					} else if (url.indexOf("isLine=on") > 0) {
						Util.debug("isOnline : true");
						isOnline = true;
					}
					
					_url = url;
					view.loadUrl(url);
					return true;
				}
				
				if (url_scheme_nm.contains("tel:") || url_scheme_nm.contains("mainto:") || url_scheme_nm.contains("market:")) {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
					return false;
				} else if(url_scheme_nm.contains("intent:")) {
					try {
						Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
						if (getPackageManager().resolveActivity(intent, 0) == null) {
							String packageName = intent.getPackage();
							startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pname:" + packageName)));
							return true;
						}
						
						startActivity(intent);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return true;
				} else {
					try {
						Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
						if (getPackageManager().resolveActivity(intent, 0) == null) {
							String packageName = intent.getPackage();
							startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pname:" + packageName)));
							return true;
						}
						startActivity(intent);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return true;
				}
				
			}
			
			return true;
		}

		public void onLoadResource(WebView view, String url) {
			try {
				if (progressDialog == null) {
					progressDialog = WaitDialog.show(self, "", "", true, true, null);
				}
			} catch (Exception e) {
			}
			(new Handler()).postDelayed(new Runnable() {
				public void run() {
					try {
						if (progressDialog != null && progressDialog.isShowing()) {
							progressDialog.dismiss();
							progressDialog = null;
						}
					} catch (Exception e) {
					}
				}
			}, 2000);
		}

		public void onPageFinished(WebView view, String url) {
			try {
				if (progressDialog != null && progressDialog.isShowing()) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (url.endsWith(".mp4")) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					Uri uri = Uri.parse(url);
					i.setDataAndType(uri, "video/mp4");
					startActivity(i);
				}
			} catch (Exception e) {
			}
			CookieSyncManager.getInstance().sync();
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Util.ToastMessage(self, getResources().getString(R.string.api_http_alert));
		}
	}

	public void onStart() {
		super.onStart();
		CookieSyncManager.createInstance(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		CookieSyncManager.getInstance().stopSync();
	}

	private class AndroidBridge {
		@JavascriptInterface
		public void callAndroid(final String arg) {
			handler.post(new Runnable() {
				public void run() {
					if (arg.equals("home")) {
						finish();
						((Activity) self).startActivity(new Intent(self, MainActivity.class));
					}
				}
			});
		}
		
		@JavascriptInterface
		public void goLogin(final String url) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					if (CUser.mno.equals("")) {
						Intent i = new Intent(self, LoginActivity.class);
						if (isOnline == true) {
							i.putExtra(Constants.REQUEST_FIELD, Constants.REQUEST_SEARCH_ONLINE_ACTIVITY);
							startActivityForResult(i, Constants.REQUEST_SEARCH_ONLINE_ACTIVITY);
						} else {
							i.putExtra(Constants.REQUEST_FIELD, Constants.REQUEST_SEARCH_OFFLINE_ACTIVITY);
							startActivityForResult(i, Constants.REQUEST_SEARCH_OFFLINE_ACTIVITY);
						}
					} else {
						Util.alertYesNo(self, R.string.app_name, R.string.app_logout, new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										finish();
										(new Handler()).post(new Runnable() {
											public void run() {
												CUser.username = "";
												CUser.userpw = "";
												CUser.mno = "";
												CUser.userid = "";
												logout();
											}
										});
									}
								}, null);
					}
				}
			});
		}
		
		@JavascriptInterface
		public void goPreview(final String url) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					startActivity(new Intent(self,Html5WebActivity.class).putExtra("url", url));
				}
			});
		}

		@JavascriptInterface
		public void telCounsel() { 
			handler.post(new Runnable() {
				public void run() {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.tel_guestcenter)));
				}
			});
		}
		
		@JavascriptInterface
		public void goOnLinePay(final String url) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					String moveUrl = Constants.MobilePaymentUrl + url;
					if (!moveUrl.contains("mem_id")) {
						moveUrl += "&mem_id="+CUser.userid;;
					}
					Util.debug("goOnLinePay : " + moveUrl);

					Preference pref = new Preference(self, Util.MT_PREFS);
					pref.write("direct_url", moveUrl);

					startActivity(new Intent(self,PopWebViewActivity.class).putExtra("url", moveUrl).putExtra("title", "수강신청"));
				}
			});
		}

		@JavascriptInterface
		public void goOffLinePay(final String url) {
			handler.post(new Runnable() {
				public void run() {
					String moveUrl = Constants.MobilePaymentUrl + url;
					if (!moveUrl.contains("mem_id")) {
						moveUrl += "&mem_id="+CUser.userid;;
					}
					Util.debug("goOffLinePay : " + moveUrl);
					
					Preference pref = new Preference(self, Util.MT_PREFS);
					pref.write("direct_url", moveUrl);

					startActivity(new Intent(self,PopWebViewActivity.class).putExtra("url", moveUrl).putExtra("title", "수강신청"));
				}
			});
		}
	}
}
