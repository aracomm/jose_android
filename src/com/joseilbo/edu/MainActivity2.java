package com.joseilbo.edu;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import com.joseilbo.edu.adapter.AdapterItemManager;
import com.joseilbo.edu.adapter.Api;
import com.joseilbo.edu.adapter.LectureListThisMonth;
import com.joseilbo.edu.adapter.LectureListThisMonthAdapter;
import com.joseilbo.edu.adapter.LectureListThisMonthOnline;
import com.joseilbo.edu.adapter.NoticeAdapter;
import com.joseilbo.edu.adapter.OnResponseListener;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Preference;
import com.joseilbo.edu.common.StaticVars;
import com.joseilbo.edu.common.Util;
import com.joseilbo.edu.container.EventItem;
import com.joseilbo.edu.container.Main;
import com.joseilbo.edu.container.NoticeItem;

public class MainActivity2 extends XActivity implements OnClickListener, OnResponseListener {
	MainActivity2 self;
	Context context;
	OnResponseListener callback;
	ArrayList<NoticeItem> nlist;
	ArrayList<EventItem> elist;

	ListView list;
	NoticeAdapter nadapter;

	ArrayList<NoticeItem> nlistThisMonth;
	ListView listThisMonth;
	LectureListThisMonthAdapter nadapterThisMonth;

	int density = 0;

	String planner_tel = "", kakaoid = "";
	boolean isInstalled = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_old);
		Util.setStrictMode();
		self = this;
		context = this;
		callback = this;
		// Get screen info
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		density = metrics.densityDpi;
		Util.debug("DPI : " + String.valueOf(metrics.densityDpi) + " dpi");
		((ImageButton) findViewById(R.id.btn_study)).setOnClickListener(self);
		((ImageButton) findViewById(R.id.btn_monthlyonline)).setOnClickListener(self);
		((ImageButton) findViewById(R.id.btn_allcourse)).setOnClickListener(self);
		((ImageButton) findViewById(R.id.btn_support)).setOnClickListener(self);
		((ImageButton) findViewById(R.id.btn_more)).setOnClickListener(self);
		((ImageButton) findViewById(R.id.btn_more2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_call)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_login)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_search)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_homepage)).setOnClickListener(self);

		if (CUser.mno.equals("")) {
			((ImageView) findViewById(R.id.btn_login)).setImageResource(R.drawable.btn_login2);
		} else {
			((ImageView) findViewById(R.id.btn_login)).setImageResource(R.drawable.btn_logout2);
		}

		initLists();

		File filedir;
		if (Util.canUseExternalMemory())
			filedir = new File(Constants.rootDirectory + CUser.userid + "/");
		else
			filedir = new File(Constants.InternalrootDirectory + CUser.userid + "/");
		
		if (!filedir.exists())
			filedir.mkdirs();
	}

	private void initLists() {
		// 공지사항
		list = (ListView) findViewById(R.id.list);
		list.setDivider(null);

		nlist = new ArrayList<NoticeItem>();
		list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				String url = nlist.get(position).getLink();
				startActivity(new Intent(self, PopWebViewActivity.class).putExtra("url", url));
			}
		});

		// 이달의 오프라인 교육
		listThisMonth = (ListView) findViewById(R.id.list2);
		listThisMonth.setDivider(null);

		nlistThisMonth = new ArrayList<NoticeItem>();
		listThisMonth.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				String url = nlistThisMonth.get(position).getLink();
				if (url.indexOf("mem_id") < 0 && !CUser.userid.equals("")) {
					url += "&mem_id=" + CUser.userid ;
				}
				
				startActivity(new Intent(self, PopWebViewActivity.class).putExtra("url", url));
			}
		});
	}

	@Override
	protected void onResume() {
		_api.main(context, callback);
		_api.lectureListThisMonth(context, callback); // jose
		//_api.lectureListThisMonthOnline(context, callback); //jose

		super.onResume();
	}

	@Override
	public void onClick(View view) {
		super.onClick(view);
		switch (view.getId()) {
		case R.id.btn_search:
			String _searchUrl = Constants.CourseSearchUrl + "?mem_id=" + CUser.userid;
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra( "url", _searchUrl));
			break;
		case R.id.btn_study:
			if (CUser.mno.equals("")) {
				Intent i = new Intent(this, LoginActivity.class);
				startActivity(i);
			} else {
				startActivity(new Intent(self, RootActivity.class).putExtra( "menu", 0));
				finish();
			}
			break;
		case R.id.btn_monthlyonline:
			String _url = Constants.MonthOnlineUrl;
			//startActivity(new Intent(self, PopWebViewActivity.class).putExtra("url", _url).putExtra("title", "이달의 온라인교육"));
			//startActivity(new Intent(self, RootActivity.class).putExtra("menu",1).putExtra("url", _url)); // jose - OnlineClassActivity
			
			Preference pref = new Preference(this, Util.MT_PREFS);
			pref.write("direct_url", _url);
			
			startActivity(new Intent(self, RootActivity.class).putExtra("menu",1)); // jose - OnlineClassActivity
			finish();
			break;
		case R.id.btn_allcourse:
			startActivity(new Intent(self, RootActivity.class).putExtra("menu",1)); // jose - OnlineClassActivity
			finish();
			break;
		case R.id.btn_support:
			startActivity(new Intent(self, RootActivity.class).putExtra("menu", 2)); // jose - OfflineClassActivity
			finish();
			break;
		case R.id.btn_more:
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra( "url", Constants.NoticeListUrl));
			break;
		case R.id.btn_more2:
			startActivity(new Intent(self, RootActivity.class).putExtra("menu", 2)); // jose - OfflineClassActivity
			finish();
			break;
		case R.id.btn_login:
			if (CUser.mno.equals("")) {
				Intent i = new Intent(this, LoginActivity.class);
				startActivity(i);
			} else {
				Util.alertYesNo(self, R.string.app_name, R.string.app_logout, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								finish();
								(new Handler()).post(new Runnable() {
									public void run() {
										CUser.username = "";
										CUser.userpw = "";
										CUser.mno = "";
										CUser.userid = "";
										logout();
									}
								});
							}
						}, null);
			}
			break;
		case R.id.btn_call:
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.tel_guestcenter));
			startActivity(intent);
			break;
		case R.id.btn_homepage:
			Intent homepageIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.Homepage));
			startActivity(homepageIntent);
			break;
		}
	}

	public void onResponseReceived(int api, Object result) {
		try {
			if (result == null) {
				Util.PopupMessage(this, getResources().getString(R.string.api_http_alert));
				Log.e("jose", String.valueOf(api));
				Log.e("jose", String.valueOf(result));
				return;
			}
			
			switch (api) {
			case Api.THISMONTH:
					LectureListThisMonth lectureList = (LectureListThisMonth) result;
					AdapterItemManager.AddLectureListThisMonth(lectureList.list);
					nlistThisMonth = StaticVars.thisMonthItems;
					nadapterThisMonth = new LectureListThisMonthAdapter(self, nlistThisMonth, density);
					listThisMonth.setAdapter(nadapterThisMonth);
				break;
			case Api.MAIN:
					Main main = (Main) result;
					AdapterItemManager.AddNotice(main.list);
					nlist = StaticVars.noticeItems;
					nadapter = new NoticeAdapter(self, nlist, density);
					list.setAdapter(nadapter);
				break;
			case Api.THISMONTHONLINE:
					LectureListThisMonthOnline onlineList = (LectureListThisMonthOnline) result;
					AdapterItemManager.AddMonthOnlineList(onlineList.list);
				break;
			}

		} catch (NullPointerException e) {
			Log.e("jose", e.toString());
		}
	}

	@Override
	public void onBackPressed() {
		Util.alertYesNo(self, R.string.app_name, R.string.app_exit, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finish();
						(new Handler()).post(new Runnable() {
							public void run() {
								CUser.userid = "";
								CUser.username = "";
								CUser.userpw = "";
							}
						});
					}
				}, null);
		return;
	}
}
