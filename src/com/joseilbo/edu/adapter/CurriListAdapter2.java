package com.joseilbo.edu.adapter;

import java.util.ArrayList;

import com.joseilbo.edu.Html5WebActivity;
import com.joseilbo.edu.R;
import com.joseilbo.edu.common.Preference;
import com.joseilbo.edu.common.Util;
import com.joseilbo.edu.container.CurriInfo;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class CurriListAdapter2 extends BaseAdapter {

	private CurriListAdapterAction mCurriListAdapterAction;

	public interface CurriListAdapterAction {
		void playStudy(String url);

		void downloadStudy(String url);

		void playDownloadStudy(String key);
	}

	public void setOnCurriListAdapterAction(CurriListAdapterAction action) {
		mCurriListAdapterAction = action;
	}

	private Context mContext;
	private String num;
	private ArrayList<CurriInfo> list;

	public CurriListAdapter2(Context pContext, String num,
			ArrayList<CurriInfo> list) {
		this.mContext = pContext;
		this.num = num;
		this.list = list;
	}

	public void setList(ArrayList<CurriInfo> list) {
		this.list = list;
	}

	public int getCount() {
		return list.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int positon) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		ListItemHolder listItemHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.curri_item, null);

			listItemHolder = new ListItemHolder();

			listItemHolder.tv_subject = (TextView) convertView
					.findViewById(R.id.tv_subject);
			listItemHolder.tv_progress = (TextView) convertView
					.findViewById(R.id.tv_progress);
			listItemHolder.tv_txt = (TextView) convertView
					.findViewById(R.id.tv_txt);
			convertView.setTag(listItemHolder);
		} else {
			listItemHolder = (ListItemHolder) convertView.getTag();
		}

		if (isDownloaded(list.get(position).chDownload)) {
			listItemHolder.tv_progress.setText(mContext.getResources()
					.getString(R.string.curri_study_movie));
			listItemHolder.tv_progress
					.setBackgroundResource(R.drawable.btn_download_play);
		} else {
			listItemHolder.tv_progress.setText(mContext.getResources()
					.getString(R.string.curri_study_download));
			listItemHolder.tv_progress
					.setBackgroundResource(R.drawable.btn_study_download);
		}

		String txt = mContext.getResources().getString(
				R.string.curri_study_start);
		listItemHolder.tv_txt.setText(txt);
		listItemHolder.tv_subject.setText(list.get(position).chSubject);

		listItemHolder.tv_txt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String url = list.get(position).chSource;
				
				//학습URL에 역슬래쉬가 포함되어 있으면 제거
				if (url.contains("\\")) url = url.replace("\\", "");
				
				mCurriListAdapterAction.playStudy(url);
			}
		});

		listItemHolder.tv_progress
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						String downloadUrl = list.get(position).chDownload;

						if (isDownloaded(downloadUrl)) {
							mCurriListAdapterAction
									.playDownloadStudy(makeKeyForDownloadStudy(downloadUrl));
						} else {
							mCurriListAdapterAction.downloadStudy(downloadUrl);
						}
					}
				});

		return convertView;
	}

	private boolean isDownloaded(String url) {
		String key = makeKeyForDownloadStudy(url);

		Preference pref = new Preference(mContext, Util.MT_PREFS);
		String value = pref.read(key, "");
		if (value.equals("")) {
			return false;
		} else {
			return true;
		}
	}

	private String makeKeyForDownloadStudy(String url) {
		String[] data = url.replace("\\", "").split("/");
		String key = this.num + "_" + data[data.length - 1];
		return key;
	}

	class ListItemHolder {
		TextView tv_subject;
		TextView tv_progress;
		TextView tv_txt;
	}
}
