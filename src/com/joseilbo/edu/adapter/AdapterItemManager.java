package com.joseilbo.edu.adapter;

import java.util.List;

import com.joseilbo.edu.common.StaticVars;
import com.joseilbo.edu.container.CurriInfo;
import com.joseilbo.edu.container.CurriculumItem;
import com.joseilbo.edu.container.Event;
import com.joseilbo.edu.container.EventItem;
import com.joseilbo.edu.container.LectureInfo;
import com.joseilbo.edu.container.LectureItem;
import com.joseilbo.edu.container.MonthOnlineInfo;
import com.joseilbo.edu.container.MonthOnlineItem;
import com.joseilbo.edu.container.NoticeInfo;
import com.joseilbo.edu.container.NoticeItem;
import com.joseilbo.edu.container.SiteItem;
import com.joseilbo.edu.container.WeekInfo;



public class AdapterItemManager {
	private static void site_addlist(String siteid, String sitename) {
		SiteItem siteitem = new SiteItem();    	
		siteitem.setSite(siteid, sitename);
		StaticVars.siteItems.add(siteitem);
	}

	/*
	private static void curri_addlist(String id, String step_id,String lesson_id, String type, String chapter,
			String sort, String time, String offline_yn, String subject, String start_day,
			String status, String service_yn, String study_yn, String video_info_type, List<WeekInfo> curriculum) {
    	CurriculumItem ctem = new CurriculumItem();    	
    	ctem.setCurriculum(id, step_id, lesson_id, type, chapter,
    			sort, time, offline_yn, subject, start_day,
    			status, service_yn, study_yn, video_info_type, curriculum);
    	StaticVars.curriItems.add(ctem);
    }
	
	public static CurriculumItem curriitem_addlist(String idx,String no,String gubun,String displa_no,String standtime,String learningtime,
			String learninginfo, String content_id,String jindo_yn) {
    	CurriculumItem ctem = new CurriculumItem();    	
    	ctem.setCurriculumItem(idx, no, gubun, displa_no, standtime,learningtime,learninginfo,content_id,jindo_yn);
    	return ctem;
    }
	
	public static void AddCurriculum(List<CurriculumInfo> clist) {
		StaticVars.curriItems.clear();
		for (CurriculumInfo c : clist) {
			AdapterItemManager.curri_addlist(c.classkey,c.weekseq,c.weekname,c.attandance,c.kind,c.startdate,c.enddate,c.fullenddate,c.curriculum,c.planChkYN,c.planURL);
		}	     
	}
	*/
	
	public static void curri_addlist(WeekInfo data, List<CurriculumItem> children) {
		CurriculumItem ctem = new CurriculumItem();    	
		ctem.setCurriculum(data, children);
		StaticVars.curriItems.add(ctem);
	}
	
	public static CurriculumItem curriitem_addlist(WeekInfo data) {
    	CurriculumItem ctem = new CurriculumItem();    	
    	ctem.setCurriculumItem(data);
    	return ctem;
    }
	
	public static void curriitem_addlist2(CurriInfo data) {
		CurriInfo item = (CurriInfo) data.clone();
    	StaticVars.curriItems2.add(item);
    }
	
	private static void lec_addlist(String part, String page,String subject, String readflag, String movieurl, String htmlurl,String progress, String pagestudytime, String pagetotaltime)  {
    	LectureItem ltem = new LectureItem();    	
    	ltem.setLecture(part, page, subject, readflag, movieurl, htmlurl, progress, pagestudytime, pagetotaltime);
    	StaticVars.lectureItems.add(ltem);
    }
	
	public static void AddLecture(List<LectureInfo> leclist) {
		StaticVars.lectureItems.clear();
		for (LectureInfo l : leclist) {
			AdapterItemManager.lec_addlist(l.part, l.page, l.subject, l.readflag, l.movieurl, l.htmlurl,l.progress,l.pagestudytime,l.pagetotaltime);
		}	     
	}
	private static void notice_addlist(String brd_idx, String brd_subject, String brd_link)  {
    	NoticeItem ltem = new NoticeItem();    	
    	ltem.setNotice(brd_idx, brd_subject, brd_link);
    	StaticVars.noticeItems.add(ltem);
    }
	public static void AddNotice(List<NoticeInfo> noticelist) {
		StaticVars.noticeItems.clear();
		for (NoticeInfo n : noticelist) {
//			AdapterItemManager.notice_addlist(n.brd_idx, n.brd_subject);
//			AdapterItemManager.notice_addlist(n.id, n.subject);		// mHanuri
			AdapterItemManager.notice_addlist(n.num, n.title, n.link);
		}	     
	}
	
	// jose - begin
	private static void lectureListThisMonth_addlist(String brd_idx, String brd_subject, String brd_link)  {
    	NoticeItem ltem = new NoticeItem();    	
    	ltem.setNotice(brd_idx, brd_subject, brd_link);
    	StaticVars.thisMonthItems.add(ltem);
    }
	public static void AddLectureListThisMonth(List<NoticeInfo> noticelist) {
		StaticVars.thisMonthItems.clear();
		for (NoticeInfo n : noticelist) {
			AdapterItemManager.lectureListThisMonth_addlist(n.num, n.title, n.link);
		}	     
	}
	// jose - end
	
	private static void event_addlist(String evt_idx, String evt_url,  String evt_img_url)  {
    	EventItem item = new EventItem();    	
    	item.setEvent(evt_idx, evt_url, evt_img_url);
    	StaticVars.eventItems.add(item);
    }
	
	public static void AddEvent(List<Event> eventList) {
		StaticVars.eventItems.clear();
		for (Event e : eventList) {
			AdapterItemManager.event_addlist(e.evt_idx, e.evt_url, e.evt_img_url);
		}	     
	}

	public static void AddMonthOnlineList(List<MonthOnlineInfo> list) {
		StaticVars.monthOnlineItems.clear();
		for (MonthOnlineInfo info : list) {
			AdapterItemManager.monthOnlineList_addlist(info.img, info.url);
		}
	}

	private static void monthOnlineList_addlist(String img, String url) {
		StaticVars.monthOnlineItems.add(new MonthOnlineItem(img, url));
	}
}