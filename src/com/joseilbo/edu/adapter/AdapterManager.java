package com.joseilbo.edu.adapter;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.container.ApiResult;
import com.joseilbo.edu.container.CurriculumInfo2;
import com.joseilbo.edu.container.Download;
import com.joseilbo.edu.container.Lecture;
import com.joseilbo.edu.container.Main;
import com.joseilbo.edu.container.OTInfo;
import com.joseilbo.edu.container.Version;



public class AdapterManager extends Api {

	HttpHelper _httpHelper = new HttpHelper();
	ObjectMapper _objectMapper = new ObjectMapper();	
	ExecutorService _threadExecutor = Executors.newCachedThreadPool();

	public AdapterManager() {
		super.setBaseUrl(Constants.baseUrl);
	}

	/**
	 * HTTP를 이용하여 API 요청
	 * 쓰레드를 이용하여 비동기로 처리.
	 * @param lmParams 
	 */
	public void request(Params lmParams) {							

		try {
			/* Set parameters for Asynchronous sender. */
			lmParams.setUri(getUri(lmParams.getApi()));
			lmParams.setValueTypeRef(getTypeReference(lmParams.getApi()));

			/*
			 * Execute asynchronous sender thread.
			 */
			_threadExecutor.execute(
					new AsyncSender(_httpHelper, _objectMapper, lmParams));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * API 명령별 컨테이너 클래스형을 리턴한다.<br><br>
	 * @param api
	 * @return TypeReference<?>
	 */	
	private TypeReference<?> getTypeReference(int api) {
		switch(api) {
		case Api.SIGN 			: return new TypeReference<ApiResult>() {};					
//		case Api.CURRICULUM 	: return new TypeReference<List<CurriculumInfo>>() {};	//	mHanuri
		case Api.CURRICULUM 	: return new TypeReference<CurriculumInfo2>() {};
		case Api.LECTURE		: return new TypeReference<Lecture>() {};		
		case Api.DOWNLOAD		: return new TypeReference<Download>() {};		
		case Api.MAIN			: return new TypeReference<Main>() {};		
		case Api.PLAYSTATUS		: return new TypeReference<ApiResult>() {};		
		case Api.SIGNOUT		: return new TypeReference<ApiResult>() {};		
		case Api.USERIDCHK		: return new TypeReference<ApiResult>() {};		
		case Api.VERSION		: return new TypeReference<Version>() {};		
		case Api.PUSH			: return new TypeReference<ApiResult>() {};		
		case Api.OTINFO			: return new TypeReference<OTInfo>() {};		
		case Api.SUPERUSER 			: return new TypeReference<ApiResult>() {};
		case Api.THISMONTH 			: return new TypeReference<LectureListThisMonth>() {};	
		case Api.THISMONTHONLINE : return new TypeReference<LectureListThisMonthOnline>() {};
		}
		return null;
	}

	/**
	 * API의 URI를 구한다.
	 * @param api
	 * @return URI
	 */
	private String getUri(int api) {
		return (getBaseUrl() + getPath(api));
	}	

	/**
	 * Encode UTF-8
	 * @param params
	 * @return UTF8 Map<String, String>
	 */
	private Map<String, String> toUTF8(Map<String, String> params) {	
		Map<String, String> utf8Map = new HashMap<String, String>();
		for (Map.Entry<String, String> entry : params.entrySet())			 
			utf8Map.put(entry.getKey(), toUTF8(entry.getValue()));
		return utf8Map;				
	}	

	/**
	 * Encode UTF-8
	 * @param String
	 * @return UTF8 String ( or Empty string )
	 */
	private String toUTF8(String s) {
		try {
			return URLEncoder.encode(s, "UTF-8");
		} catch(Exception e) {
			return "";
		}
	}

	/**
	 * Close thread.
	 */
	public void close() {
		if (_threadExecutor != null)
			_threadExecutor.shutdownNow();
	}

	/**
	 * Shutdown connection manager.
	 */
	public void shutdownConnectionManager() {
		if (_httpHelper != null)
			_httpHelper.shutdownConnectionManager();
	}
}