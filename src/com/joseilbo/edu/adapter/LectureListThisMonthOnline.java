package com.joseilbo.edu.adapter;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.joseilbo.edu.container.MonthOnlineInfo;

@JsonIgnoreProperties(ignoreUnknown = true) 
public class LectureListThisMonthOnline {
	@JsonProperty("ErrorCode")	public String errorCode;
	@JsonProperty("TotCount")	public String totCount;
	@JsonProperty("List")	public List<MonthOnlineInfo> list;
}
