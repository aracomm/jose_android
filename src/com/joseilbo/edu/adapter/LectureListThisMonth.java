package com.joseilbo.edu.adapter;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.joseilbo.edu.container.NoticeInfo;

@JsonIgnoreProperties(ignoreUnknown = true) 
public class LectureListThisMonth {
	@JsonProperty("ErrorCode")	public String errorCode;
	@JsonProperty("TotCount")	public String totCount;
	@JsonProperty("List")	public List<NoticeInfo> list;
}
