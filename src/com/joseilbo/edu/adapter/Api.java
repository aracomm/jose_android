package com.joseilbo.edu.adapter;


public class Api {

	/*
	 * API Command.
	 * 
	 * 상수의 값은 0부터 순차적이어야 한다.
	 * Path 배열의 인덱스로 사용되므로 상호 순서가 올바른지 확인하여야 한다.
	 * 신규로 추가 시는 뒤에 저장한다.
	 */	
	public static final int SIGN 								= 0;	// 로그인
	public static final int CURRICULUM					= 1;	// 커리큘럼
	public static final int LECTURE							= 2;	// 강의목록
	public static final int DOWNLOAD					= 3;	// 다운로드
	public static final int MAIN								= 4;	// 메인 - 공지사항
	public static final int PLAYSTATUS					= 5;	// 플레이 저장
	public static final int SIGNOUT							= 6;	// 로그아웃
	public static final int USERIDCHK						= 7;	// 공인인증서 로그인을 위한 아이디 유효 체크
	public static final int VERSION							= 8;	// 버전체크
	public static final int PUSH								= 9;	// 알림체크
	public static final int OTINFO							= 10;	// OT강의 URL
	public static final int SUPERUSER						= 11; //수퍼유저 로그인
	public static final int THISMONTH						= 12;	// 메인 - 이달의 오프라인 교육 목록
	public static final int THISMONTHONLINE			= 13; // 메인 - 이달의 온라인 교육 목록

	// Base URL
	private static String BaseUrl;	

	// Path
	private static final String Path[] = {
		"/Json/login_Json.php",										// jose, login - login
		"/Json/lec_list_json2.php",									// jose, MyCurriculmActivity - curri_list
		"",
		"",
		"/Json/notice_json.php",										// jose, main - notice 
		"",
		"",
		"",
		"/Json/version.php",											// jose 
		"",
		"",
		"",
		"/Json/today_offline_json.php",							// jose, main - lecture list this month
		"/Json/month_online_json.php"								// jose, main - 
	};	

	/**
	 * 기본 URL 구함.<br><br>
	 * @return URL
	 */
	protected String getBaseUrl() {
		return BaseUrl;
	}	

	/**
	 * 기본 URL 설정.<br><br>
	 * @param url
	 */
	protected void setBaseUrl(String url) {
		BaseUrl = url;
	}	

	/**
	 * API에 대한 Path를 구함.<br><br>
	 * @param api
	 * @return 값이 존재하지 않는 경우 공백 &quot;&quot; 리턴.
	 */
	protected String getPath(int api) {
		try {
			return Path[api];
		} catch(Exception e) {
			return "";
		}
	}
}
