package com.joseilbo.edu.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joseilbo.edu.DownloadActivity;
import com.joseilbo.edu.R;
import com.joseilbo.edu.container.LectureItem;

public class DownListAdapter extends BaseAdapter {
	Context context;
	ArrayList<LectureItem> list;
	private LayoutInflater mInflater;
	int gubun=0;
	DBmanager db_manager;	
	SQLiteDatabase db;
	boolean mode_edit=false;
	public DownListAdapter(Context context,ArrayList<LectureItem> list) {
		this.context=context;
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.list=list;
	}
	public void setList(ArrayList<LectureItem> list)
	{
		this.list=list;
	}
	public void setMode(boolean mode)
	{
		mode_edit=mode;
	}
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}


	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}


	public long getItemId(int positon) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		CheckBox chk_item;
		TextView tv_subject;
		TextView tv_size;
		TextView tv_title;
		ImageButton btn_cha,btn_play;
		final LectureItem item=list.get(position);
		if (convertView == null){

			//				imageView.setLayoutParams(new GridView.LayoutParams(95, 95));
			convertView = (LinearLayout) LayoutInflater.from(context).inflate(
					R.layout.down_chkitem, parent, false);
			

		}

			chk_item = (CheckBox) convertView.findViewById(R.id.chk_item);
			tv_subject = (TextView) convertView.findViewById(R.id.tv_subject);
			tv_size = (TextView) convertView.findViewById(R.id.tv_size);
			tv_title = (TextView) convertView.findViewById(R.id.tv_title);
			convertView.setTag(R.id.chk_item, chk_item);
			btn_cha=(ImageButton)convertView.findViewById(R.id.btn_cha);
			btn_play=(ImageButton)convertView.findViewById(R.id.btn_play);
	
		chk_item.setChecked(item.isSelected());
		chk_item.setFocusable(false);
		if(mode_edit)
		{
			chk_item.setVisibility(View.VISIBLE);
			tv_size.setVisibility(View.VISIBLE);
			btn_cha.setVisibility(View.GONE);
			btn_play.setVisibility(View.GONE);

		}
		else
		{
			chk_item.setVisibility(View.GONE);
			tv_size.setVisibility(View.GONE);
			btn_cha.setVisibility(View.VISIBLE);
			btn_play.setVisibility(View.VISIBLE);
		}
		tv_subject.setText(item.getSubject());
		tv_size.setText(item.getSize()+"MB");
		tv_title.setText(item.getCurriTitle());
		

		String filename=item.getClasskey()+"_"+item.getGYear()+"_"+item.getGNum()+"_"+item.getPart()+".mp4";
		btn_play.setOnClickListener(new OnClickListener(){

			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(context.getClass() == (DownloadActivity.class) )
				{
//					((DownloadActivity)context).goVideo(position);
				}
			
			}
			
		});
		return convertView;
	}



}
