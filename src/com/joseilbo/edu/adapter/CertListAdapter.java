package com.joseilbo.edu.adapter;

import java.util.ArrayList;
import java.util.List;

import com.joseilbo.edu.R;
import com.joseilbo.edu.container.CertItem;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CertListAdapter extends BaseAdapter {

	private Context mContext;
	private boolean isSetCertlist = false;

	private List<CertItem> mItems = new ArrayList<CertItem>();
	
	// used to keep selected position in ListView
	private int selectedPos = -1;	// init value for not-selected
		
	private Cursor cursor = null;
	
	public CertListAdapter(Context context) {
		mContext = context;
	}

	public void addItem(CertItem it) {
		mItems.add(it);
	}
	
	public void removeItem(int position){
		mItems.remove(position);
	}

	public void setListItems(List<CertItem> lit) {
		mItems = lit;
	}

	public int getCount() {
		return mItems.size();
	}

	public Object getItem(int position) {
		return mItems.get(position);
	}

	public boolean areAllItemsSelectable() {
		return false;
	}

	public boolean isSelectable(int position) {
		try {
			return mItems.get(position).isSelectable();
		} catch (IndexOutOfBoundsException ex) {
			return false;
		}
	}

	public long getItemId(int position) {
		return position;
	}
	
	public void setSelectedPosition(int pos){
		selectedPos = pos;
		// inform the view of this change
		notifyDataSetChanged();
	}

	public int getSelectedPosition(){
		return selectedPos;
	}
	
	public void setCursor(Cursor cursor){
		this.cursor = cursor;
	}
	
	public Cursor getCursor(){
		return this.cursor;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		CertItem item = mItems.get(position);
		//convertView�� �ѹ� ������� ���ɻ����� ���� �ҽ� ~
		if (convertView == null || isSetCertlist) {			
			convertView = LayoutInflater.from(mContext).inflate(R.layout.cert_item,null);		
		}
			
			SpannableStringBuilder sb = new SpannableStringBuilder();
			sb.append(item.getCertName());
			sb.setSpan(new StyleSpan(Typeface.BOLD), 0, item.getCertName().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			((TextView)convertView.findViewById(R.id.tv_name)).setText(sb.toString());
			((TextView)convertView.findViewById(R.id.tv_kind)).setText(item.getCertKind());
			((TextView)convertView.findViewById(R.id.tv_date)).setText(item.getCertDate());

//			itemView.setText(0, sb.toString());
//			itemView.setText(1, item.getData(1));
//			itemView.setText(2, item.getData(2));
	
		
		 // change the row color based on selected state		
		//Resources res = mContext.getResources();
        if(selectedPos == position){
        	convertView.setBackgroundResource(R.color.gray1);
        }else{
        	convertView.setBackgroundColor(Color.WHITE);
        }          
		return convertView;
	}
}
