package com.joseilbo.edu.adapter;

import java.util.List;

import com.joseilbo.edu.R;
import com.joseilbo.edu.container.GuideItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.TextView;

public class GuideListAdapter extends BaseExpandableListAdapter {

	private Context mContext;
	private ExpandableListView list;
	private List<GuideItem> citem;
	private int[] groupStatus;

	public GuideListAdapter(Context pContext,
			ExpandableListView pExpandableListView,
			List<GuideItem> pGroupCollection) {
		mContext = pContext;
		citem = pGroupCollection;
		list = pExpandableListView;
		groupStatus = new int[citem.size()];

		setListEvent();
	}

	private void setListEvent() {

		list.setOnGroupExpandListener(new OnGroupExpandListener() {
			public void onGroupExpand(int arg0) {
				groupStatus[arg0] = 1;
			}
		});

		list.setOnGroupCollapseListener(new OnGroupCollapseListener() {
			public void onGroupCollapse(int arg0) {
				groupStatus[arg0] = 0;
			}
		});
	}
	
	public String getChild(int groupPosition, int childPosition) {
		return citem.get(groupPosition).getAryCont().get(childPosition).toString();
	}
	
	public long getChildId(int arg0, int arg1) {
		return 0;
	}
	
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		ChildHolder childHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.guide_item, null);

			childHolder = new ChildHolder();
			childHolder.tv_subject = (TextView) convertView.findViewById(R.id.tv_subject);
			convertView.setTag(childHolder);
		}else {
			childHolder = (ChildHolder) convertView.getTag();
		}
		childHolder.tv_subject.setText(citem.get(groupPosition).arycont.get(childPosition).toString());
		
		return convertView;
	}
	
	public int getChildrenCount(int gPosition) {
		if(citem.get(gPosition).arycont!=null)
			return citem.get(gPosition).arycont.size();
		else
			return 0;
	}
	
	public Object getGroup(int gPosition) {
		return citem.get(gPosition);
	}
	
	public int getGroupCount() {
		return citem.size();
	}
	
	public long getGroupId(int gPosition) {
		return gPosition;
	}

	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		GroupHolder groupHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.guide_group_item,
					null);
			groupHolder = new GroupHolder();
			groupHolder.tv_subject = (TextView) convertView.findViewById(R.id.tv_subject);
			groupHolder.iv_folder = (ImageView) convertView.findViewById(R.id.iv_folder);
			convertView.setTag(groupHolder);
		}
		else {
			groupHolder = (GroupHolder) convertView.getTag();
		}
		
		if (groupStatus[groupPosition] == 0) {
			groupHolder.iv_folder.setImageResource(R.drawable.btn_plus);
		} else {
			groupHolder.iv_folder.setImageResource(R.drawable.btn_minus);
		}
		groupHolder.tv_subject.setText(citem.get(groupPosition).getSubject());

		return convertView;
	}

	class GroupHolder {
		ImageView iv_attend;
		TextView tv_subject;
		ImageView iv_folder;
	}

	class ChildHolder {
		TextView tv_subject;
		TextView tV_progress;
	}
	
	public boolean hasStableIds() {
		return true;
	}

	public boolean isChildSelectable(int arg0, int arg1) {
		return true;
	}
}
