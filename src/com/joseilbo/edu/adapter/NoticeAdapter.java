package com.joseilbo.edu.adapter;

import java.util.ArrayList;

import com.joseilbo.edu.R;
import com.joseilbo.edu.common.Util;
import com.joseilbo.edu.container.NoticeItem;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NoticeAdapter extends BaseAdapter {
	Context context;
	ArrayList<NoticeItem> list;
	private LayoutInflater mInflater;
	int gubun=0;
	DBmanager db_manager;	
	SQLiteDatabase db;
	boolean mode_down=false;
	int density=0;	
	public NoticeAdapter(Context context,ArrayList<NoticeItem> list, int density ) {
		this.context=context;
		this.list=list;
		this.density=density;
	}
	public void setList(ArrayList<NoticeItem> list)
	{
		this.list=list;
	}
	
	public int getCount() {
		// TODO Auto-generated method stub
		if(density<=240) 
		{
			if(list.size()>2)
				return 2;
			else 
				return list.size();
		}
		else //풀HD해상도
		{
			if(list.size()>2)
				return 2;
			else 
			return list.size();

		}
	}


	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}


	public long getItemId(int positon) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final NoticeItem item=list.get(position);
		TextView tv_subject;
		
		if (convertView == null)
		{
			//	imageView.setLayoutParams(new GridView.LayoutParams(95, 95));
			convertView =  (LinearLayout) LayoutInflater.from(context).inflate(R.layout.notice_item, parent, false);
		}
			tv_subject = (TextView) convertView.findViewById(R.id.tv_subject);
//			tv_subject.setText(item.getBrdSubject());
			tv_subject.setText(item.getSubject());
	
		return convertView;
	}



}
