package com.joseilbo.edu.adapter;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joseilbo.edu.R;
import com.joseilbo.edu.common.Util;
import com.joseilbo.edu.container.LectureItem;

public class LectureListAdapter extends BaseAdapter {
	Context context;
	ArrayList<LectureItem> list;
	int gubun = 0;
	DBmanager db_manager;
	SQLiteDatabase db;
	boolean mode_down = false;
	String classidx = "";
	boolean isCDE = true;

	public LectureListAdapter(Context context, ArrayList<LectureItem> list, String classidx, boolean isCDE) {
		this.context = context;
		this.list = list;
		this.classidx = classidx;// classkey_gisuYear_gisuNum;
		this.isCDE = isCDE;
	}

	public void setList(ArrayList<LectureItem> list) {
		this.list = list;
	}

	public void setMode(boolean mode) {
		mode_down = mode;
	}

	public int getCount() {
		return list.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int positon) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final LectureItem item = list.get(position);
		TextView tv_lec_current, tv_lec_per, tv_lec_total, tv_subject, tv_page;
		ImageView iv_down;
		ImageView btn_play;
		final CheckBox chk_item;
		if (convertView == null) {
			convertView = (LinearLayout) LayoutInflater.from(context).inflate( R.layout.lec_chkitem, parent, false);
		}

		chk_item = (CheckBox) convertView.findViewById(R.id.chk_item);
		tv_lec_current = (TextView) convertView.findViewById(R.id.tv_lec_current);
		tv_lec_per = (TextView) convertView.findViewById(R.id.tv_lec_per);
		tv_lec_total = (TextView) convertView.findViewById(R.id.tv_lec_total);
		tv_subject = (TextView) convertView.findViewById(R.id.tv_subject);
		tv_page = (TextView) convertView.findViewById(R.id.tv_page);
		iv_down = (ImageView) convertView.findViewById(R.id.iv_down);
		btn_play = (ImageView) convertView.findViewById(R.id.btn_play);
		convertView.setTag(R.id.chk_item, chk_item);

		chk_item.setChecked(item.isSelected());
		chk_item.setFocusable(false);
		if (isCDE)
			iv_down.setVisibility(View.INVISIBLE);
		else
			iv_down.setVisibility(View.VISIBLE);
		if (mode_down) {
			chk_item.setVisibility(View.VISIBLE);
			if (item.getMovieurl().length() == 0) {
				chk_item.setVisibility(View.GONE);
				btn_play.setVisibility(View.GONE);
			} else {
				chk_item.setVisibility(View.VISIBLE);
				btn_play.setVisibility(View.VISIBLE);
			}
		} else
			chk_item.setVisibility(View.GONE);

		tv_subject.setText(item.getSubject());
		int page = Integer.parseInt(item.getPage()) - 1;
		String str_page = "";
		if (page / 10 == 0) {
			str_page = "0" + page;
		} else{
			str_page = String.valueOf(page);
		}
	
		tv_page.setText(str_page);
		tv_lec_current.setText(Util.toDisplayTime(item.getPageStudyTime()));
		tv_lec_total.setText("/" + Util.toDisplayTime(item.getPageTotalTime()));
		tv_lec_per.setText(item.getProgress() + "%");

		String filename = classidx + "_" + item.getPart() + ".mp4";
		File dfile = new File(Util.getDownloadPath(filename));
		if (dfile.isFile() && dfile.exists()) {
			iv_down.setImageResource(R.drawable.i_dwn_on);
		} else {
			iv_down.setImageResource(R.drawable.i_dwn_off);
		}
		chk_item.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
			}
		});
		return convertView;
	}

}
