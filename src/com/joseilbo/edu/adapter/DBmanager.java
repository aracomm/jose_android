package com.joseilbo.edu.adapter;

import static android.provider.BaseColumns._ID;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBmanager extends SQLiteOpenHelper{
	//private static final String DATABASE_NAME = "History.db";
	private static final int DTATBASE_VERSION = 1;
	private String databasename ;

	public DBmanager(Context ctx, String dbname){

		super(ctx, dbname, null, DTATBASE_VERSION);
		databasename = dbname;
		// Log.d("EVENTSDATA", "EventsData ");
	}

	@Override
	public void onCreate(SQLiteDatabase db){
		// Log.d("EVENTSDATA", "SQLiteDatabase start");
		if(databasename.equals(databasename))
		{
//			db.execSQL("DROP TABLE IF EXISTS " + "userinfo");
//			db.execSQL("DROP TABLE IF EXISTS " + "device");
//			db.execSQL("DROP TABLE IF EXISTS " + "appinfo");
//			db.execSQL("DROP TABLE IF EXISTS " + "download");
//			db.execSQL("DROP TABLE IF EXISTS " + "lecture_progress");

			db.execSQL("CREATE TABLE " + "userinfo" + 
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+"mno"+" TEXT,"+"passwd"+" TEXT,"+"user_id"+" TEXT, pushchk INTEGER DEFAULT 0, username TEXT, usermode INTEGER DEFAULT 0 );" //0:일반 1:인증서로그인
					);
			db.execSQL("CREATE TABLE " + "device" + 
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+"device_token"+" TEXT );"
					);
			db.execSQL("CREATE TABLE " + "appinfo" + 
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, version TEXT);");                                       
			db.execSQL("CREATE TABLE download ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, classkey VARCHAR, gyear VARCHAR, gnum VARCHAR, part VARCHAR, subject TEXT, filename TEXT, htmlurl TEXT, size VARCHAR, chasi VARCHAR, curri_title TEXT,pagestudytime VARCHAR, pagetotaltime VARCHAR, progress VARCHAR);");
			db.execSQL("CREATE TABLE lecture_progress ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, classkey VARCHAR, gyear VARCHAR, gnum VARCHAR, part VARCHAR, status VARCHAR, playtime VARCHAR, position VARCHAR, page VARCHAR, user_id VARCHAR);");
		
		}
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		// Log.d("EVENTSDATA", "SQLiteDatabase onUpgrade");
		if(databasename.equals(databasename))
		{
			db.execSQL("DROP TABLE IF EXISTS " + "userinfo");
			db.execSQL("DROP TABLE IF EXISTS " + "device");
			db.execSQL("DROP TABLE IF EXISTS " + "appinfo");
			db.execSQL("DROP TABLE IF EXISTS " + "download");
			db.execSQL("DROP TABLE IF EXISTS " + "lecture_progress");

		}
		onCreate(db);

	}
}