package com.joseilbo.edu.adapter;

import java.util.List;

import com.joseilbo.edu.R;
import com.joseilbo.edu.container.CurriculumItem;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.TextView;

public class CurriListAdapter extends BaseExpandableListAdapter {

	private Context mContext;
	private ExpandableListView list;
	private List<CurriculumItem> citem;
	private int[] groupStatus;

	public CurriListAdapter(Context pContext, ExpandableListView pExpandableListView, List<CurriculumItem> pGroupCollection) {
		mContext = pContext;
		citem = pGroupCollection;
		list = pExpandableListView;
		groupStatus = new int[citem.size()];

		setListEvent();
	}

	private void setListEvent() {

		list.setOnGroupExpandListener(new OnGroupExpandListener() {

					
					public void onGroupExpand(int arg0) {
						// TODO Auto-generated method stub
						groupStatus[arg0] = 1;
					}
				});

		list.setOnGroupCollapseListener(new OnGroupCollapseListener() {

					
					public void onGroupCollapse(int arg0) {
						// TODO Auto-generated method stub
						groupStatus[arg0] = 0;
					}
				});
	}

	
	public String getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return citem.get(groupPosition).curriChildren.get(childPosition).curriculum.lesson_subject;
	}

	
	public long getChildId(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {
		// TODO Auto-generated method stub

		ChildHolder childHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.curri_item, null);

			childHolder = new ChildHolder();

			childHolder.tv_subject = (TextView) convertView.findViewById(R.id.tv_subject);
			childHolder.tv_progress = (TextView)convertView.findViewById(R.id.tv_progress);
			childHolder.tv_txt = (TextView)convertView.findViewById(R.id.tv_txt);
			convertView.setTag(childHolder);
		}else {
			childHolder = (ChildHolder) convertView.getTag();
		}
		String txt = mContext.getResources().getString(R.string.curri_study_complete);
		String progress = citem.get(groupPosition).curriChildren.get(childPosition).curriculum.ratio;
		
//		childHolder.tV_progress.setText(progress);// .curriculum.progress+"%");
		if(citem.get(groupPosition).curriChildren.get(childPosition).curriculum.complete_yn == null || citem.get(groupPosition).curriChildren.get(childPosition).curriculum.complete_yn.toUpperCase().equals("N"))
			txt = mContext.getResources().getString(R.string.curri_study_start);
		
		if (progress == null) 
			progress = "0";
			
		childHolder.tv_progress.setText(progress + "%");
		childHolder.tv_txt.setText(txt);
		childHolder.tv_subject.setText(citem.get(groupPosition).curriChildren.get(childPosition).curriculum.lesson_subject);
		return convertView;
	}

	
	public int getChildrenCount(int gPosition) {
		// TODO Auto-generated method stub
		if(citem.get(gPosition).curriChildren!=null)
			return citem.get(gPosition).curriChildren.size();
		else
			return 0;
	}

	
	public Object getGroup(int gPosition) {
		// TODO Auto-generated method stub
		return citem.get(gPosition);
	}

	
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return citem.size();
	}

	
	public long getGroupId(int gPosition) {
		// TODO Auto-generated method stub
		return gPosition;
	}

	
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) 
 {
		// TODO Auto-generated method stub
		GroupHolder groupHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.curri_group_item,
					null);
			groupHolder = new GroupHolder();
			groupHolder.iv_attend = (ImageView) convertView.findViewById(R.id.iv_attend);
			groupHolder.tv_subject = (TextView) convertView.findViewById(R.id.tv_subject);
			groupHolder.iv_folder = (ImageView) convertView.findViewById(R.id.iv_folder);
			convertView.setTag(groupHolder);
		}
		else {
			groupHolder = (GroupHolder) convertView.getTag();
		}
//		if(citem.get(groupPosition).curriculum.time == null ||
//				!citem.get(groupPosition).curriculum.time.equals("2"))
//			groupHolder.iv_attend.setVisibility(View.GONE);
//		else
//			groupHolder.iv_attend.setVisibility(View.VISIBLE);
		groupHolder.iv_attend.setVisibility(View.GONE);
		if (groupStatus[groupPosition] == 0) {
			groupHolder.iv_folder.setImageResource(R.drawable.btn_plus);
		} else {
			groupHolder.iv_folder.setImageResource(R.drawable.btn_minus);
		}
		groupHolder.tv_subject.setText(citem.get(groupPosition).curriculum.subject);

		return convertView;
	}

	class GroupHolder {
		ImageView iv_attend;
		TextView tv_subject;
		ImageView iv_folder;
	}

	class ChildHolder {
		TextView tv_subject;
		TextView tv_progress;
		TextView tv_txt;
	}

	
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return true;
	}
}
