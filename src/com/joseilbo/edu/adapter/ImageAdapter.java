package com.joseilbo.edu.adapter;

import com.joseilbo.edu.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {

		private LayoutInflater mInflater;
		private static final int[] ids = { R.drawable.no_1, R.drawable.no_2, R.drawable.no_3, R.drawable.no_4,
				R.drawable.no_5, R.drawable.no_6 };

		public ImageAdapter(Context context) {
			mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
			return ids.length;
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.image_item, null);
			}
			((ImageView) convertView.findViewById(R.id.imageView)).setImageResource(ids[position]);
			return convertView;
		}

	}


