package com.joseilbo.edu.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;

import com.joseilbo.edu.R;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Util;

/**
 * Adapter 클래스<br>
 * 함수를 호출하면 HTTP 서버로 부터 API 결과를 가져온 후<br>
 * OnResponseListener 인터페이스의 onResponseReceived 콜백함수로 결과를 리턴한다. <br>
 */
public class Adapter implements OnApiAlertListener {

	// Adapter Manager.
	private AdapterManager _am;
	private Handler _handler = new Handler();

	/**
	 * Constructor.
	 */
	public Adapter() {
		_am = new AdapterManager();
	}

	public void close() {
		_am.close();
	}

	/**
	 * shutdownConnection<br>
	 * DefaultHttpClient 의 ConnectionManager 를 종료한다.<br>
	 * 모든 연결이 끊어진다.<br>
	 */
	public void shutdownConnection() {
		_am.shutdownConnectionManager();
	}

	/**
	 * @param url
	 */
	public void setBaseUrl(String url) {
		_am.setBaseUrl(url);
	}

	/**
	 * 로그인
	 * 로그인(수시로)
	 * 콜백함수인 onResponseReceived 의 두번째 인자를 <b>Sign</b> 클래스로 형변환하여 사용해야한다.
	 * @param user_id - 사용자 아이디
	 * @param userpw - 비밀번호
	 * @param udid - 고유값
	 * @param devicetoken - 푸시토큰
	 * @param context
	 * @param callback - onResponseReceived 함수
	 */
	public void login(String userid, String userpw, String udid, String devicetoken, Context context, OnResponseListener callback) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("ID", userid);
		params.put("PWD", userpw);
		params.put("DEVID", devicetoken);
		_am.request(new Params(Api.SIGN, params, context, callback, this, _handler));
	}

	public void curriculum(String num, Context context, OnResponseListener callback) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("NUM", num);
		params.put("ID", CUser.userid);

		_am.request(new Params(Api.CURRICULUM, params, context, callback, this, _handler));
	}

	public void main(Context context, OnResponseListener callback) {
		Map<String, String> params = new HashMap<String, String>();
		_am.request(new Params(Api.MAIN, params, context, callback, this, _handler));
	}

	// jose - begin
	public void lectureListThisMonthOnline(Context context, OnResponseListener callback) {
		Map<String, String> params = new HashMap<String, String>();
		_am.request(new Params(Api.THISMONTHONLINE, params, context, callback, this, _handler));
	}

	public void lectureListThisMonth(Context context, OnResponseListener callback) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
		Date currentTime = new Date();
		String dYear = formatter.format(currentTime);
		formatter = new SimpleDateFormat("MM");
		String dMonth = formatter.format(currentTime);

		Map<String, String> params = new HashMap<String, String>();
		params.put("YEAR", dYear);
		params.put("MON", dMonth);

		_am.request(new Params(Api.THISMONTH, params, context, callback, this, _handler));
	}

	// jose - end
	public void loginout(Context context, OnResponseListener callback) {
		Map<String, String> params = new HashMap<String, String>();
		_am.request(new Params(Api.SIGNOUT, params, context, callback, this, _handler));
	}

	public void useridChk(String userid, Context context, OnResponseListener callback) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("user_id", userid);
		_am.request(new Params(Api.USERIDCHK, params, context, callback, this, _handler));
	}

	public void Version(Context context, OnResponseListener callback) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("platform", Constants.PLATFORM_ANDROID);
		_am.request(new Params(Api.VERSION, params, context, callback, this, _handler));
	}

	public void Push(String userid, String gubun, Context context, OnResponseListener callback) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("user_id", userid);
		params.put("gubun", gubun);
		_am.request(new Params(Api.PUSH, params, context, callback, this, _handler));
	}

	public void OTInfo(Context context, OnResponseListener callback) {
		Map<String, String> params = new HashMap<String, String>();
		_am.request(new Params(Api.OTINFO, params, context, callback, this, _handler));
	}

	/**
	 * 통신장애 Alert 콜백 API 요청 후 통신 장애가 발생하면 이 함수가 호출이 된다.
	 * @param api - 요청한 API 명령
	 * @param params - 요청한 API 파라메터
	 */
	public void onApiAlert(int api, Params params, ApiException ex) {
		if (params != null) {
			try {
				alertYesNo(api, params, ex);
				Util.debug(" >> HTTP response exception code = " + ex.getErrorCode());
				Util.debug(" >> HTTP response exception message = " + ex.getMessage());
			} catch (Exception e) {
			}
		}
	}

	/**
	 * 통신장애 Alert 메시지를 출력한다.
	 * @param api  - 요청한 API 명령
	 * @param params  - 요청한 API 파라메터
	 * @throws Exception
	 */
	public void alertYesNo(final int api, final Params params, ApiException ex)
			throws Exception {

		final Context context = params.getContext();
		String errorstr = "";
		if (ex.getErrorCode() == 1002) {
			Util.ToastMessage(context, "인터넷에 연결할 수 없습니다. 네트워크 불안정으로 학습시간이 정상저장되지\n않을 수 있으니 출석여부를 꼭 확인하세요.");
		} else {
			errorstr = Integer.toString(ex.getErrorCode());
			AlertDialog.Builder dialog = new AlertDialog.Builder(context);
			dialog.setTitle(context.getResources().getString(R.string.app_name))
					.setMessage(ex.getMessage() + "\n" + errorstr)
					.setCancelable(false)
					.setPositiveButton("확인",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.dismiss();
								}
							}).setCancelable(false).show();
		}

	}
}