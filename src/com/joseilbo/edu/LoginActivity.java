package com.joseilbo.edu;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.joseilbo.edu.adapter.Api;
import com.joseilbo.edu.adapter.DBmanager;
import com.joseilbo.edu.adapter.HttpHelper;
import com.joseilbo.edu.adapter.OnResponseListener;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Environments;
import com.joseilbo.edu.common.IntentModelActivity;
import com.joseilbo.edu.common.Util;
import com.joseilbo.edu.container.ApiResult;
import com.joseilbo.edu.container.Version;

public class LoginActivity extends IntentModelActivity implements OnClickListener, OnResponseListener {
	ImageButton btn_login, btn_certlogin;
	EditText edit_id, edit_pwd;
	LoginActivity self = null;
	Context context;
	DBmanager db_manager;
	SQLiteDatabase db;
	OnResponseListener callback;
	String userid = "", userpw = "", device_token = "";
	CookieManager cookieManager;
	HttpHelper hh = new HttpHelper();
	int pushchk = 0;
	
	int activityRequestCode = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		self = this;
		context = this;
		callback = this;
		
		activityRequestCode = getIntent().getIntExtra(Constants.REQUEST_FIELD, -1);
		
		CookieSyncManager.createInstance(this);
		cookieManager = CookieManager.getInstance();
		initializeDebuggable();
		loadEnvironments();
		initialize();
	}

	public void loadEnvironments() {
		Environments.load(this);
		if (!Environments.PREFERENCE_EXIST) {
			Environments.save(this);
		}
	}

	public void initialize() {
		edit_id = (EditText) findViewById(R.id.edit_id);
		edit_pwd = (EditText) findViewById(R.id.edit_pwd);
		edit_id.setOnClickListener(self);
		edit_pwd.setOnClickListener(self);
		edit_id.setText(CUser.userid);
		pushchk = CUser.pushchk;

		btn_login = (ImageButton) findViewById(R.id.btn_login);
		btn_login.setOnClickListener(this);
		((Button) findViewById(R.id.btn_home)).setOnClickListener(self);
		((Button) findViewById(R.id.btn_member)).setOnClickListener(self);
		((Button) findViewById(R.id.btn_findid)).setOnClickListener(self);

		_api.Version(context, callback);

	}

	public void onResponseReceived(int api, Object result) {
		Util.debug("Login result    :   " + result);
		try {
			switch (api) {
			case Api.SIGN:
			case Api.SUPERUSER:
			case Api.USERIDCHK:

				if (result != null) {

					ApiResult ar = (ApiResult) result;
					switch (ar.errorCode) {
					case 0:
						CUser.userid = userid;
						CUser.mno = CUser.userid;
						addEvent();
						break;
					case -1:
						Util.PopupMessage(this, "아이디 입력 없음");
						break;
					case -2:
						Util.PopupMessage(this, "암호 입력 없음");
						break;
					case -3:
						Util.PopupMessage(this, "고유번호 입력 없음");
						break;
					case -4:
						Util.PopupMessage(this, "그외 에러");
						break;
					case -5:
						Util.PopupMessage(this, "등록되지 않은 아이디");
						break;
					case -6:
						Util.PopupMessage(this, "비밀번호가 일치안함");
						break;
					case -7:
						Util.PopupMessage(this, "기계 고유번호가 맞기 않음");
						break;
					}

				} else {
					Util.PopupMessage(this, getResources().getString(R.string.api_http_alert));
				}
				break;
			case Api.VERSION:
				if (result != null) {
					final Version ver = (Version) result;

					if (ver.version.trim().length() > 0) {
						db_manager = new DBmanager(self, Constants.DATABASE_NAME);
						db = db_manager.getWritableDatabase();
						Cursor c = db.query("appinfo", new String[] { "version" }, null, null, null, null, null);
						String str_current = "";
						try {
							if (c.getCount() != 0) {
								c.moveToFirst();
								for (int i = 0; i < c.getCount(); i++) {
									str_current = c.getString(0);
									c.moveToNext();
								}

							} else {
								str_current = Util.getVersion(self);
							}
						} catch (Exception e) {
						}
						c.close();
						Util.debug(str_current);

						if (!str_current.equals(ver.version.trim()) && Double.valueOf(str_current) < Double.valueOf(ver.version.trim())) {
							String alert_str = "현재버전: " + str_current
									+ "\n최신버전: " + ver.version.trim() + "\n\n"
									+ ver.newComment;

							AlertDialog.Builder builder = new AlertDialog.Builder(context);
							builder.setMessage(alert_str);
							builder.setTitle("New Version");
							builder.setPositiveButton("업데이트하기",
									new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int which) {
											if (!ver.link.equals("")) {
												startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ver.link .replace("\\/", "/"))));
											}
										}
									});
							builder.setNegativeButton(null, null);
							AlertDialog dialog = builder.create();
							dialog.setCancelable(false);
							dialog.show();
							ContentValues cv = new ContentValues();
							cv.put("version", ver.version.trim());
							db.update("appinfo", cv, null, null);
						}
						db.close();

					} else {
						Util.ToastMessage(this, "버전 정보 로딩 실패");
					}
				} else {
					Util.PopupMessage(this, getResources().getString(R.string.api_http_alert));
				}
				break;
			}
		} catch (NullPointerException e) {
		}
	}

	@Override
	public void onClick(View v) {
		userid = edit_id.getText().toString();
		userpw = edit_pwd.getText().toString();
		switch (v.getId()) {
		case R.id.btn_home:
			startActivity(new Intent(self, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			finish();
			break;
			
		case R.id.btn_login:
			if (userid.trim().length() == 0)
				Util.ToastMessage(self, getString(R.string.id_null));
			else if (userpw.trim().length() == 0)
				Util.ToastMessage(self, getString(R.string.pwd_null));
			else
				_api.login(userid, userpw, Util.getDeviceID(self), CUser.device_token, context, callback);
			break;
			
		case R.id.btn_member:
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra( "url", Constants.RegistMemeberUrl).putExtra("title", "회원가입"));
			break;
			
		case R.id.btn_findid:
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra( "url", Constants.FindIdPwdUrl).putExtra("title", "아이디/패스워드 찾기"));
			break;
		}
		super.onClick(v);
	}

	public void initializeDebuggable() {
		Constants.debuggable = (0 != (getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE));
	}

	private void addEvent() {
		db_manager = new DBmanager(this, "UserInfo.db");
		db = db_manager.getWritableDatabase();

		String sql = "delete from userinfo";
		db.execSQL(sql);
		CUser.usermode = 0;
		sql = "insert into userinfo ( user_id, passwd,usermode,mno,username,pushchk) values ('"
				+ userid
				+ "', '"
				+ userpw
				+ "', "
				+ CUser.usermode
				+ ",'"
				+ CUser.mno
				+ "','"
				+ CUser.username
				+ "','"
				+ CUser.pushchk
				+ "')";
		db.execSQL(sql);
		ContentValues cv = new ContentValues();
		cv.put("version", Util.getVersion(self));
		Util.debug(Util.getVersion(self));
		db.close();
		CUser.userid = userid;
		CUser.userpw = userpw;
		
		if(activityRequestCode == Constants.REQUEST_ONLINE_ACTIVITY) {
			setResult(RESULT_OK);
			finish();
		} else if(activityRequestCode == Constants.REQUEST_OFFLINE_ACTIVITY) {
			setResult(RESULT_OK);
			finish();
		} else if(activityRequestCode == Constants.REQUEST_SEARCH_ONLINE_ACTIVITY) {
			setResult(RESULT_OK);
			finish();
		} else if(activityRequestCode == Constants.REQUEST_SEARCH_OFFLINE_ACTIVITY) {
			setResult(RESULT_OK);
			finish();
		} else {
			startActivity(new Intent(self, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			finish();
		}
	}

	public void onBackPressed() {
		startActivity(new Intent(self, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		finish();
	}
}
