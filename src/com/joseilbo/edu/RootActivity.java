package com.joseilbo.edu;




import com.joseilbo.edu.R;
import com.joseilbo.edu.adapter.HttpHelper;
import com.joseilbo.edu.adapter.OnResponseListener;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Util;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.Toast;
/**
 * <b>기본 탭뷰화면 </b><br>
 * @author boram han
 */
public class RootActivity extends TabActivity implements OnTabChangeListener, OnTouchListener, OnResponseListener{
	public static RootActivity self = null;
	public static Context context;
	public static TabHost tabHost;
	CookieManager cookieManager;
	HttpHelper hh=new HttpHelper();
	int menu=0;
	int progress_cnt=0;

//	private String url = "";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.root);
//		Intent i=getIntent();
		Bundle bun = getIntent().getExtras();
		if(bun != null)
		{
			if(bun.getInt("menu")>=0)
				menu= bun.getInt("menu"); //탭 메뉴 값
			
//			url = bun.getString("url");
		}
		self = this;		
		context = this;
		initialize();
	}
	
	
	/**
	 * ����Ʈ�� �� Activity �ʱ�ȭ 
	 */
	public void initialize() {
		ImageView menu1 = new ImageView(this);
		menu1.setBackgroundResource(R.drawable.tab01);
		menu1.setScaleType(ScaleType.FIT_XY);
		ImageView menu2 = new ImageView(this);
		menu2.setBackgroundResource(R.drawable.tab02);
		menu2.setScaleType(ScaleType.FIT_XY);
		ImageView menu3 = new ImageView(this);
		menu3.setBackgroundResource(R.drawable.tab03);
		menu3.setScaleType(ScaleType.FIT_XY);
		ImageView menu4 = new ImageView(this);
		menu4.setBackgroundResource(R.drawable.tab04);
		menu4.setScaleType(ScaleType.FIT_XY);
		ImageView menu5 = new ImageView(this);
		menu5.setBackgroundResource(R.drawable.tab05);
		menu5.setScaleType(ScaleType.FIT_XY);
//		ImageView menu6 = new ImageView(this);

		tabHost = getTabHost();
		TabHost.TabSpec spec;
		tabHost.addTab(tabHost.newTabSpec("tab1")
			.setIndicator(new TabView(RootActivity.this, R.drawable.tab01))
			.setContent(new Intent(this, TabGroupActivity.class).putExtra("tab", "tab1").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));
		
		tabHost.addTab(tabHost.newTabSpec("tab2")
			.setIndicator(new TabView(RootActivity.this, R.drawable.tab02))
//			.setContent(new Intent(this, TabGroupActivity.class).putExtra("tab", "tab2").putExtra("url", url).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));			
			.setContent(new Intent(this, TabGroupActivity.class).putExtra("tab", "tab2").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));
		
		tabHost.addTab(tabHost.newTabSpec("tab3")
			.setIndicator(new TabView(RootActivity.this, R.drawable.tab03))
			.setContent(new Intent(this, TabGroupActivity.class).putExtra("tab", "tab3").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));
		
		tabHost.addTab(tabHost.newTabSpec("tab4")
			.setIndicator(new TabView(RootActivity.this, R.drawable.tab04))
			.setContent(new Intent(this, TabGroupActivity.class).putExtra("tab", "tab4").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));
		
		tabHost.addTab(tabHost.newTabSpec("tab5")
			.setIndicator(new TabView(RootActivity.this, R.drawable.tab05))
			.setContent(new Intent(this, TabGroupActivity.class).putExtra("tab", "tab5").addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));
		
			/*
			tabHost.setOnTabChangedListener(new OnTabChangeListener() {
			
			public void onTabChanged(String arg0) {							
			}   
		});
		*/
		tabHost.setCurrentTab(menu);
		tabHost.setOnTabChangedListener(this);

	}
	

	
	public void onTabChanged(String tabId) {
		Log.e("","tabId : " + tabId);
		
		if (tabId.equals("tab1")) {
			Util.debug("tab1");
			//내 강의실 메뉴 클릭 시 로그인이 안되어 있을 때
			if(CUser.mno.equals("")) {
				Toast toast = Toast.makeText(context.getApplicationContext(), "로그인이 필요한 메뉴입니다.", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				finish();
				startActivity(new Intent(self,LoginActivity.class));
				
			}
		} else if (tabId.equals("tab2")) {
			Util.debug("tab2");			
		} else if (tabId.equals("tab3")) {
			Util.debug("tab3");
		} else if (tabId.equals("tab4")) {
			Util.debug("tab4");
		}else if (tabId.equals("tab5")) {
			Util.debug("tab5");
		}
	}
		
	
	private class TabView extends LinearLayout {		
		public TabView(Context ctx, int drawable) {
			super(ctx);	
			
			//화면구분을 위해 background color 를 설정함.
			setBackgroundColor(Color.parseColor("#CCCCCC"));
			
            LinearLayout.LayoutParams params =  new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
            params.gravity = Gravity.CENTER;
            //background color를 보이게 하기 위해 top margin을 2로 설정함.
            params.topMargin = 2;
			
			ImageView iv = new ImageView(ctx);
            iv.setImageResource(drawable);
            iv.setScaleType(ScaleType.FIT_XY);
            iv.setLayoutParams(params);       
            
            setOrientation(LinearLayout.VERTICAL);
            addView(iv);
		}
	}

	public void onResponseReceived(int api, Object obj) {
		// TODO Auto-generated method stub
		
	}



	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		
		return false;
	}
	@Override
	public void onConfigurationChanged (Configuration newConfig)
	{

	super.onConfigurationChanged(newConfig);

	}
}
