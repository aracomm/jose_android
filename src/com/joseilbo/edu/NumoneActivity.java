package com.joseilbo.edu;

import java.util.ArrayList;

import com.joseilbo.edu.R;
import com.joseilbo.edu.adapter.HttpHelper;
import com.joseilbo.edu.adapter.ImageAdapter;
import com.joseilbo.edu.adapter.OnResponseListener;
import com.joseilbo.edu.common.IntentModelActivity;
import com.joseilbo.edu.common.Util;
import com.joseilbo.edu.ui.CircleFlowIndicator;
import com.joseilbo.edu.ui.ViewFlow;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.CookieManager;
import android.widget.ImageButton;
import android.widget.ImageView;

public class NumoneActivity extends IntentModelActivity implements OnClickListener{

	NumoneActivity self;
	Context context;
	OnResponseListener callback;  
	private Handler _handler = new Handler();
	ArrayList<String> list;
	int density=0;
	CookieManager cookieManager;
	HttpHelper hh=new HttpHelper();
	ViewFlow viewFlow;
	CircleFlowIndicator indic;
	ImageAdapter adapter;
	ImageView iv_no;
	int current=0;
	private static final int[] ids = { R.drawable.no_1, R.drawable.no_2, R.drawable.no_3, R.drawable.no_4,
		R.drawable.no_5, R.drawable.no_6 };
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.numone);
		Util.setStrictMode();
		self=this;
		((ImageButton)findViewById(R.id.btn_left)).setOnClickListener(self);
		((ImageButton)findViewById(R.id.btn_close)).setOnClickListener(self);
		((ImageButton)findViewById(R.id.btn_right)).setOnClickListener(self);
		iv_no=(ImageView)findViewById(R.id.iv_no);
		iv_no.setImageResource(ids[current]);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		_api.main(context, callback);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		super.onClick(view);
		switch(view.getId())
		{
		case R.id.btn_close:
			onBackPressed();
			break;
		case R.id.btn_left:
			if(current>0)
			{
				current--;
				iv_no.setImageResource(ids[current]);

			}
			break;
		case R.id.btn_right:
			if(current<ids.length-1)
			{
				current++;
				iv_no.setImageResource(ids[current]);
			}
			break;
		}
	}



	@Override
	public void onBackPressed() 
	{
		finish();
		return;
	}
	
}
