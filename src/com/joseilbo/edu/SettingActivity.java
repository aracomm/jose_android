package com.joseilbo.edu;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joseilbo.edu.adapter.Api;
import com.joseilbo.edu.adapter.DBmanager;
import com.joseilbo.edu.adapter.OnResponseListener;
import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Environments;
import com.joseilbo.edu.common.Util;
import com.joseilbo.edu.container.Version;

public class SettingActivity extends XActivity implements OnResponseListener, OnClickListener {
	SettingActivity self = null;

	Cursor cursor;
	String uid, url;
	WebView mWebView;
	ImageButton btn_home, btn_logout;
	CheckBox chk_3g;
	AlertDialog alert;
	protected static WaitDialog dialog;
	Button btn_update, btn_login_info;
	String update_url = "", str_current = "";
	TextView tv_id, tv_version;
	DBmanager db_manager;
	SQLiteDatabase db;
	Context context;
	OnResponseListener callback;
	int progress_cnt = 0;
	RelativeLayout titlebar;

	public void onCreate(Bundle savedInstanceState) {
		self = this;
		context = this; 
		callback = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		setTitle("설정");
		
		//setTitleButton((ImageButton) findViewById(R.id.leftImageButton), R.drawable.btn_home_on2);
		setTitleButton((ImageButton) findViewById(R.id.rightImageButton), R.drawable.btn_close);
		
		btn_login_info = (Button) findViewById(R.id.btn_login_info);
		btn_update = (Button) findViewById(R.id.btn_update);
		tv_id = (TextView) findViewById(R.id.tv_id);
		tv_version = (TextView) findViewById(R.id.tv_version);
		
		str_current = Util.getVersion(self);
		tv_version.setText(str_current);
		
		btn_update.setOnClickListener(self);
		btn_login_info.setOnClickListener(self);
		
		loadDatabase();
	}

	public void initialize() {
		chk_3g = (CheckBox) findViewById(R.id.chk_3g);
		if (CUser.pushchk == 1) {
			chk_3g.setChecked(true);
		} else {
			chk_3g.setChecked(false);
		}

		chk_3g.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				db_manager = new DBmanager(self, Constants.DATABASE_NAME);
				db = db_manager.getWritableDatabase();
				ContentValues cv = new ContentValues();
				int chk = 0;
				if (isChecked) {
					chk = 1;
				} else {
					chk = 0;
				}
				Util.debug("" + chk);
				cv.put("pushchk", chk);
				db.update("userinfo", cv, null, null);
				db.close();
				CUser.pushchk = chk;
				
				Environments.ALLOW_3G = isChecked;
				Environments.save(context);
			}
		});
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btn_login_info:
			if (CUser.mno.equals("")) {
				startActivity(new Intent(self, LoginActivity.class));
				finish();
			}
			break;
		case R.id.rightImageButton:
			finish();
			break;
		case R.id.leftImageButton:
			finish();
			break;
		case R.id.btn_update:
			if (((String) ((TextView) findViewById(R.id.tv_verstate)).getText()).trim().equals("최신버전:")) {
				if (update_url.length() > 0) {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(update_url.replace("\\/", "/"))));
				}
			}

			break;
		}
	}

	public void logout() {
		db = db_manager.getWritableDatabase();

		CUser.mno = "";
		String sql = "delete from userinfo";
		db.execSQL(sql);
		Environments.SAVE_ACCOUNT_INFO = false;
		Environments.save(self);
		db.close();
		finish();
		startActivity(new Intent(self, LoginActivity.class));
	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();
		
		_api.Version(context, callback);
		super.onResume();

	}

	public void loadDatabase() {
		db_manager = new DBmanager(this, "UserInfo.db");
		db = db_manager.getReadableDatabase();
		Cursor userinfo_cursor = db.query("userinfo", new String[] { "user_id",
				"pushchk" }, null, null, null, null, null);
		try {
			Util.debug("db count=" + userinfo_cursor.getCount());
			if (userinfo_cursor.getCount() != 0) {
				userinfo_cursor.moveToFirst();

				CUser.pushchk = userinfo_cursor.getInt(1);
				tv_id.setText(userinfo_cursor.getString(0).toString());
				userinfo_cursor.moveToNext();
			}
		} catch (Exception e) {
		}
		initialize();
	}

	public void onResponseReceived(int api, Object result) {
		switch (api) {
		case Api.VERSION: // 버전 정보
			if (result == null) {
				Util.PopupMessage(self, getResources().getString(R.string.api_http_alert));
				return;
			}

			Version ver = (Version) result;
			if (ver.version.trim().length() < 1) {
				Util.ToastMessage(self, "버전 정보 로딩 실패");
				return;
			}

			if (ver.link.trim().length() > 0) {
				update_url = ver.link.replace("\\/", "/");
			}

			if (Float.parseFloat(ver.version.trim()) > Float.parseFloat(str_current)) {
				tv_version.setText(ver.version);
				((TextView) findViewById(R.id.tv_verstate)).setText("최신버전: ");
			} else {
				tv_version.setText(str_current);
				((TextView) findViewById(R.id.tv_verstate)).setText("현재버전: ");
			}
			break;
		/*
		 * case Api.PUSH:
		 * 
		 * if (result != null) {
		 * 
		 * ApiResult push = (ApiResult)result;
		 * 
		 * if (push.result.contains("OK")) {
		 * Environments.ALLOW_PUSH=chk_alarm.isChecked();
		 * Environments.save(self);
		 * 
		 * } else { Util.ToastMessage(self, push.message);
		 * 
		 * }
		 * 
		 * } else { // 수신, 파싱 오류 Util.PopupMessage(self,
		 * getResources().getString(R.string.api_http_alert)); } break;
		 */
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		finish();
	}
}
