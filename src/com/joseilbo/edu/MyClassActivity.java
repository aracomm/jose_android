package com.joseilbo.edu;

import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Util;

public class MyClassActivity extends XActivity {
	Cursor cursor;
	String uid, _url;

	WebView mWebView;
	private final Handler handler = new Handler();
	ImageButton btn_write, btn_help, btn_list;
	AlertDialog alert;

	private Map<String, Object> map = new HashMap<String, Object>();

	protected static WaitDialog progressDialog;
	MyClassActivity self = null;
	String menu;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self = this;
		setContentView(R.layout.titlewebview3);
		
		//event assign
		((ImageView) findViewById(R.id.iv_home)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_study_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_call_upper_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_setting_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_search_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_introduce_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_login_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_notice_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_call_main2)).setOnClickListener(self);

		//login check
		if (CUser.mno.equals("")) {
			((ImageView) findViewById(R.id.btn_login_main2)).setImageResource(R.drawable.footer_btn2);
		} else {
			((ImageView) findViewById(R.id.btn_login_main2)).setImageResource(R.drawable.footer_btn3);
		}

		//myclass url
		_url = Constants.MyClassUrl + "?mem_id=" + CUser.userid + "&is_part=on";
		
		//init myclass web
		initWebview();
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.iv_home:
			startActivity(new Intent(self, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			finish();
			break;
		
		case R.id.btn_study_main2:
			break;
			
		case R.id.btn_call_upper_main2:
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.tel_guestcenter)));
			break;
			
		case R.id.btn_setting_main2:
			startActivity(new Intent(this, SettingActivity.class));
			break;
			
		case R.id.btn_search_main2:
			String _searchUrl = Constants.CourseSearchUrl + "?mem_id=" + CUser.userid;
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra( "url", _searchUrl).putExtra("title", "검색"));
			break;
			
		case R.id.btn_introduce_main2:
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra("url", Constants.MobileOfficeIntro).putExtra("title", "재무교육원 소개"));
			break;
			
		case R.id.btn_login_main2:
			if (CUser.mno.equals("")) {
				startActivity(new Intent(this, LoginActivity.class));
			} else {
				Util.alertYesNo(self, R.string.app_name, R.string.app_logout, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								finish();
								(new Handler()).post(new Runnable() {
									public void run() {
										CUser.username = "";
										CUser.userpw = "";
										CUser.mno = "";
										CUser.userid = "";
										logout();
										
										((ImageView) findViewById(R.id.btn_login_main2)).setImageResource(R.drawable.footer_btn2);
									}
								});
							}
						}, null);
			}
			break;
			
		case R.id.btn_notice_main2:
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra( "url", Constants.NoticeListUrl).putExtra("title", "공지사항"));
			break;
			
		case R.id.btn_call_main2:
			startActivity(new Intent(this, SupportActivity.class));
			break;
		}
	}

	public void initWebview() {
		loadWebView(_url);
	}

	public void putAll(Map<? extends String, ? extends Object> arg0) {
		map.putAll(arg0);
	}

	private void loadWebView(String url) {
		mWebView = (WebView) findViewById(R.id.webview);
		mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.addJavascriptInterface(new AndroidBridge(), "android");
		mWebView.setHorizontalScrollBarEnabled(true); // 세로 scroll 제거
		mWebView.setVerticalScrollBarEnabled(false); // 가로 scroll 제거
		mWebView.getSettings().setBuiltInZoomControls(false);
		mWebView.setWebViewClient(new webViewClient());
		mWebView.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onJsConfirm(WebView view, String url,
					String message, final android.webkit.JsResult result) {
				new AlertDialog.Builder(self)
						.setTitle(getText(R.string.app_name))
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										result.confirm();
									}
								})
						.setNegativeButton(android.R.string.cancel,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										result.cancel();
									}
								}).create().show();

				return true;
			};

			public boolean onJsAlert(WebView view, String url,
					final String message, JsResult result) {
				final JsResult r = result;
				if (null == self)
					return false;

				new AlertDialog.Builder(self)
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										if (message.contains("로그인") || message.contains("Session")) {
											CUser.mno = "";
											mWebView.stopLoading();
											logout();
										}
										r.confirm();
									}
								}).setCancelable(false).create().show();
				return true;
			}

			public void onProgressChanged(WebView view, int newProgress) {
			}
		});
		
		mWebView.setDownloadListener(new DownloadListener() {
			public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
				startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url)));
			}
		});

		mWebView.loadUrl(url);
	}

	private class webViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.startsWith("tel:")) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
			} else {
				if (url.contains("#")) {
				}
				_url = url;
				view.loadUrl(url);
			}
			return true;
		}

		public void onLoadResource(WebView view, String url) {
			try {
				if (progressDialog == null) {
					progressDialog = WaitDialog.show(self, "", "", true,
							true, null);
				}
			} catch (Exception e) {
			}

			(new Handler()).postDelayed(new Runnable() {
				public void run() {
					try {
						if (progressDialog != null
								&& progressDialog.isShowing()) {
							progressDialog.dismiss();
							progressDialog = null;
						}
					} catch (Exception e) {
					}
				}
			}, 2000);
		}

		public void onPageFinished(WebView view, String url) {
			try {
				if (progressDialog != null && progressDialog.isShowing()) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (url.endsWith(".mp4")) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					Uri uri = Uri.parse(url);
					i.setDataAndType(uri, "video/mp4");
					startActivity(i);
				}
			} catch (Exception e) {
			}
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Util.ToastMessage(self, getResources().getString(R.string.api_http_alert));
		}
	}

	public void onStart() {
		super.onStart();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();

		super.onResume();
		mWebView.reload();
	}

	private class AndroidBridge {
		@JavascriptInterface
		public void callAndroid(final String arg) {
			handler.post(new Runnable() {
				public void run() {
					if (arg.equals("home")) {
						finish();
					}
				}
			});
		}

		@JavascriptInterface
		public void moveClass(final String mem_id, final String lec_num) {
			handler.post(new Runnable() {
				public void run() {
					if (!CUser.mno.equals("")) {
						startActivity(new Intent(self, MyCurriculumActivity.class).putExtra("num", lec_num));
					} else {
						Util.alertOk(self, R.string.login_info);
					}
				}
			});
		}

		@JavascriptInterface
		public void go_home() {
			handler.post(new Runnable() {
				public void run() {
					finish();
				}
			});
		}
	}

	@Override
	public void onBackPressed() {
		if (!mWebView.canGoBack()) {
			finish();
		} else {
			mWebView.goBack();
		}
		return;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
