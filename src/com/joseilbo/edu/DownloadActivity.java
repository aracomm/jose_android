package com.joseilbo.edu;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joseilbo.edu.adapter.DBmanager;
import com.joseilbo.edu.adapter.DownListAdapter;
import com.joseilbo.edu.adapter.HttpHelper;
import com.joseilbo.edu.adapter.OnResponseListener;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Util;
import com.joseilbo.edu.container.LectureItem;

public class DownloadActivity extends XActivity implements OnResponseListener {
	Cursor cursor;
	String classkey = "", title = "", part = "", filename = "";
	AlertDialog alert;
	DownloadActivity self = null;
	CookieManager cookieManager;
	HttpHelper hh = new HttpHelper();
	ListView list;
	DownListAdapter dadapter;
	ArrayList<LectureItem> lec_list, down_list;
	OnResponseListener callback;
	Context context;
	boolean mode_edit = false;
	boolean mode_all = false;
	boolean mode_complete = false;
	RelativeLayout layout_edit;
	ImageButton btn_all, btn_del;
	int chk_cnt = 0, down_cnt = 0, chk_total = 0;
	long totalsize = 0;
	Dialog dialog;
	TextView tv_total;
	TextView tv_subject;
	TextView tv_progress, tv_result;
	ProgressBar down_progress, total_progress;
	private static final int CONNECTIVITY_MSG = 0;
	private static final int PHONE_STATE_MSG = 1;
	private static final int DIALOG_PROGRESS = 0;
	int playsize = 0;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self = this;
		context = getParent();
		callback = this;
		setContentView(R.layout.download);
		setTitle("다운로드");
		setTitleButton(((ImageButton) findViewById(R.id.leftImageButton)),
				R.drawable.btn_home);
		setTitleButton(((ImageButton) findViewById(R.id.rightImageButton)),
				R.drawable.btn_file);
		layout_edit = (RelativeLayout) findViewById(R.id.layout_edit);
		list = (ListView) findViewById(R.id.list);
		list.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				CheckBox checkbox = (CheckBox) view.getTag(R.id.chk_item);
				if (mode_edit) {
					checkbox.setChecked(!checkbox.isChecked());
					lec_list.get(position).setSelected(checkbox.isChecked());
					countChk(lec_list.get(position).isSelected());
				} else {
					// Intent i=new Intent(self,VideoPlayActivity.class);
					// i.putExtra("subject",lec_list.get(position).getSubject());
					// i.putExtra("part", lec_list.get(position).getPart());
					// i.putExtra("current", 0);
					// i.putExtra("classkey",
					// lec_list.get(position).getClasskey());
					// i.putExtra("gisuYear",
					// lec_list.get(position).getGYear());
					// i.putExtra("gisuNum", lec_list.get(position).getGNum());
					// startActivity(i);
				}
			}

		});
		lec_list = new ArrayList<LectureItem>();
		down_list = new ArrayList<LectureItem>();
		btn_all = (ImageButton) findViewById(R.id.btn_all);
		btn_all.setOnClickListener(self);
		btn_del = (ImageButton) findViewById(R.id.btn_del);
		btn_del.setOnClickListener(self);
		btn_del.setEnabled(false);
		btn_del.setClickable(false);
		String totalMem = "", availableMem = "";
		long total, available;
		String pattern = "#####.##";
		DecimalFormat dformat = new DecimalFormat(pattern);
		if (Util.canUseExternalMemory()) {
			available = Util.getExteranlMemoryAvailableSize();
			total = Util.getExteranlMemoryTotal();
			Util.debug("" + ((double) ((double) available / Util.GigaBytes)));
			if (available / Util.GigaBytes != 0)
				availableMem = dformat
						.format(((double) ((double) available / Util.GigaBytes)))
						+ "GB";
			else if (available / Util.MegaBytes != 0)
				availableMem = dformat
						.format(((double) ((double) available / Util.MegaBytes)))
						+ "MB";
			else
				availableMem = dformat
						.format(((double) ((double) available / Util.KiloBytes)))
						+ "KB";

			// if(total/Util.GigaBytes!=0)
			// {
			// totalMem=dformat.format(((double)((double)total/Util.GigaBytes)))+"GB";
			//
			// }
			// else if(total/Util.MegaBytes!=0)
			// totalMem=dformat.format(((double)((double)total/Util.MegaBytes)))+"MB";
			// else
			// totalMem=dformat.format(((double)((double)total/Util.KiloBytes)))+"KB";

		} else {

			available = Util.getInternalMemoryAvailableSize();
			total = Util.getInternalMemoryTotalSize();

			if (available / Util.GigaBytes != 0)
				availableMem = dformat
						.format(((double) ((double) available / Util.GigaBytes)))
						+ "GB";
			else if (available / Util.MegaBytes != 0)
				availableMem = dformat
						.format(((double) ((double) available / Util.MegaBytes)))
						+ "MB";
			else
				availableMem = dformat
						.format(((double) ((double) available / Util.KiloBytes)))
						+ "KB";

			// if(total/Util.GigaBytes!=0)
			// totalMem=dformat.format(((double)((double)total/Util.GigaBytes)))+"GB";
			// else if(total/Util.MegaBytes!=0)
			// totalMem=dformat.format(((double)((double)total/Util.MegaBytes)))+"MB";
			// else
			// totalMem=dformat.format(((double)((double)total/Util.KiloBytes)))+"KB";
		}
		((TextView) findViewById(R.id.tv_available)).setText("[남은용량: "
				+ availableMem + "]");

		loadDatabase();
	}

	private void loadDatabase() {
		// TODO Auto-generated method stub
		db_manager = new DBmanager(self, "UserInfo.db");
		db = db_manager.getReadableDatabase();
		String sql = "SELECT * FROM download ";
		Cursor cursor = db.rawQuery(sql, null);

		try {
			if (cursor.getCount() != 0 && cursor.moveToFirst()) {
				do {
					String classkey = cursor.getString(cursor
							.getColumnIndex("classkey"));
					String subject = cursor.getString(cursor
							.getColumnIndex("subject"));
					String part = cursor.getString(cursor
							.getColumnIndex("part"));
					String filename = cursor.getString(cursor
							.getColumnIndex("filename"));
					String htmlurl = cursor.getString(cursor
							.getColumnIndex("htmlurl"));
					String size = cursor.getString(cursor
							.getColumnIndex("size"));
					String curri_title = cursor.getString(cursor
							.getColumnIndex("curri_title"));
					String gisuYear = cursor.getString(cursor
							.getColumnIndex("gyear"));
					String gisuNum = cursor.getString(cursor
							.getColumnIndex("gnum"));
					String chasi = cursor.getString(cursor
							.getColumnIndex("chasi"));
					if (filename.contains(CUser.userid)) {
						LectureItem item = new LectureItem();
						item.setDownLecture(part, "", subject, "", "", htmlurl,
								size, classkey, filename, curri_title,
								gisuYear, gisuNum, chasi);
						lec_list.add(item);
						playsize += Integer.valueOf(size);
					}
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
		}
		cursor.close();
		if (db != null)
			db.close();

		initialize();
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.leftImageButton:
			onBackPressed();
			break;
		case R.id.rightImageButton:
			mode_edit = !mode_edit;
			dadapter.setMode(mode_edit);
			if (mode_edit) {
				setTitleButton(
						((ImageButton) findViewById(R.id.rightImageButton)),
						R.drawable.btn_finish);
				layout_edit.setVisibility(View.VISIBLE);
			} else {

				setTitleButton(
						((ImageButton) findViewById(R.id.rightImageButton)),
						R.drawable.btn_file);
				layout_edit.setVisibility(View.GONE);
			}
			dadapter.notifyDataSetChanged();

			break;
		case R.id.btn_all:
			mode_all = !mode_all;
			chk_cnt = 0;
			if (mode_all) {
				// dadapter.setAllChecked(true);
				for (LectureItem item : lec_list) {
					item.setSelected(true);
					countChk(true);
				}
				dadapter.notifyDataSetChanged();
				btn_all.setImageResource(R.drawable.btn_desel);
			} else {
				for (LectureItem item : lec_list) {
					item.setSelected(false);
					countChk(false);

				}
				dadapter.notifyDataSetChanged();
				btn_all.setImageResource(R.drawable.btn_all);

			}

			break;

		case R.id.btn_del:
			Util.alert(getParent(), "다운로드 삭제", "삭제하시겠습니까?", "확인", "취소",
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							makeChkList(false);

						}
					}, null);
			break;
		}

	}

	public void countChk(boolean ischk) {

		if (ischk) {
			chk_cnt++;
		} else {
			if (chk_cnt > 0)
				chk_cnt--;
		}
		if (chk_cnt == 0) {
			btn_del.setImageResource(R.drawable.btn_del_unable);
			btn_del.setEnabled(false);
			btn_del.setClickable(false);
			btn_all.setImageResource(R.drawable.btn_all);
		} else {
			btn_del.setImageResource(R.drawable.btn_del);
			btn_del.setEnabled(true);
			btn_del.setClickable(true);

			if (lec_list.size() == chk_cnt) {
				mode_all = true;
				btn_all.setImageResource(R.drawable.btn_desel);

			} else {
				mode_all = false;
				btn_all.setImageResource(R.drawable.btn_all);
			}

		}

	}

	public void initialize() {
		// TODO Auto-generated method stub
		dadapter = new DownListAdapter(self, lec_list);
		list.setItemsCanFocus(false);
		list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		list.setAdapter(dadapter);
		if (lec_list.size() == 0) {
			btn_all.setClickable(false);
			View v = LayoutInflater.from(getParent()).inflate(
					R.layout.empty_cell, null);
			// if(list.getFooterViewsCount()==0)
			list.addFooterView(v);
		}

		String totalMem = "";
		String pattern = "#####.##";
		DecimalFormat dformat = new DecimalFormat(pattern);
		if (playsize / Util.KiloBytes != 0) {
			totalMem = dformat
					.format(((double) ((double) playsize / Util.KiloBytes)))
					+ "GB";

		} else {
			totalMem = playsize + "MB";

		}
		((TextView) findViewById(R.id.tv_total)).setText(totalMem);

	}

	public void onStart() {

		super.onStart();

		CookieSyncManager.createInstance(this);

	}

	@Override
	public void onPause() {

		super.onPause();

		CookieSyncManager.getInstance().stopSync();

	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();
		
		if (CUser.dupe) {
			CUser.dupe = false;
			finish();
			((Activity) getParent()).startActivity(new Intent(self, LoginActivity.class));
		}
		
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		finish();
		((Activity) getParent()).startActivity(new Intent(self, MainActivity.class));
		return;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onResponseReceived(int api, Object result) {
	}

	public void makeChkList(boolean isdown) {
		int size = lec_list.size();
		for (int i = size - 1; i >= 0; i--) {
			if (lec_list.get(i).isSelected()) {
				delDownload(lec_list.get(i).getFilename());
				lec_list.remove(i);
			}
		}
		
		clearDown();
		Util.ToastMessage(getParent(), "삭제되었습니다.");
		dadapter.notifyDataSetChanged();
	}

	public void delDownload(String filename) {
		db_manager = new DBmanager(self, "UserInfo.db");
		db = db_manager.getWritableDatabase();
		String sql = "SELECT * FROM download  WHERE filename=?";
		Cursor cursor = db.rawQuery(sql, new String[] { filename });

		try {
			if (cursor.getCount() != 0) {
				String dsql = "Delete FROM download  WHERE filename='" + filename + "'";
				db.execSQL(dsql);
			}
		} catch (Exception e) {
		}
		cursor.close();

		if (db != null)
			db.close();

		File file = new File(Util.getDownloadPath(filename));
		if (file.isFile() && file.exists()) {
			if (file.delete()) {
			}
		}
	}

	public void clearDown() {
		down_cnt = 0;
		chk_cnt = 0;
	}
}
