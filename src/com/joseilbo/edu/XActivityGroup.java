package com.joseilbo.edu;

import java.util.ArrayList;

import com.joseilbo.edu.R;
import com.joseilbo.edu.adapter.HttpHelper;
import com.joseilbo.edu.common.Util;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.CookieManager;

public class XActivityGroup extends ActivityGroup implements OnTouchListener {
	public ArrayList<View> history;
	public XActivityGroup group;
	public static Bitmap displayBitmap;
	private Uri mImageCaptureUri;
	CookieManager cookieManager;
	HttpHelper hh=new HttpHelper();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		history = new ArrayList<View>();
		group = this;
		RootActivity.tabHost.getCurrentTabView().setOnTouchListener(this); // 현재 탭 다시 터치 시 새로고침

	}

	/**
	 * ViewChange
	 * @param v
	 */
	public void changeView(View v)  {
		history.remove(history.size()-1);
		history.add(v);
		setContentView(v);
	}
	
	/**
	 * History add View
	 * @param v request visible view
	 */
	public void addView(View v) {		
		history.add(v);  
		setContentView(v);
	} 
	
	/**
	 * Clear history
	 * @param v
	 */
	public void clearTopView(View v) {
		history.clear();
		history.add(v);
	}
	
	/**
	 * Reload View
	 */
	public void reloadTopView() {
		if (history.size() > 0) {
			setContentView(history.get(history.size()-1));
		}
	}
	/**
	 * Create View
	 */
	public void createView() {
		 View v = history.get(history.size()-1);
		    XActivity currentActivity = (XActivity)v.getContext();
		    currentActivity.onCreate(null);
	} 
	public void back() {	
		Log.d("jose", "size: "+history.size());
		if (history.size() > 1) {			
			history.remove(history.size()-1);
			setContentView(history.get(history.size()-1));		
			onResume();
		} else {
			Util.alertYesNo(getParent(), R.string.app_name, R.string.app_exit, 

					new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {					
					/*
					 * Application exit.
					 */
					finish();
					(new Handler()).post(new Runnable() {
						public void run() {
//							CUser.uid="";
//							CUser.idx="";
//							CUser.userid="";
							cookieManager=CookieManager.getInstance();
							cookieManager.removeAllCookie();
							hh.clearCookie();
							//	    							CookieSyncManager.getInstance().sync();
							
						}
					});
				}
			}, null);		
			}
	} 
	
	public void clear() {
		history.clear();
	}
	
	@Override 
	public void onBackPressed() { 
		 int length = history.size();
		  
		  View v = history.get(history.size()-1);
		    XActivity currentActivity = (XActivity)v.getContext();
		    currentActivity.onBackPressed();
		 
		return;
	}
	 @Override
	  protected void onResume() {
	    super.onResume();
	    // call current activity's onResume()
	    View v = history.get(history.size()-1);
	    XActivity currentActivity = (XActivity)v.getContext();
//	    currentActivity.onResume();
	  }
	 
		public boolean onTouch(View v, MotionEvent e) {
			// TODO Auto-generated method stub
			if (e.getAction()==MotionEvent.ACTION_DOWN)
		    {

				 int length = history.size();
				    if (length > 1)
				    {
				    	View view = history.get(0);
				    	history.clear();
				    	history.add(view);
						setContentView(view);
					
					    Log.d("jose","size: "+history.size());

				    }

		    }

			return false;
		}
		@Override
		 protected void onActivityResult(int requestCode, int resultCode, Intent data)
		 {
			 if(resultCode != RESULT_OK)
			 {
				 return;
			 }
			 BitmapFactory.Options options = new BitmapFactory.Options();
			 options.inJustDecodeBounds=true;
		    Uri mImageCaptureUri = data.getData();
		    String []proj={MediaStore.Images.Media.DATA};
		    Cursor cursor=managedQuery(mImageCaptureUri,proj,null,null,null);
		    int column_index=cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		    cursor.moveToFirst();
		    Uri photoUri=Uri.parse(cursor.getString(column_index));
		    String uploadImagePath=photoUri.getEncodedPath();	  
		    displayBitmap = BitmapFactory.decodeFile(uploadImagePath, options);

		    options=Util.getBitmapSize(options);
		    displayBitmap=BitmapFactory.decodeFile(uploadImagePath, options);
		    Bitmap resized=Util.ResizeBitmap(displayBitmap);
		    displayBitmap=resized;
		    View view = history.get(history.size()-1);
		    XActivity currentActivity = (XActivity)view.getContext();
		    currentActivity.onResume(); 

		    
		    
		 }
}
