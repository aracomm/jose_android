package com.joseilbo.edu;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.webkit.CookieSyncManager;

import com.google.android.gcm.GCMRegistrar;
import com.joseilbo.edu.adapter.Api;
import com.joseilbo.edu.adapter.DBmanager;
import com.joseilbo.edu.adapter.OnResponseListener;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Environments;
import com.joseilbo.edu.common.IntentModelActivity;
import com.joseilbo.edu.common.Util;
import com.joseilbo.edu.container.ApiResult;

public class IntroActivity extends IntentModelActivity implements OnResponseListener{
	DBmanager db_manager;	
	SQLiteDatabase db;	
	String id="",passwd="",device_token="",siteid="";
	Context context;
	OnResponseListener callback;  
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.intro);
		CookieSyncManager.createInstance(this);
		context = this;
		callback = this;
//		GCMRegistration_id();
//		CUser.device_token = "6E38D59D-4906-4C04-9372-780203DE6D97";	// jose - temporary value setting
		CUser.device_token = Util.getDeviceSerialNumber();
		initialize();
		start();
	}
	public void start() {
//		startPushService();
		(new Handler()).postDelayed(new Runnable() {
			public void run() {		
				if(!CUser.userid.equals("")) {
					_api.login(CUser.userid, CUser.userpw, Util.getDeviceID(self),CUser.device_token, context, callback);
				}
				else {
					goMain();
				}
			}
		}, 2000);
	}

	public void goLogin() {
		Intent i=new Intent(this,LoginActivity.class);
		startActivity(i);
		finish();
	}
	public void goMain() {
   	 Intent i = new Intent(this,MainActivity.class);
		startActivity(i);
		finish();
	}
	public void initialize() {
		initializeDebuggable();

		loadEnvironments();
		DBmanager db_manager= new DBmanager(this,Constants.DATABASE_NAME);	
		SQLiteDatabase db=db_manager.getReadableDatabase();
		Cursor tw_cursor = db.query("userinfo", new String[] {"mno","user_id","passwd","pushchk","username","usermode"}, null, null, null, null, null);

		try {
			if (tw_cursor.getCount()!= 0) {
				tw_cursor.moveToFirst();
				for(int i=0; i<tw_cursor.getCount();i++) {
					CUser.mno=tw_cursor.getString(0);
					CUser.userid=tw_cursor.getString(1);
					CUser.userpw=tw_cursor.getString(2);
					tw_cursor.moveToNext();
				}
			}
		} catch(Exception e) {	    		
		}

		tw_cursor.close(); 
		if(db!=null)
			db.close();	
	}
	
	public void GCMRegistration_id() {
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		final String regId = GCMRegistrar.getRegistrationId(this);

		if (regId.equals("") || regId ==null) {
			GCMRegistrar.register(this, Constants.senderID);
        }  else {
			CUser.device_token=regId;
			System.out.println("registration_id complete!!");
			db_manager = new DBmanager(context,"UserInfo.db");
			db = db_manager.getWritableDatabase();
			String sql = "delete from device";
			db.execSQL(sql);
			sql = "insert into device (device_token) values('"+regId+"')";
			db.execSQL(sql);
			db.close();
		}
	}

	public void startPushService() {
		Intent registrationIntent = new Intent("com.google.android.c2dm.intent.REGISTER");
		registrationIntent.putExtra("app", PendingIntent.getBroadcast(this, 0, new Intent(), 0));
		registrationIntent.putExtra("sender", "365258868387");
		startService(registrationIntent); 
	}
	
	public void initializeDebuggable() {
		Constants.debuggable = (0 != (getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE));
	}
	
	public void loadEnvironments() {
		Environments.load(this);    	
		if (!Environments.PREFERENCE_EXIST) {
			Environments.save(this);
		}
	}

	public void onResponseReceived(int api, Object result) {
		switch(api) {
		// 로그인
		case Api.SIGN :
			if (result != null) {
				ApiResult signIn = (ApiResult)result;
				switch(signIn.errorCode) {
				case 0:
					CUser.mno = CUser.userid;
					break;
				case -1:
					Util.PopupMessage(this, "아이디 입력 없음");
					break;
				case -2:
					Util.PopupMessage(this, "암호 입력 없음");
					break;
				case -3:
					Util.PopupMessage(this, "고유번호 입력 없음");
					break;
				case -4:
					Util.PopupMessage(this, "그외 에러");
					break;
				case -5:
					Util.PopupMessage(this, "등록되지 않은 아이디");
					break;
				case -6:
					Util.PopupMessage(this, "비밀번호가 일치안함");
					break;
				case -7:
					Util.PopupMessage(this, "기계 고유번호가 맞기 않음");
					break;
				}
				// 자동 로그인이 성공해도 실패해도 일단 Main 화면으로 이동한다.
				// password가 휴대폰 말고 PC 인터넷으로 변경되었을 경우, 자동 로그인이 실패할 수 있다.
				goMain();
			} else {
				Util.PopupMessage(this, getResources().getString(R.string.api_http_alert));
			}			
			break; 
		}		
	}
}