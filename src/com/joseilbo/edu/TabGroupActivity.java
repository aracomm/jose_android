package com.joseilbo.edu;

import java.util.UUID;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;

public class TabGroupActivity extends XActivityGroup {
	public static TabGroupActivity tab1 = null;
	public static TabGroupActivity tab2 = null;
	public static TabGroupActivity tab3 = null;
	public static TabGroupActivity tab4 = null;
	public static TabGroupActivity tab5 = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		tab1 = this;
		tab2 = this;
		tab3 = this;
		tab4 = this;
		tab5 = this;

		initialize();		
	}
	
	private void initialize() {
		boolean cleartop = false;
		Intent intent = getIntent();
		String tab = intent.getExtras().getString("tab");
		String url = intent.getExtras().getString("url");
		
		if (tab.equals("tab1")) {
			intent = new Intent(this, MyClassActivity.class);
			tab1 = this;
		} else if (tab.equals("tab2")) {			
			intent = new Intent(this, OnlineClassActivity.class);
			intent.putExtra("mem_id", CUser.username);
			intent.putExtra("is_part", "on");
			intent.putExtra("url", url);
			tab2 = this;
		} else if (tab.equals("tab3")) {
			intent = new Intent(this, OfflineClassActivity.class);
			intent.putExtra("mem_id", CUser.username);
			tab3 = this;
		} else if (tab.equals("tab4")) {
			intent = new Intent(this, GuideActivity.class);
			tab4 = this;
		}
		else if (tab.equals("tab5")) {
			intent = new Intent(this, SettingActivity.class);
			tab5 = this;
		}
		
		goActivity(intent, cleartop);
	}
	
	public void goActivity(Intent intent, boolean clearTop)  {	
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_FORWARD_RESULT);	
		View view = getLocalActivityManager().startActivity(String.valueOf(UUID.randomUUID()), intent).getDecorView();
		
		if (!clearTop)
			addView(view);	
		else 
			clearTopView(view);
	}	 
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case Constants.REQUEST_ONLINE_ACTIVITY:
			((OnlineClassActivity)getCurrentActivity()).onActivityResult(requestCode, resultCode, data);
			break;
		case Constants.REQUEST_OFFLINE_ACTIVITY:
			((OfflineClassActivity)getCurrentActivity()).onActivityResult(requestCode, resultCode, data);
			break;
		}
	}
}
