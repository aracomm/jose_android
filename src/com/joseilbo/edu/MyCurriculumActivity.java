package com.joseilbo.edu;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.joseilbo.edu.adapter.AdapterItemManager;
import com.joseilbo.edu.adapter.Api;
import com.joseilbo.edu.adapter.CurriListAdapter2;
import com.joseilbo.edu.adapter.CurriListAdapter2.CurriListAdapterAction;
import com.joseilbo.edu.adapter.OnResponseListener;
import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Preference;
import com.joseilbo.edu.common.StaticVars;
import com.joseilbo.edu.common.Util;
import com.joseilbo.edu.container.CurriInfo;
import com.joseilbo.edu.container.CurriculumInfo2;
import com.joseilbo.edu.ui.DownloadFileAsync;
import com.joseilbo.edu.ui.DownloadFileAsync.DownloadFileAsyncAction;

public class MyCurriculumActivity extends XActivity implements OnResponseListener, CurriListAdapterAction, DownloadFileAsyncAction {
	Cursor cursor;
	String title = "";
	String num = "";

	AlertDialog alert;
	protected static WaitDialog dialog;
	MyCurriculumActivity self = null;

	ListView list;
	CurriListAdapter2 cadapter;
	ArrayList<CurriInfo> clist;
	OnResponseListener callback;
	Context context;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self = this;
		context = this;
		callback = this;
		Intent i = getIntent();
		title = i.getStringExtra("title");
		num = i.getStringExtra("num");

		setContentView(R.layout.curriculum);
		
		//event assign
		((ImageView) findViewById(R.id.iv_home)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_study_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_call_upper_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_setting_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_search_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_introduce_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_login_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_notice_main2)).setOnClickListener(self);
		((ImageView) findViewById(R.id.btn_call_main2)).setOnClickListener(self);
		
		//login check
		if (CUser.mno.equals("")) {
			((ImageView) findViewById(R.id.btn_login_main2)).setImageResource(R.drawable.footer_btn2);
		} else {
			((ImageView) findViewById(R.id.btn_login_main2)).setImageResource(R.drawable.footer_btn3);
		}
		
		((TextView) findViewById(R.id.tv_title)).setText(title);
		
		list = (ListView) findViewById(R.id.list);
		clist = new ArrayList<CurriInfo>();
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.iv_home:
			startActivity(new Intent(self, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			finish();
			break;
		
		case R.id.btn_study_main2:
				finish();
			break;
			
		case R.id.btn_call_upper_main2:
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.tel_guestcenter)));
			break;
			
		case R.id.btn_setting_main2:
			startActivity(new Intent(this, SettingActivity.class));
			break;
			
		case R.id.btn_search_main2:
			String _searchUrl = Constants.CourseSearchUrl + "?mem_id=" + CUser.userid;
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra( "url", _searchUrl).putExtra("title", "검색"));
			break;
			
		case R.id.btn_introduce_main2:
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra("url", Constants.MobileOfficeIntro).putExtra("title", "재무교육원 소개"));
			break;
			
		case R.id.btn_login_main2:
			if (CUser.mno.equals("")) {
				startActivity(new Intent(this, LoginActivity.class));
			} else {
				Util.alertYesNo(self, R.string.app_name, R.string.app_logout, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								finish();
								(new Handler()).post(new Runnable() {
									public void run() {
										CUser.username = "";
										CUser.userpw = "";
										CUser.mno = "";
										CUser.userid = "";
										logout();
										
										((ImageView) findViewById(R.id.btn_login_main2)).setImageResource(R.drawable.footer_btn2);
									}
								});
							}
						}, null);
			}
			break;
			
		case R.id.btn_notice_main2:
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra( "url", Constants.NoticeListUrl).putExtra("title", "공지사항"));
			break;
			
		case R.id.btn_call_main2:
			startActivity(new Intent(this, SupportActivity.class));
			break;
		}
	}

	public void onStart() {
		super.onStart();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();
		
		if (CUser.userid.length() == 0) {
			(new Handler()).postDelayed(new Runnable() {
				public void run() {
					_api.curriculum(num, context, callback);
				}
			}, 2000);
		} else {
			_api.curriculum(num, context, callback);
		}
		
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		finish();
		return;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onResponseReceived(int api, Object result) {
		switch (api) {
		case Api.CURRICULUM:
			if (result != null) {
				CurriculumInfo2 allCurriData = (CurriculumInfo2) result;
				((TextView) findViewById(R.id.tv_title)).setText(allCurriData.lecSubject);

				StaticVars.curriItems2.clear();

				for (CurriInfo n : allCurriData.curriculum) {
					AdapterItemManager.curriitem_addlist2(n);
				}

				clist = StaticVars.curriItems2;
				cadapter = new CurriListAdapter2(this, this.num, clist);
				cadapter.setOnCurriListAdapterAction(this);
				list.setAdapter(cadapter);
			} else {
				Util.PopupMessage(self, getResources().getString(R.string.api_http_alert));
			}
			break;
		}
	}

	@Override
	public void playStudy(final String url) {
		if (Util.isWifiConnected(self) || CUser.pushchk == 1) {
			startActivity(new Intent(MyCurriculumActivity.this, Html5WebActivity.class).putExtra("url", url));
		} else {
			Util.alert(self, getResources().getString(R.string.app_name), "3G/LTE 네트워크를 사용할 경우 데이터 요금이 부과 될 수 있습니다.. 계속 진행하시겠습니까?", "예", "아니오"
			, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {		
					startActivity(new Intent(MyCurriculumActivity.this, Html5WebActivity.class).putExtra("url", url));
				}
			}
			, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {					
				}
			});	
		}
	}

	@Override
	public void downloadStudy(String url) {
		final String downloadUrl = url.replace("\\", "");
		String[] data = downloadUrl.split("/");
		final String fileName = data[data.length-1];
		final String key = this.num + "_" + fileName;
		
		if (Util.isWifiConnected(self) || CUser.pushchk == 1) {
			DownloadFileAsync download = new DownloadFileAsync(context);
			download.setOnDownloadFileAsyncAction(MyCurriculumActivity.this);
			download.execute(downloadUrl, key, fileName);			
		}else{
			Util.alertYesNo(context, R.string.app_name, R.string.download_3g_alert, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					DownloadFileAsync download = new DownloadFileAsync(context);
					download.setOnDownloadFileAsyncAction(MyCurriculumActivity.this);
					download.execute(downloadUrl, key, fileName);			
				}
			}, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {}
			});
		}
	}

	@Override
	public void onDownloadEnd(String key, String path) {
		if (!key.equals("") && !path.equals("")) {
			Preference pref = new Preference(this, Util.MT_PREFS);
			pref.write(key, path);
			_api.curriculum(num, context, callback);
		}
	}

	@Override
	public void playDownloadStudy(String key) {
		Preference pref = new Preference(this, Util.MT_PREFS);
		String downloadedPath =  pref.read(key, "");
		if (!downloadedPath.equals("")) {
			Intent i = new Intent(MyCurriculumActivity.this, VideoViewActivity.class);
			i.putExtra("uri", downloadedPath);
			startActivity(i);
		}
	}
}
