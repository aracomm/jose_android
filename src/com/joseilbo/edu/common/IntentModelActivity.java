package com.joseilbo.edu.common;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;

import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;

import com.joseilbo.edu.LoginActivity;
import com.joseilbo.edu.R;
import com.joseilbo.edu.RootActivity;
import com.joseilbo.edu.adapter.Adapter;
import com.joseilbo.edu.adapter.DBmanager;
import com.joseilbo.edu.adapter.HttpHelper;
import com.joseilbo.edu.adapter.OnResponseListener;
import com.joseilbo.edu.container.ApiResult;
import com.joseilbo.edu.ui.UITools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class IntentModelActivity extends Activity implements OnClickListener, OnResponseListener{
	protected static final int HOME = 15;
	public boolean isMain=false;
	protected IntentModelActivity self = this;
	protected Adapter _api= new Adapter();
	DBmanager db_manager;	
	SQLiteDatabase db;
	CookieManager cookieManager;

	protected boolean finish_alert=false;
	//	public Model getIntentModel() {
	//		Bundle extras = getIntent().getExtras();
	//		return new Model(this, (Map) extras.getSerializable("model"));
	//	}

	
	public void setTitleImage(int id) {

ImageView image = (ImageView) findViewById(R.id.img_toplogo);
image.setImageResource(id);
image.setVisibility(View.VISIBLE);
	}
	public void setSubTitle(String title) {


		((TextView) findViewById(R.id.sub_title)).setText(title);
	}
	public  void setTitle(String title) {
		((TextView)	(findViewById(R.id.titleText))).setText(title);
	}
		

	
	public void setProgress(int resId, int max, int progress) {
		((ProgressBar) findViewById(resId)).setMax(max);
		((ProgressBar) findViewById(resId)).setProgress(progress);
	}

	public void setText(int resId, String text) {
		((TextView) findViewById(resId)).setText(text);
	}

	public String getDeviceID() {
		String uid = "";
		try{
			TelephonyManager tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
			uid = tManager.getDeviceId();

		}catch(Exception e) {;}

		if(uid == null || uid.equals(""))
		{
			uid = "35" + //we make this look like a valid IMEI
					Build.BOARD.length()%10+ Build.BRAND.length()%10 + 
					Build.CPU_ABI.length()%10 + Build.DEVICE.length()%10 + 
					Build.DISPLAY.length()%10 + Build.HOST.length()%10 + 
					Build.ID.length()%10 + Build.MANUFACTURER.length()%10 + 
					Build.MODEL.length()%10 + Build.PRODUCT.length()%10 + 
					Build.TAGS.length()%10 + Build.TYPE.length()%10 + 
					Build.USER.length()%10 ; //13 digits

		}

		return uid;

	}

	public void setImageFromURL(int resId, String url) {
		try {
			URLConnection conn = new URL(url).openConnection();
			((ImageView) findViewById(resId)).setImageDrawable(Drawable.createFromStream(conn.getInputStream(), "src"));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setImageResource(int iconResId, int drawableResId) {
		((ImageView) findViewById(iconResId)).setImageResource(drawableResId);
	}

//	@Override
//	public void onBackPressed() {
//				TabBar tabBar = (TabBar) findViewById(R.id.tabBar);
//				
//				if (TabRegistry.globalTabBar && tabBar != null && this.belongsTo(tabBar.tabInfos)) {
//					Intent intent = new Intent(this, MainActivity.class);
//					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//					startActivity(intent);
//					overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
//				} 
//				else if(isMain) {
//					if(finish_alert)
//					{
//						finish();
//		//				CUser.uid="";
//		//				CUser.userid="";
//		//				CUser.target="";
//					}
//					else
//					{
//						Util.ToastMessage(this, "?�로버튼???�번 ???�르�?종료?�니??");
//						finish_alert=!finish_alert;
//					}
//				}
//				else
//					finish();
//	}
		
	public static void closeActivity(final Context context) {
		((Activity)context).finish();
		System.exit(0);
	}
	public void setTitleVisible(int visible)
	{
		RelativeLayout titlebar=(RelativeLayout)findViewById(R.id.titleBar);
		titlebar.setVisibility(visible);
	}
	public void setTitleButton(ImageButton btn, int resource)
	{
		btn.setBackgroundResource(resource);
		btn.setVisibility(View.VISIBLE);
//		UITools.makeStateful(btn);
		btn.setOnClickListener(this);
	}
	public void setTitleButton(Button btn,String str)
	{
		btn.setVisibility(View.VISIBLE);
		UITools.makeStateful(btn);
		btn.setText(str);
		btn.setOnClickListener(this);
	}
	public void clearTitleButton(ImageButton btn) {
		btn.setVisibility(View.GONE);
		btn.setOnClickListener(null);
	}
	@Override
	public void onConfigurationChanged (Configuration newConfig)
	{

	super.onConfigurationChanged(newConfig);

	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	public void setVisibleTitle(int bool)
	{
		((RelativeLayout)findViewById(R.id.titleBar)).setVisibility(bool);
	}
	public void logout()
	{
		
		db_manager = new DBmanager(this,"UserInfo.db");
		db = db_manager.getWritableDatabase();
		String sql = "delete from userinfo";
		db.execSQL(sql);
		Environments.SAVE_ACCOUNT_INFO=false;
		Environments.save(this);
		db.close();
		finish();
		startActivity(new Intent(this,LoginActivity.class));
	}
	public void loadUserInfo() {
		DBmanager db_manager= new DBmanager(this,"UserInfo.db");	
		SQLiteDatabase db=db_manager.getReadableDatabase();
//		Cursor tw_cursor = db.query("userinfo", new String[] {"mno","userid","userpw","pushchk","username","usermode"}, null, null, null, null, null);
		Cursor tw_cursor = db.query("userinfo", new String[] {"mno","user_id","passwd","pushchk","username","usermode"}, null, null, null, null, null);
		//	values.put("uid", StaticVars.uid);

		try {
//			Log.d("passone","db count="+tw_cursor.getCount());
			if (tw_cursor.getCount()!= 0) {
				tw_cursor.moveToFirst();
				for(int i=0; i<tw_cursor.getCount();i++)
				{
					//CUser.mno=tw_cursor.getString(0);
					CUser.userid=tw_cursor.getString(1);
					//CUser.userpw=tw_cursor.getString(2);
					//CUser.pushchk=tw_cursor.getInt(3);
					//CUser.username=tw_cursor.getString(4);
					//CUser.usermode=tw_cursor.getInt(5);
					tw_cursor.moveToNext();
				}
//				_api.login(CUser.userid, CUser.userpw, Util.getDeviceID(this),CUser.device_token, this, this);
				/*
				HttpHelper hh=new HttpHelper();
				CookieSyncManager.createInstance(this);
				cookieManager=CookieManager.getInstance();
				  hh.updateCookies("COOKIENAME",CUser.username);
				  hh.updateCookies("COOKIEUID",CUser.userid);
				  hh.updateCookies("COOKIEIDX",CUser.mno);
				hh.updateCookies("COOKIEUDID",Util.getDeviceID(self));
				  CookieSyncManager.getInstance().sync();
				*/

			}
		} catch(Exception e) {	    		
		}

		tw_cursor.close(); 
		Cursor cursor = db.query("device", new String[] {"device_token"}, null, null, null, null, null);
		try {
//			Log.d("passone","db count="+tw_cursor.getCount());
			if (cursor.getCount()!= 0) {
				cursor.moveToFirst();
				for(int i=0; i<cursor.getCount();i++)
				{
					CUser.device_token=tw_cursor.getString(0);
				tw_cursor.moveToNext();
				}
//				_api.login(CUser.userid, CUser.userpw, Util.getDeviceID(this),CUser.device_token, this, this);
			}
		} catch(Exception e) {	    		
		}

		cursor.close(); 
		if(db!=null)
			db.close();
	}
	public void onResponseReceived(int api, Object result) {
		// TODO Auto-generated method stub
		if (result != null) {

			ApiResult signIn = (ApiResult)result;	    		

			/*	mHanuri
			if (signIn.result.trim().equals("OK")) {  			
//				self.onRestart();
			} else if (signIn.result.trim().equals("FAIL")) {    	
				
				Util.ToastMessage(this, signIn.message);

			}  
			*/
			
			switch(signIn.errorCode) {
			case 0:
				CUser.mno = CUser.userid;
				break;
			case -1:
				Util.PopupMessage(this, "아이디 입력 없음");
				break;
			case -2:
				Util.PopupMessage(this, "암호 입력 없음");
				break;
			case -3:
				Util.PopupMessage(this, "고유번호 입력 없음");
				break;
			case -4:
				Util.PopupMessage(this, "그외 에러");
				break;
			case -5:
				Util.PopupMessage(this, "등록되지 않은 아이디");
				break;
			case -6:
				Util.PopupMessage(this, "비밀번호가 일치안함");
				break;
			case -7:
				Util.PopupMessage(this, "기계 고유번호가 맞기 않음");
				break;
			}



		} else {
			Util.PopupMessage(this, getResources().getString(R.string.api_http_alert));
		}		
	}   
}
