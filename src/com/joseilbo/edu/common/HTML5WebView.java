package com.joseilbo.edu.common;



import java.util.List;

import org.apache.http.cookie.Cookie;

import com.joseilbo.edu.LoginActivity;
import com.joseilbo.edu.R;
import com.joseilbo.edu.adapter.HttpHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class HTML5WebView extends WebView {

	private Context                                                         mContext;
	private MyWebChromeClient                                       mWebChromeClient;
	private View                                                            mCustomView;
	private FrameLayout                                                     mCustomViewContainer;
	private WebChromeClient.CustomViewCallback      mCustomViewCallback;

	private FrameLayout                                                     mContentView;
	private LinearLayout                                                     mBrowserFrameLayout;
	private FrameLayout                                                     mLayout;
	CookieManager cookieManager;
	HttpHelper hh=new HttpHelper();
	private ProgressDialog progressDialog;
	private final Handler handler = new Handler();
	RelativeLayout titlebar;
	Button btn_golist;
	static final String LOGTAG = "HTML5WebView";
	HTML5WebView self;
	int loadCnt=0; //두번 로딩 방지 카운트
	private void init(final Context context) {
		mContext = context;             
		Activity a = (Activity) mContext;
		self=this;
		mLayout = new FrameLayout(context);
		mBrowserFrameLayout = (LinearLayout) LayoutInflater.from(a).inflate(R.layout.custom_screen, null);
		mContentView = (FrameLayout) mBrowserFrameLayout.findViewById(R.id.main_content);
		mContentView = (FrameLayout) mBrowserFrameLayout.findViewById(R.id.main_content);
		btn_golist = (Button) mBrowserFrameLayout.findViewById(R.id.btn_golist);
		btn_golist.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//목록으로 나올 때 영상소리 멈추게함..
				self.loadData("", "text/html", null); 

				((Activity) context).finish();
			}
		});
		mCustomViewContainer = (FrameLayout) mBrowserFrameLayout.findViewById(R.id.fullscreen_custom_content);
		mLayout.addView(mBrowserFrameLayout, COVER_SCREEN_PARAMS);
		CookieSyncManager.createInstance(context);
		cookieManager=CookieManager.getInstance();
		if(CUser.mno.equals(""))
		{
			cookieManager.removeAllCookie();
			hh.clearCookie();	
		}
		cookieManager = CookieManager.getInstance();
		List<Cookie> cookies=hh.getCookies();
		Cookie sessionInfo = null;
		Log.d("jose","uid="+CUser.mno);

		for (Cookie cookie : cookies ) {
			sessionInfo = cookie;
			String cookieString = sessionInfo.getName() + "="
					+ sessionInfo.getValue() + "; path="
					+ sessionInfo.getPath()+"; domain="+sessionInfo.getDomain();
			Log.d("jose","s"+cookieString);
			cookieManager.setCookie(sessionInfo.getDomain(),cookieString);

			CookieSyncManager.getInstance().sync();
		}

		CookieSyncManager.getInstance().startSync();
		mWebChromeClient = new MyWebChromeClient();


		addJavascriptInterface(new AndroidBridge(), "android");

		setHorizontalScrollBarEnabled(false); // ?�로 scroll ?�거
		setVerticalScrollBarEnabled(false); // �?�� scroll ?�거
		setWebChromeClient(mWebChromeClient);

		setWebViewClient(new MyWebViewClient());

		// Configure the webview
		WebSettings s = getSettings();
		s.setDomStorageEnabled(true); // I think you will need this one
		s.setPluginState(PluginState.ON);
		s.setBuiltInZoomControls(false);
		this.setInitialScale(1);
		s.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
		s.setUseWideViewPort(true);
		s.setLoadWithOverviewMode(true);
		s.setSaveFormData(true);
		s.setJavaScriptEnabled(true);
		s.setCacheMode(WebSettings.LOAD_NO_CACHE);
		s.setDefaultTextEncodingName("utf-8");
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN){
			try {
				  s.setAllowUniversalAccessFromFileURLs(true);
			        s.setAllowFileAccessFromFileURLs(true);
			        } catch(NullPointerException e) {
			}
		}
		mContentView.addView(this);
	}

	public HTML5WebView(Context context) {
		super(context);
		init(context);
	}

	public HTML5WebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public HTML5WebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public FrameLayout getLayout() {
		return mLayout;
	}

	public boolean inCustomView() {
		return (mCustomView != null);
	}

	public void hideCustomView() {
		mWebChromeClient.onHideCustomView();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if ((mCustomView == null) && canGoBack()){
				goBack();
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	private class MyWebChromeClient extends WebChromeClient {
		private Bitmap          mDefaultVideoPoster;
		private View            mVideoProgressView;

		@Override
		public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback)
		{
			//Log.i(LOGTAG, "here in on ShowCustomView");
			HTML5WebView.this.setVisibility(View.GONE);
			// if a view already exists then immediately terminate the new one
			if (mCustomView != null) {
				callback.onCustomViewHidden();
				return;
			}

			mCustomViewContainer.addView(view);
			mCustomView = view;
			mCustomViewCallback = callback;
			mCustomViewContainer.setVisibility(View.VISIBLE);
		}

		@Override
		public void onHideCustomView() {

			if (mCustomView == null)
				return;        

			// Hide the custom view.
			mCustomView.setVisibility(View.GONE);

			// Remove the custom view from its container.
			mCustomViewContainer.removeView(mCustomView);
			try {

				Thread.sleep(1500);

			} catch (InterruptedException e) {

			// TODO Auto-generated catch block

				e.printStackTrace();

			}
			mCustomView = null;
			mCustomViewContainer.setVisibility(View.GONE);
			mCustomViewCallback.onCustomViewHidden();
			HTML5WebView.this.setVisibility(View.VISIBLE);

			//Log.i(LOGTAG, "set it to webVew");
		}

		@Override
		public Bitmap getDefaultVideoPoster() {
			//Log.i(LOGTAG, "here in on getDefaultVideoPoster");    
			if (mDefaultVideoPoster == null) {
				mDefaultVideoPoster = BitmapFactory.decodeResource(
						getResources(), R.drawable.ic_launcher);
			}
			return mDefaultVideoPoster;
		}

		@Override
		public View getVideoLoadingProgressView() {
			//Log.i(LOGTAG, "here in on getVideoLoadingPregressView");

			if (mVideoProgressView == null) {
				LayoutInflater inflater = LayoutInflater.from(mContext);
				mVideoProgressView = inflater.inflate(R.layout.video_loading_progress, null);
			}
			return mVideoProgressView; 
		}

		@Override
		public void onReceivedTitle(WebView view, String title) {
		}

		@Override
		public void onProgressChanged(WebView view, int newProgress) {
			((Activity) mContext).getWindow().setFeatureInt(Window.FEATURE_PROGRESS, newProgress*100);
		}

		@Override
		public void onCloseWindow(WebView window) {
			// TODO Auto-generated method stub
			Util.debug("close window");
			super.onCloseWindow(window);
		}

		public boolean onJsAlert(WebView view, String url, final String message, JsResult result)
		{
			//if(null==view)return;
			final JsResult r = result;
			// return super.onJsAlert(view, url, message, result);
			if(null == mContext)return false;

			new AlertDialog.Builder(mContext)//mContext.getApplicationContext());
			/* .setTitle("AlertDialog") */
			.setMessage(message).setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					if(message.equals("Session expired!"))
					{
						((Activity)mContext).finish();
						CUser.mno="";

						cookieManager=CookieManager.getInstance();
						cookieManager.removeAllCookie();
						hh.clearCookie();
						mContext.startActivity(new Intent(mContext,LoginActivity.class));

					}
					r.confirm();
				}
			}).setCancelable(false).create().show();
			return true;
		}
		@Override
		public boolean onJsConfirm(WebView view, String url, String message, final android.webkit.JsResult result)
		{
			new AlertDialog.Builder(mContext)
			.setTitle(Constants.app_name)
			.setMessage(message)
			.setPositiveButton(android.R.string.ok,
					new AlertDialog.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					result.confirm();
				}
			})
			.setNegativeButton(android.R.string.cancel,
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					result.cancel();
				}
			})
			.create()
			.show();

			return true;
		};


	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Log.i("jose", "shouldOverrideUrlLoading: "+url);
			if(url.startsWith("tel:"))
			{
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				mContext.startActivity( intent );
			}
			else      
			{

				Log.d("jose","url:"+url);
				//두번로딩 안되게 막음
				view.loadUrl(url); 

				
			}
			return true;

		}


		public void onPageFinished(WebView view, String url) {
			Log.d("jose","onPageFinished:"+url);

			CookieSyncManager.getInstance().sync();

		}


		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			Log.d("jose","onPageStarted:"+url+"real Url:"+self.getUrl());
			//두번로딩 안되게 카운트가 1이상이면 로딩을 멈춤. 특히 퀴즈에서 발생
			//if(loadCnt>0)
			//	view.stopLoading();
			loadCnt++; 
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public WebResourceResponse shouldInterceptRequest(WebView view,
				String url) {
			// TODO Auto-generated method stub

			return super.shouldInterceptRequest(view, url);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Util.ToastMessage(mContext, getResources().getString(R.string.api_http_alert));

		}
		
	}

	static final FrameLayout.LayoutParams COVER_SCREEN_PARAMS =
			new FrameLayout.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

	private class AndroidBridge {
		public void callAndroid(final String arg) { // must be final
			handler.post(new Runnable() {
				public void run() {


				}
			});
		}

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		//홈버튼 눌렀을 때 영상소리 멈추게함..
	    self.loadData("", "text/html", "utf-8");

		super.onPause();
	}
	
}

