package com.joseilbo.edu.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.Context;
import android.widget.DatePicker;

public class YMPickerDialog extends android.app.DatePickerDialog {
	 public YMPickerDialog(Context context, OnDateSetListener callBack,
	            int year, int monthOfYear, int dayOfMonth) {
	        super(context, callBack, year, monthOfYear, dayOfMonth);
	        
	        updateTitle(year, monthOfYear);
	    }
	    public void onDateChanged(DatePicker view, int year,
	            int month, int day) {
	        updateTitle(year, month);
	    }
	    private void updateTitle(int year, int month) {
	     
	        setTitle(year+"년 "+month+"월" );
	    }   
	        /*
	         * the format for dialog tile,and you can override this method
	         */
	    public SimpleDateFormat getFormat(){
	        return new SimpleDateFormat("yyyy-MM");
	    };
}
