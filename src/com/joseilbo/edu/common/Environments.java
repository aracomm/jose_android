package com.joseilbo.edu.common;


import android.content.Context;

public class Environments {	

	/**
	 * ȯ�漳�� ��������
	 */
	public static boolean PREFERENCE_EXIST = false;
	/**
	 * �ڵ��α���
	 */
	public static boolean AUTO_LOGIN = false;
	/**
	 * �������� ����
	 */
	public static boolean SAVE_ACCOUNT_INFO = true;
	
	/**
	 * 3G�ٿ�ε�?���?
	 */
	public static boolean ALLOW_PUSH = false;
	public static boolean ALLOW_3G = false;
	/**
	 * 3G�ٿ�ε�?���?
	 */
	public static boolean ALERT_WHEN_DOWNLOAD_ON_MOBILE_3G = true;
	public static float DOWNLOAD_CAPACITY = 4;

	/**
	 * �������Ͽ� ���� ���࿩��
	 */
	public static boolean ISAGREE = true;
	/**
	 * �������� �ε���
	 */
	public static int NOTICE_IDX = 0;

	/**
	 * ?�시 ?�림
	 */


	public static int current_view=0;


	
	/**
	 * Save configuration information to preference file.<br>
	 * @param context
	 */
	public static void save(Context ctx) {
		Preference prefs = new Preference(ctx, Constants.PREFERENCE_FILE);
		prefs.write("PREFERENCE_EXIST", true);
		prefs.write("AUTO_LOGIN", AUTO_LOGIN);
		prefs.write("ALLOW_3G", ALLOW_PUSH);
		prefs.write("SAVE_ACCOUNT_INFO", SAVE_ACCOUNT_INFO);
		prefs.write("ALLOW_ALLOW_PUSH", ALLOW_PUSH);
		prefs.write("ALERT_WHEN_DOWNLOAD_ON_MOBILE_3G", ALERT_WHEN_DOWNLOAD_ON_MOBILE_3G);
		prefs.write("DOWNLOAD_CAPACITY", DOWNLOAD_CAPACITY);
	}

	/**
	 * Load configuration information from preference file.<br>
	 * @param context
	 */
	public static void load(Context ctx) {
		Preference prefs = new Preference(ctx, Constants.PREFERENCE_FILE);
		PREFERENCE_EXIST = prefs.read("PREFERENCE_EXIST", false);
		AUTO_LOGIN = prefs.read("AUTO_LOGIN", false);
		SAVE_ACCOUNT_INFO = prefs.read("SAVE_ACCOUNT_INFO", true);
		ALLOW_PUSH = prefs.read("ALLOW_PUSH", false);
		ALERT_WHEN_DOWNLOAD_ON_MOBILE_3G = prefs.read("ALERT_WHEN_DOWNLOAD_ON_MOBILE_3G", true);
		DOWNLOAD_CAPACITY = prefs.read("DOWNLOAD_CAPACITY", 4.0f);

	}	 
}

