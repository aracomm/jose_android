package com.joseilbo.edu.common;

import java.util.ArrayList;

import com.joseilbo.edu.container.CurriInfo;
import com.joseilbo.edu.container.CurriculumItem;
import com.joseilbo.edu.container.CurriculumItem2;
import com.joseilbo.edu.container.EventItem;
import com.joseilbo.edu.container.LectureItem;
import com.joseilbo.edu.container.MonthOnlineItem;
import com.joseilbo.edu.container.NoticeItem;
import com.joseilbo.edu.container.SiteItem;





public class StaticVars {
	public static String uid = null;

	public static ArrayList<SiteItem> siteItems = new ArrayList<SiteItem>();
	public static ArrayList<CurriculumItem> curriItems = new ArrayList<CurriculumItem>();
	public static ArrayList<LectureItem> lectureItems = new ArrayList<LectureItem>();
	public static ArrayList<NoticeItem> noticeItems = new ArrayList<NoticeItem>();
	public static ArrayList<EventItem> eventItems = new ArrayList<EventItem>();
	public static ArrayList<NoticeItem> thisMonthItems = new ArrayList<NoticeItem>();	// jose
	public static ArrayList<CurriInfo> curriItems2 = new ArrayList<CurriInfo>();	// jose
	public static ArrayList<MonthOnlineItem> monthOnlineItems = new ArrayList<MonthOnlineItem>(); // jose

	public static void clearStaticVars() {
		curriItems.clear();
		lectureItems.clear();
		noticeItems.clear();
		eventItems.clear();
		thisMonthItems.clear();
		curriItems2.clear();
		monthOnlineItems.clear();
	}	
}
