package com.joseilbo.edu.common;

import android.os.Environment;

public class Constants {
	public static final boolean Eula = false;
	public static final String rootDirectory = Environment.getExternalStorageDirectory().getAbsolutePath()+"/jose/";
	public static final String InternalrootDirectory = Environment.getRootDirectory().getAbsolutePath()+"/jose/";
	public final static String libraryKey="f1103eeb49f74f83b36cae6a9f7eff39";

	public static final String imageDirectory = rootDirectory+"/files/image";
	public final static String tempImage = Environment.getExternalStorageDirectory().getAbsolutePath()+"/temp.jpg";
	public static String senderID="1034572221469"; //1034572221469 365258868387
	
	public static final String PLATFORM_ANDROID		= "1";
	public static final String PLATFORM_IOS			= "2";
	
	public static String tel_guestcenter = "tel:0231468230"; 
	public static String tel_studycenter = "tel:0231468230"; 
	public static String tel_techcenter = "tel:0231468230";
	
	/**
	 * API URL
	 */
	public static final String Homepage = "http://m.joseilbo.com";	// Joseilbo Mobile Homepage
	
	/**
	 * Real Service Configure.
	 * */
	public static boolean isReal = true;
	public static String baseUrl = "http://edu.joseilbo.com";			//real - jose web server
	public static String FindIdPwdUrl 			= baseUrl + "/member/find_id.php";
	public static String RegistMemeberUrl 	= baseUrl + "/member/mem_join2.php";
	public static String CourseSearchUrl 		= baseUrl + "/mobile/course/search.php";
	public static String MyClassUrl 				= baseUrl + "/mobile/class/myClass_continue.php";
	public static String OnlineClassUrl 			= baseUrl + "/mobile/course/online_course.php";
	public static String OfflineClassUrl 			= baseUrl + "/mobile/course/offline_course.php"; 
	public static String NoticeListUrl 			= baseUrl + "/mobile/board/noticeList.php"; 
	public static String KakaoInfoUrl 			= baseUrl + "/mobile/course/talk.php";
	public static String MonthOnlineUrl  		= baseUrl + "/mobile/course/month_online.php";
	public static String MobileMainUrl 			= baseUrl + "/mobile/index.htm";
	public static String MobileOnlineListUrl 	= baseUrl + "/mobile/course/online_course_list.php";
	public static String MobileCategoryUrl 	= baseUrl + "/mobile/course/online_course_cate.php";
	public static String MobileOnlineList2Url	= baseUrl + "/mobile/course/online_course_list2.php";
	public static String MobileOfflineListUrl 	= baseUrl + "/mobile/course/offline_course_list.php";
	public static String MobileOfficeIntro 		= baseUrl + "/mobile/edu_info.htm";
	public static String MobilePaymentUrl 	= baseUrl + "/mobile/course/";
	
	/**
	 * Dev Service Configure.
	 * */
	/*
	public static boolean isReal = false;
	public static String baseUrl 					= "http://tedu.joseilbo.com";	
	public static String FindIdPwdUrl 			= baseUrl + "/member/find_id.php";
	public static String RegistMemeberUrl 	= baseUrl + "/member/mem_join2.php";
	public static String CourseSearchUrl 		= baseUrl + "/mobile/course/search.php";
	public static String MyClassUrl 				= baseUrl + "/mobile/class/myClass_continue.php";
	public static String OnlineClassUrl 			= baseUrl + "/mobile/course/online_course.php";
	public static String OfflineClassUrl 			= baseUrl + "/mobile/course/offline_course.php"; 
	public static String NoticeListUrl 			= baseUrl + "/mobile/board/noticeList.php"; 
	public static String KakaoInfoUrl 			= baseUrl + "/mobile/course/talk.php";
	public static String MonthOnlineUrl  		= baseUrl + "/mobile/course/month_online.php";
	public static String MobileMainUrl 			= baseUrl + "/mobile/index.htm";
	public static String MobileOnlineListUrl 	= baseUrl + "/mobile/course/online_course_list.php";
	public static String MobileCategoryUrl 	= baseUrl + "/mobile/course/online_course_cate.php";
	public static String MobileOnlineList2Url 	= baseUrl + "/mobile/course/online_course_list2.php";
	public static String MobileOfflineListUrl 	= baseUrl + "/mobile/course/offline_course_list.php";
	public static String MobileOfficeIntro 		= baseUrl + "/mobile/edu_info.htm";
	public static String MobilePaymentUrl 	= baseUrl + "/mobile/course/";
 	*/
	
	/**
	 * LOGCAT TAG
	 */
	public static String TAG = "jose";
	
	/**
	 * Debuggable
	 */
	public static boolean debuggable = true;
	
	/**
	 * Database name
	 */
	public static final String DATABASE_NAME = "UserInfo.db";
	public static final String PREFERENCE_FILE = "Jose";
	public static String app_name="Jose";
	
	/**
	 * ACTIVITY REQUEST CODE
	 * */
	public static final String REQUEST_FIELD = "REQUEST_FIELD";
	public static final int REQUEST_ONLINE_ACTIVITY = 700;
	public static final int REQUEST_OFFLINE_ACTIVITY = 701;
	public static final int REQUEST_SEARCH_ONLINE_ACTIVITY = 702;
	public static final int REQUEST_SEARCH_OFFLINE_ACTIVITY = 703;
	
	/**
	 * Other Configure..
	 * */
	public static final int MOVIE_MOVE_TERM = 10;
}
