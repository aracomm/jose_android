package com.joseilbo.edu.container;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * API ó�� ���?
 */
@JsonAutoDetect()
@JsonIgnoreProperties(ignoreUnknown = true) 

public class CurriculumInfo {

	@JsonProperty("id")			public String id;
	@JsonProperty("curriculum")	public List<WeekInfo> curriculum;

}