package com.joseilbo.edu.container;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect()
@JsonIgnoreProperties(ignoreUnknown = true) 

public class CurriInfo {
	@JsonProperty("ch_num")			public String chNum;
	@JsonProperty("ch_subject")		public String chSubject;
	@JsonProperty("ch_downlaod")	public String chDownload;
	@JsonProperty("ch_source")		public String chSource;
	
	public CurriInfo clone() {
		CurriInfo newItem = new CurriInfo();
		
		newItem.chNum = this.chNum;
		newItem.chSubject = this.chSubject;
		newItem.chDownload = this.chDownload;
		newItem.chSource = this.chSource;
		
		return newItem;
	}
}