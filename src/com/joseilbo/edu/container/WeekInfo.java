package com.joseilbo.edu.container;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * API ó�� ���?
 */
@JsonAutoDetect()
@JsonIgnoreProperties(ignoreUnknown = true) 

public class WeekInfo {
	
	@JsonProperty("id")				public String id;		
	@JsonProperty("step_id")		public String step_id;			
	@JsonProperty("lesson_id")		public String lesson_id;			
	@JsonProperty("type")			public String type;			
	@JsonProperty("chapter")		public String chapter;			
	@JsonProperty("sort")			public String sort;			
	@JsonProperty("start_day")		public String start_day;			
	@JsonProperty("peroid")			public String peroid;			
	@JsonProperty("time")			public String time;			
	@JsonProperty("offline_yn")		public String offline_yn;		
	@JsonProperty("subject")		public String subject;			
	@JsonProperty("tutor_id")		public String tutor_id;			
	@JsonProperty("classroom_id")	public String classroom_id;			
	@JsonProperty("sisu")			public String sisu;			
	@JsonProperty("lesson_type")	public String lesson_type;			
	@JsonProperty("assign_score")	public String assign_score;			
	@JsonProperty("step_day")		public String step_day;			
	@JsonProperty("study_day")		public String study_day;			
	@JsonProperty("start_time")		public String start_time;			
	@JsonProperty("end_time")		public String end_time;			
	@JsonProperty("status")			public String status;			
	@JsonProperty("service_yn")		public String service_yn;		
	@JsonProperty("type_1")			public String type_1;			
	@JsonProperty("lesson_subject")	public String lesson_subject;			
	@JsonProperty("complete_yn")	public String complete_yn;			
	@JsonProperty("course_lesson_id")	public String course_lesson_id;			
	@JsonProperty("learning_time")	public String learning_time;			
	@JsonProperty("max_pos")		public String max_pos;			
	@JsonProperty("study_yn")		public String study_yn;			
	@JsonProperty("video_info_type")	public String video_info_type;			

	@JsonProperty("movie_type")		public String movie_type;			
	@JsonProperty("start_file")		public String start_file;			
	@JsonProperty("mobile_file")	public String mobile_file;			
	@JsonProperty("lesson_page")	public String lesson_page;			
	@JsonProperty("lesson_time")	public String lesson_time;			
	@JsonProperty("lesson_complete_time")	public String lesson_complete_time;			
	
	@JsonProperty("cde_url") public String cde_url;
	@JsonProperty("course_user_id") public String course_user_id;
	
	@JsonProperty("ratio") public String ratio;
	@JsonProperty("cpid") public String cpid;
	@JsonProperty("first_date") public String first_date;
	@JsonProperty("last_date") public String last_date;
	@JsonProperty("cptime") public String cptime;
}