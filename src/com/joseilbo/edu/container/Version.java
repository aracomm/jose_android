package com.joseilbo.edu.container;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true) 
public class Version {

	@JsonProperty("version")		public String version;		// 
	@JsonProperty("link")			public String link;			// 
	@JsonProperty("new")			public String newComment;		//

}

