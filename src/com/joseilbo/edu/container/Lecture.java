package com.joseilbo.edu.container;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * API ó�� ���?
 */
@JsonAutoDetect()
@JsonIgnoreProperties(ignoreUnknown = true) 

public class Lecture {

	@JsonProperty("part")	public String part;		
	@JsonProperty("page")	public String page;			
	@JsonProperty("subject")	public String subject;			
	@JsonProperty("progress")	public String progress;			
	@JsonProperty("movieurl")	public String movieurl;			
	@JsonProperty("htmlurl")	public String htmlurl;			
	@JsonProperty("studytime")	public String studytime;			
	@JsonProperty("totaltime")	public String totaltime;			
	@JsonProperty("lecture")	public List<LectureInfo> lecture;			
	@JsonProperty("kind")	public String kind;			

}