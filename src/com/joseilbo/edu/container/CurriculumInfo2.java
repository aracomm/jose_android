package com.joseilbo.edu.container;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect()
@JsonIgnoreProperties(ignoreUnknown = true) 

public class CurriculumInfo2 {
	@JsonProperty("ErrorCode")		public String errorCode;
	@JsonProperty("LEC_NUM")		public String lecNum;
	@JsonProperty("LEC_SUBJECT")	public String lecSubject;
	@JsonProperty("List")			public List<CurriInfo> curriculum;
}