package com.joseilbo.edu.container;

public class MonthOnlineItem {
	
	private String url;
	private String img;
	
	public MonthOnlineItem() {}
	
	public MonthOnlineItem(String img, String url) {
		this.url = url;
		this.img = img;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Override
	public String toString() {
		return "MonthOnlineItem [url=" + url + ", img=" + img + "]";
	}
}
