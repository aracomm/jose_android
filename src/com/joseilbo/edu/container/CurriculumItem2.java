package com.joseilbo.edu.container;

import java.util.List;

public class CurriculumItem2 {

	public WeekInfo curriculum = null;
	public List<CurriculumItem2> curriChildren = null;
	
	public void setCurriculumItem(WeekInfo data)
	{
		this.curriculum = data;
	}	

	public void setCurriculum(WeekInfo data, List<CurriculumItem2> children)
	{
		this.curriculum = data;
		this.curriChildren = children;	
	}	

}
