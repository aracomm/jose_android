package com.joseilbo.edu.container;

import java.util.List;

public class GuideItem {

public String subject;	
public List<String> arycont;
	public GuideItem() {		
	}
	
	public String getSubject() {
		return subject;
	}
	
	public List<String> getAryCont() {
		return arycont;
	}
	
	public void setGuide(String subject, List<String> arycont)
	{
		this.subject = subject;
		this.arycont = arycont;	
	}	

}
