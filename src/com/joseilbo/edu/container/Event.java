package com.joseilbo.edu.container;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
public class Event {

	@JsonProperty("evt_idx")	public String evt_idx;		
	@JsonProperty("evt_url")	public String evt_url;		
	@JsonProperty("evt_img_url")	public String evt_img_url;		

}