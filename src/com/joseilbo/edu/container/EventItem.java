package com.joseilbo.edu.container;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EventItem {
private String evt_idx, evt_url,evt_img_url;

public EventItem() {		
	}
	
	public String getEvtIdx() {
		return evt_idx;
	}
	
	public String getEvtUrl() {
		return evt_url;
	}
	public String getEvtImgUrl() {
		return evt_img_url;
	}
	public void setEvent(String evt_idx, String evt_url, String evt_img_url) {
		this.evt_idx = evt_idx;
		this.evt_img_url = evt_img_url;	
		this.evt_url = evt_url;	


	}	

}
