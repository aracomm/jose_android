package com.joseilbo.edu.container;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CertItem {
private String cert_name, cert_kind,cert_date;
/**
 * True if this item is selectable
 */
private boolean mSelectable = true;
public CertItem() {		
	}
	
	public String getCertName() {
		return cert_name;
	}
	
	public String getCertKind() {
		return cert_kind;
	}
	public String getCertDate() {
		return cert_date;
	}

	public void setCert(String cert_name, String cert_kind,String cert_date) {
		this.cert_name = cert_name;
		this.cert_kind = cert_kind;	
		this.cert_date = cert_date;	


	}	
	/**
	 * True if this item is selectable
	 */
	public boolean isSelectable() {
		return mSelectable;
	}
	/**
	 * Set selectable flag
	 */
	public void setSelectable(boolean selectable) {
		mSelectable = selectable;
	}

}
