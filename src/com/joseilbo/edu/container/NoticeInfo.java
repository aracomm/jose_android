package com.joseilbo.edu.container;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * API ó�� ���?
 */
@JsonAutoDetect()
@JsonIgnoreProperties(ignoreUnknown = true) 

public class NoticeInfo {
	/*
	@JsonProperty("brd_idx")	public String brd_idx;		
	@JsonProperty("brd_subject")	public String brd_subject;
	*/			

	/* mHanuri
	@JsonProperty("id")			public String id;
	@JsonProperty("subject")	public String subject;
	@JsonProperty("content")	public String content;
	@JsonProperty("reg_date")	public String reg_date;
	@JsonProperty("hit_cnt")	public String hit_cnt;
	*/
	
	@JsonProperty("num")		public String num;
	@JsonProperty("title")		public String title;
	@JsonProperty("link")		public String link;
}