package com.joseilbo.edu.container;

import java.util.List;

public class CurriculumItem {

	public WeekInfo curriculum = null;
	public List<CurriculumItem> curriChildren = null;
	
	public void setCurriculumItem(WeekInfo data)
	{
		this.curriculum = data;
	}	

	public void setCurriculum(WeekInfo data, List<CurriculumItem> children)
	{
		this.curriculum = data;
		this.curriChildren = children;	
	}	

}
