package com.joseilbo.edu.container;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * API ó�� ���?
 */
@JsonAutoDetect()
@JsonIgnoreProperties(ignoreUnknown = true) 

public class LectureInfo {

	@JsonProperty("part")	public String part;		
	@JsonProperty("page")	public String page;			
	@JsonProperty("subject")	public String subject;			
	@JsonProperty("readflag")	public String readflag;			
	@JsonProperty("movieurl")	public String movieurl;			
	@JsonProperty("htmlurl")	public String htmlurl;			
	@JsonProperty("progress")	public String progress;			
	@JsonProperty("pagestudytime")	public String pagestudytime;			
	@JsonProperty("pagetotaltime")	public String pagetotaltime;			

}