package com.joseilbo.edu.container;

import com.joseilbo.edu.common.Util;



public class LectureItem {
private String part, page,subject,readflag,movieurl,htmlurl,progress,pagestudytime,pagetotaltime,size,classkey,filename,curri_title,gYear,gNum,chasi;
private boolean selected;
public LectureItem() {		
	}
	
	public String getPart() {
		return part;
	}
	
	public String getPage() {
		return page;
	}
	public String getSubject() {
		return subject;
	}
	public String getReadFlag() {
		return readflag;
	}
	public String getMovieurl() {
		return movieurl;
	}
	public String getHtmlurl() {
		return htmlurl;
	}
	public String getSize() {
		return size;
	}
	public String getClasskey() {
		return classkey;
	}
	public String getFilename() {
		return filename;
	}
	public String getCurriTitle() {
		return curri_title;
	}
	public String getProgress() {
		return progress;
	}
	public String getPageStudyTime() {
		return pagestudytime;
	}
	public String getPageTotalTime() {
		return pagetotaltime;
	}
	public String getGYear() {
		return gYear;
	}
	public String getGNum() {
		return gNum;
	}
	public String getChasi() {
		return chasi;
	}
	
	public void setLecture(String part, String page,String subject, String readflag,String movieurl,String htmlurl,String progress,String pagestudytime,String pagetotaltime) {
		this.part = part;
		this.page = page;	
		this.subject = subject;	
		this.readflag = readflag;	
		this.movieurl =Util.setDecodeUrl(movieurl);	
		this.htmlurl = htmlurl;	
		this.progress=progress;
		this.pagestudytime=pagestudytime;
		this.pagetotaltime=pagetotaltime;

	}	
	public void setDownLecture(String part, String page,String subject, String readflag,String movieurl,String htmlurl,String size,String classkey,String filename,String curri_title,String gYear,String gNum,String chasi) {
		this.part = part;
		this.page = page;	
		this.subject = subject;	
		this.readflag = readflag;	
		this.movieurl =Util.setDecodeUrl(movieurl);	
		this.htmlurl = htmlurl;	
		this.size = size;	
		this.classkey = classkey;	
		this.filename = filename;	
		this.curri_title=curri_title;
		this.gNum=gNum;
		this.gYear=gYear;
		this.chasi=chasi;
	}	
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
