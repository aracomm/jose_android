package com.joseilbo.edu.container;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
public class ApiResult {

	/*	mHanuri
	@JsonProperty("result")	public String result;		
	@JsonProperty("message")	public String message;		
	*/

	@JsonProperty("ErrorCode")	public int errorCode;		
	@JsonProperty("member_id")	public String member_id;		
	@JsonProperty("mem_name")	public String mem_name;		
	@JsonProperty("mem_email")	public String mem_email;		
	@JsonProperty("mem_lvl")	public int mem_lvl;		
	@JsonProperty("mem_point")	public int mem_point;		
}