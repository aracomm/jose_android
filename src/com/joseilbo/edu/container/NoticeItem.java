package com.joseilbo.edu.container;

public class NoticeItem {
//	private String brd_idx, brd_subject;
	private String _id, _subject;
	private String _link;		// jose

	public NoticeItem() {}
	
	/*
	public String getBrdIdx() {
		return brd_idx;
	}
	
	public String getBrdSubject() {
		return brd_subject;
	}

	public void setNotice(String brd_idx, String brd_subject) {
		this.brd_idx = brd_idx;
		this.brd_subject = brd_subject;	
	}
	*/
	public String getId() {
		return _id;
	}
	
	public String getSubject() {
		return _subject;
	}

	/*	mHanuri
	public void setNotice(String id, String subject) {
		_id = id;
		_subject = subject;
	}
	*/

	// jose - begin
	public void setNotice(String id, String subject, String link) {
		_id = id;
		_subject = subject;
		_link = link;
	}

	public String getLink() {
		return _link;
	}
	// jose - end
}
