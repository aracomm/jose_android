package com.joseilbo.edu.container;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WeekItem {
public String part, subject,startdate,enddate,leccount,fileurl,	 progress,studytime,totaltime;	
	public WeekItem() {		
	}
	
	public String getPart() {
		return part;
	}
	
	public String getStartDate() {
		return startdate;
	}
	public String getSubject() {
		return subject;
	}
	public String getEndDate() {
		return enddate;
	}
	public String getLecCnt() {
		return leccount;
	}
	public String getFileUrl() {
		return fileurl;
	}
	public String getProgress() {
		return progress;
	}
	public String getStudyTime() {
		return studytime;
	}
	public String getTotalTime() {
		return totaltime;
	}
	public void setCurriculum(String part,String subject,String startdate,String enddate,String leccount,String fileurl,String progress,String studytime,String totaltime)
	{
		this.part = part;
		this.startdate = startdate;	
		this.subject = subject;	
		this.leccount = leccount;	
		this.enddate = enddate;	
		this.progress = progress;	
		this.studytime = studytime;	
		this.fileurl = fileurl;	
		this.totaltime = totaltime;	
	}	

}
