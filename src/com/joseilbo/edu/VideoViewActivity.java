package com.joseilbo.edu;

import com.joseilbo.edu.R;
import com.joseilbo.edu.common.Util;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoViewActivity extends XActivity implements OnCompletionListener {
	
	Context context;
	Uri _uri;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self=this;
		context=getParent();

		Intent i=getIntent();
		_uri = Uri.parse(i.getStringExtra("uri").trim());

		setContentView(R.layout.videoview);
		VideoView _videoview = (VideoView)findViewById(R.id.videoView1);
		_videoview.setVideoURI(_uri);
		
		Util.debug(_uri.toString());
		
		_videoview.setOnCompletionListener(this);
        MediaController mc = new MediaController(this);

        _videoview.setMediaController(mc);
        
        _videoview.requestFocus();
        
        // 동영상이 재생준비가 완료되엇을떄를 알수있는 리스너 (실제 웹에서 영상을 다운받아 출력할때 많이 사용됨)
        _videoview.setOnPreparedListener(new OnPreparedListener() {
       
         // 동영상 재생준비가 완료된후 호출되는 메서드
         @Override
         public void onPrepared(MediaPlayer mp) {
          // TODO Auto-generated method stub
	          
	          mp.start();
         }
        });
		
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		finish();
	}

}
