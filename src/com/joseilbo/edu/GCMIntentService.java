package com.joseilbo.edu;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;
import com.joseilbo.edu.R;
import com.joseilbo.edu.adapter.DBmanager;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Util;
public class GCMIntentService extends GCMBaseIntentService {
	 String registration_id = null;
	 String c2dm_msg = "";
	DBmanager db_manager;	
	SQLiteDatabase db;
	
	public GCMIntentService() {
		super(Constants.senderID);
	}

	private static final String TAG = "jose";

	public void onReceive(Context context, Intent intent) {

		Log.i(TAG, "Device registered: regId = " );


	}
	
	 
	

	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.d("jose","reg");
		Log.i(TAG, "Device registered: regId = " + registrationId);
		registration_id=registrationId;
		handleRegistration(context);
		/*
		 * 위에 http:// 어쩌구는 GCMRegistration_Id.php의 url 써주면됨.
		 * 맨 앞에부터 url, id, pw, reg_id, msg순이다.
		 */

		
	}

	@Override
	protected void onUnregistered(Context context, String arg1) {
		Log.i(TAG, "unregistered = "+arg1);
		CUser.device_token="";
		Log.v("C2DM_REGISTRATION",">>>>>" + "unregistration done, new messages from the authorized sender will be rejected" + "<<<<<");
		db_manager = new DBmanager(context,"UserInfo.db");
		db = db_manager.getWritableDatabase();
		String sql = "delete from device";
		db.execSQL(sql);
		db.close();
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.i(TAG, "CONTEXT : = " +context);
		Log.i(TAG, "new message= ");
		if (intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {


			c2dm_msg = intent.getExtras().getString("msg");
			GET_GCM();
			Log.i(TAG,c2dm_msg);

		}

	}
//푸시받음
	public void GET_GCM() { 

		Thread thread = new Thread(new Runnable() { 
			public void run() { 

				handler.sendEmptyMessage(0); 
			} 
		}); 
		thread.start(); 
	} 

	private Handler handler = new Handler() { 
		public void handleMessage(Message msg) { 

			Context context = getApplicationContext();
			int duration = Toast.LENGTH_LONG;
			showNotification(context);
			Util.ToastMessage(context, c2dm_msg);
			Log.d("jose","msg:" + c2dm_msg);
			c2dm_msg = null;

		} 
	};
    private void showNotification(Context context) {
		// TODO Auto-generated method stub
    	NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		    PendingIntent pendingIntent = PendingIntent.getActivity(
		    context, 0,new Intent(context, IntroActivity.class), 0);
		    String ticker = c2dm_msg;
		    String title = context.getString(R.string.app_name);
		    String text = c2dm_msg;
		    
		    //?�면 켜짐 ?�무 체크
		    KeyguardManager km = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);
		    // Create Notification Object
//		    if(GetClassName(context) && !km.inKeyguardRestrictedInputMode()){}
//		    else if(km.inKeyguardRestrictedInputMode()){
		      
		     PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
		     PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK , "push");
		     wl.acquire();
		     Notification notification = new Notification(R.drawable.icon,ticker, System.currentTimeMillis());
		     notification.defaults |= (Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
		     notification.ledARGB=0xff00ff00;
		     notification.ledOnMS=300;
		     notification.ledOffMS=1000;
		     notification.flags = notification.flags |Notification.FLAG_AUTO_CANCEL | Notification.FLAG_SHOW_LIGHTS  ;
		     
		     notification.setLatestEventInfo(context,title, text, pendingIntent);
		     nm.notify(1234, notification);
//		     try {
//				nm.wait(5000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		     wl.release();
//		    }
	}

	public void showMsg(String msg, int option) 
	{ 
		Toast.makeText(this, msg, option).show(); 
	}
	@Override
	protected void onError(Context arg0, String errorId) {
		
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		Log.v("C2DM_REGISTRATION",">>>>>" + "Registration failed, should try again later." + "<<<<<");

		return super.onRecoverableError(context, errorId);
	}
	private void handleRegistration(Context context) {

			Log.d("jose","registration_id length="+registration_id.length());
			CUser.device_token=registration_id;
			System.out.println("registration_id complete!!");
			db_manager = new DBmanager(context,"UserInfo.db");
			db = db_manager.getWritableDatabase();
			String sql = "delete from device";
			db.execSQL(sql);
			sql = "insert into device (device_token) values('"+registration_id+"')";
			db.execSQL(sql);
			db.close();

	}
}
