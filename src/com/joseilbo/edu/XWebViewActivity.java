package com.joseilbo.edu;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.cookie.Cookie;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

import com.joseilbo.edu.adapter.HttpHelper;
import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Util;

public class XWebViewActivity extends XActivity {
	Cursor cursor;
	String uid, _url, title;
	WebView mWebView;
	private final Handler handler = new Handler();
	ImageButton btn_write, btn_help, btn_list;
	AlertDialog alert;
	private Map<String, Object> map = new HashMap<String, Object>();
	protected static WaitDialog progressDialog;
	XWebViewActivity self = null;
	String menu;
	CookieManager cookieManager;
	HttpHelper hh = new HttpHelper();
	boolean isRefresh = true;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self = this;
		Intent i = getIntent();
		View viewToLoad = LayoutInflater.from(this.getParent()).inflate(R.layout.titlewebview, null);
		this.setContentView(viewToLoad);
		setTitleButton((ImageButton) findViewById(R.id.leftImageButton), R.drawable.btn_prev);
		_url = Constants.baseUrl + i.getStringExtra("url");
		title = i.getStringExtra("title");
		isRefresh = i.getBooleanExtra("isRefresh", true);
		setTitle(title);
		CookieSyncManager.createInstance(this);
		cookieManager = CookieManager.getInstance();

		initWebview();
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		onBackPressed();
	}

	public void initWebview() {
		cookieManager = CookieManager.getInstance();
		List<Cookie> cookies = hh.getCookies();
		Cookie sessionInfo = null;

		for (Cookie cookie : cookies) {
			sessionInfo = cookie;
			String cookieString = sessionInfo.getName() + "="
					+ sessionInfo.getValue() + "; path="
					+ sessionInfo.getPath() + "; domain="
					+ sessionInfo.getDomain();
			Util.debug("s" + cookieString);
			cookieManager.setCookie(sessionInfo.getDomain(), cookieString);

			CookieSyncManager.getInstance().sync();
		}

		CookieSyncManager.getInstance().startSync();
		loadWebView(_url);
	}

	public void putAll(Map<? extends String, ? extends Object> arg0) {
		map.putAll(arg0);
	}

	private void loadWebView(String url) {
		mWebView = (WebView) findViewById(R.id.webview);
		mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.addJavascriptInterface(new AndroidBridge(), "android");
		mWebView.setHorizontalScrollBarEnabled(true); // 세로 scroll 제거
		mWebView.setVerticalScrollBarEnabled(false); // 가로 scroll 제거
		mWebView.getSettings().setBuiltInZoomControls(false);
		mWebView.setWebViewClient(new webViewClient()); 
		mWebView.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onJsConfirm(WebView view, String url,
					String message, final android.webkit.JsResult result) {
				new AlertDialog.Builder(getParent())
						.setTitle(getText(R.string.app_name))
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										result.confirm();
									}
								})
						.setNegativeButton(android.R.string.cancel,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										result.cancel();
									}
								}).create().show();

				return true;
			};

			public boolean onJsAlert(WebView view, String url, final String message, JsResult result) {
				final JsResult r = result;
				if (null == getParent())
					return false;

				new AlertDialog.Builder(getParent())
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										if (message.contains("로그인") || message.contains("Session")) {
											CUser.mno = "";
											cookieManager = CookieManager.getInstance();
											cookieManager.removeAllCookie();
											hh.clearCookie();
											mWebView.stopLoading();
											logout();
										}
										r.confirm();
									}
								}).setCancelable(false).create().show();
				return true;
			}

			public void onProgressChanged(WebView view, int newProgress) {
			}
		});
		
		mWebView.setDownloadListener(new DownloadListener() {
			public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(url));
				startActivity(intent);
			}
		});

		mWebView.loadUrl(url);
	}

	private class webViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.startsWith("tel:")) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(intent);
			} else {
				_url = url;
				view.loadUrl(url);
			}
			
			return true;
		}

		public void onLoadResource(WebView view, String url) {
			try {
				if (progressDialog == null) {
					progressDialog = WaitDialog.show(getParent(), "", "", true, true, null);
				}
			} catch (Exception e) {
			}
			(new Handler()).postDelayed(new Runnable() {
				public void run() {
					try {
						if (progressDialog != null && progressDialog.isShowing()) {
							progressDialog.dismiss();
							progressDialog = null;
						}
					} catch (Exception e) {
					}
				}
			}, 2000); 
		}

		public void onPageFinished(WebView view, String url) {
			try {
				if (progressDialog != null && progressDialog.isShowing()) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (url.endsWith(".mp4")) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					Uri uri = Uri.parse(url);
					i.setDataAndType(uri, "video/mp4");
					startActivity(i);
				}
			} catch (Exception e) {
			}
			CookieSyncManager.getInstance().sync();
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Util.ToastMessage(getParent(), getResources().getString(R.string.api_http_alert));
		}
	}

	public void onStart() {
		super.onStart();
		CookieSyncManager.createInstance(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		CookieSyncManager.getInstance().stopSync();
	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();
		
		if (isRefresh)
			mWebView.reload();

		super.onResume();
	}

	private class AndroidBridge {
		@JavascriptInterface
		public void callAndroid(final String arg) {
			handler.post(new Runnable() {
				public void run() {
					if (arg.equals("home")) {
						finish();
						((Activity) self).startActivity(new Intent(self, MainActivity.class));
					}
				}
			});
		}

		@JavascriptInterface
		public void go_study(final String classkey, final String title, final String gisuYear, final String gisuNum) {
			handler.post(new Runnable() {
				public void run() {
					if (CUser.usermode == 1)
						goActivity(
								new Intent(self, MyCurriculumActivity.class)
										.putExtra("classkey", classkey)
										.putExtra("gisuYear", gisuYear)
										.putExtra("gisuNum", gisuNum)
										.putExtra("title", title), false);
					else {
						Util.alertOk(getParent(), R.string.login_info);
					}
				}
			});
		}

		@JavascriptInterface
		public void write_ok() { // must be final
			handler.post(new Runnable() {
				public void run() {
					onBackPressed();
				}
			});
		}
	}

	public void onBackPressed() {
		XActivityGroup parent = ((XActivityGroup) getParent());
		parent.group.back();
		return;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
