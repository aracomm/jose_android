package com.joseilbo.edu;

import java.util.HashMap;
import java.util.Map;

import com.joseilbo.edu.R;
import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.Util;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

public class WebViewActivity extends XActivity {
	Cursor cursor;
	String uid, _url, title;
	WebView mWebView;
	private final Handler handler = new Handler();
	ImageButton btn_write, btn_help, btn_list;
	AlertDialog alert;
	private Map<String, Object> map = new HashMap<String, Object>();
	protected static WaitDialog progressDialog;
	WebViewActivity self = null;
	String menu;
	CookieManager cookieManager;

	boolean isRefresh = true;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wview);
		self = this;
		Intent i = getIntent();
		_url = i.getStringExtra("url");

		loadWebView(_url);
	}

	@Override
	public void onClick(View view) {
		onBackPressed();
	}

	private void loadWebView(String url) {
		mWebView = (WebView) findViewById(R.id.webview);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		mWebView.getSettings().setLoadsImagesAutomatically(true);
		mWebView.getSettings().setSupportZoom(true);
		mWebView.getSettings().setLoadWithOverviewMode(true);
		mWebView.getSettings().setUseWideViewPort(true);
		mWebView.getSettings().setDatabaseEnabled(true);
		mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.getSettings().setSupportMultipleWindows(true);
		mWebView.setWebViewClient(new webViewClient());
		mWebView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int newProgress) {
			}
		});

		mWebView.loadUrl(url);
	}

	private class webViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.startsWith("tel:")) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(intent);
			} else {
				_url = url;
				view.loadUrl(url);
			}
			return true;
		}

		public void onLoadResource(WebView view, String url) {
			try {
				if (progressDialog == null) {
					progressDialog = WaitDialog.show(getParent(), "", "", true, true, null);
				}
			} catch (Exception e) {
			}
			(new Handler()).postDelayed(new Runnable() {
				public void run() {
					try {
						if (progressDialog != null
								&& progressDialog.isShowing()) {
							progressDialog.dismiss();
							progressDialog = null;
						}
					} catch (Exception e) {
					}
				}
			}, 2000);
		}

		public void onPageFinished(WebView view, String url) {
			try {
				if (progressDialog != null && progressDialog.isShowing()) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (url.endsWith(".mp4")) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					Uri uri = Uri.parse(url);
					i.setDataAndType(uri, "video/mp4");
					startActivity(i);
				}
			} catch (Exception e) {
			}
			CookieSyncManager.getInstance().sync();
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Util.ToastMessage(getParent(), getResources().getString(R.string.api_http_alert));
		}
	}

	public void onStart() {
		super.onStart();
		CookieSyncManager.createInstance(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		CookieSyncManager.getInstance().stopSync();
	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();

		super.onResume();
	}

	public void onBackPressed() {
		finish();
		return;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
