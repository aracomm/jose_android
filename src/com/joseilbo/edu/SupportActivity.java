package com.joseilbo.edu;

import java.util.HashMap;
import java.util.Map;

import com.joseilbo.edu.R;
import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.Constants;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;

public class SupportActivity extends XActivity {
	Cursor cursor;
	String uid, _url;
	WebView mWebView;
	ImageButton btn_write, btn_help, btn_list;
	AlertDialog alert;
	private Map<String, Object> map = new HashMap<String, Object>();
	protected static WaitDialog dialog;
	SupportActivity self = null;
	String menu;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self = this;
		setContentView(R.layout.support);

		setTitle("학습지원");
		setTitleButton((ImageButton) findViewById(R.id.rightImageButton), R.drawable.btn_close);

		//event assign
		((ImageButton) findViewById(R.id.btn_counsel)).setOnClickListener(self);
		((ImageButton) findViewById(R.id.btn_call)).setOnClickListener(self);
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.btn_counsel:
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra("url", Constants.KakaoInfoUrl).putExtra("title", "1:1 상담"));
			break;
			
		case R.id.btn_call:
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.tel_guestcenter)));
			break;
			
		case R.id.leftImageButton:
			onBackPressed();
			break;
			
		case R.id.rightImageButton:
			finish();
			break;
		}
	}

	public void putAll(Map<? extends String, ? extends Object> arg0) {
		map.putAll(arg0);
	}

	public void onStart() {
		super.onStart();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();

		super.onResume();
	}

	@Override
	public void onBackPressed() {
		finish();
		return;
	}
}
