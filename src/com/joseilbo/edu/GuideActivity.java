package com.joseilbo.edu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;

import com.joseilbo.edu.adapter.GuideListAdapter;
import com.joseilbo.edu.adapter.OnResponseListener;
import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.container.GuideItem;

public class GuideActivity extends XActivity implements OnResponseListener {
	Cursor cursor;
	String uid, _url;
	WebView mWebView;
	ImageButton btn_write, btn_help, btn_list;
	AlertDialog alert;
	private Map<String, Object> map = new HashMap<String, Object>();
	protected static WaitDialog dialog;
	GuideActivity self = null;
	String menu;
	OnResponseListener callback;
	Context context;
	ExpandableListView list;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self = this;
		setContentView(R.layout.guide);
		context = getParent();
		callback = this;
		setTitleButton((ImageButton) findViewById(R.id.leftImageButton), R.drawable.btn_home_on2);
		setTitle("학습지원");

		((Button) findViewById(R.id.btn_1vs1_counsel)).setOnClickListener(self);
		((Button) findViewById(R.id.btn_phone_counsel)).setOnClickListener(self);

		list = (ExpandableListView) findViewById(R.id.list);
		initialize();
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();
		switch (id) {
		case R.id.btn_1vs1_counsel:
			startActivity(new Intent(self, PopWebViewActivity.class).putExtra("url", Constants.KakaoInfoUrl).putExtra("title", "1:1 상담 안내"));
			break;
			
		case R.id.btn_phone_counsel:
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.tel_guestcenter)));
			break;
			
		case R.id.leftImageButton:
			onBackPressed();
			break;
		}
	}

	public void initialize() {
		ArrayList<GuideItem> glist = new ArrayList<GuideItem>();

		String str = "1. 홈 화면에서 [학습중인과정]을 클릭하거나 하단 메뉴바에서 내강의실을 클릭하면 내강의실에 입장합니다.\r\n\r\n";
		str += "2. 학습중인과정을 확인 후 해당 과정으로 입장하실 수 있습니다.\r\n\r\n";
		str += "3. 입장한 과정화면에서는 현재까지의 진도율확인, 공지사항, 과제확인 등을 확인할 수 있습니다.";
		GuideItem item1 = new GuideItem();
		ArrayList<String> cclist1 = new ArrayList<String>();
		cclist1.add(str);
		item1.setGuide("강의실 입장", cclist1);
		glist.add(item1);

		str = "1. 내강의실의 학습중인 과정에서 진도율을 확인하려는 과정의 [입장하기] 버튼을 클릭합니다.\r\n\r\n";
		str += "2. 해당 과정에서는 [학습하기] 버튼 클릭을 클릭하면 학습목록이 나옵니다.\r\n\r\n";
		str += "3. 학습목록에서 출석기간 중인 목록을 클릭하면 진도율을 볼 수 있습니다.\r\n";
		GuideItem item2 = new GuideItem();
		ArrayList<String> cclist2 = new ArrayList<String>();
		cclist2.add(str);
		item2.setGuide("학습하기", cclist2);
		glist.add(item2);

		str = "1. 첨부파일 확인 및 과제 작성은 PC에서 이용해 주시기 바랍니다.\r\n\r\n";
		str += "2. 회원가입 및 아이디/비밀번호 찾기는 PC에서 이용해 주시기 바랍니다.\r\n";
		GuideItem item3 = new GuideItem();
		ArrayList<String> cclist3 = new ArrayList<String>();
		cclist3.add(str);
		item3.setGuide("유의사항", cclist3);
		glist.add(item3);

		str = "교육문의 : 02-3146-8230\r\n\r\n";
		str += "평일 9:00 ~ 19:00 / 토요일 9:00 ~ 14:00\r\n";
		GuideItem item4 = new GuideItem();
		ArrayList<String> cclist4 = new ArrayList<String>();
		cclist4.add(str);
		item4.setGuide("상담 전화번호, 시간 안내", cclist4);
		glist.add(item4);

		str = "1. 아이디 당 로그인은 한 기기에서만 유효합니다.\r\n\r\n";
		str += "2. 기기변경 등으로 로그인이 안될 경우 (02-3146-8230)로 문의해 주시기 바랍니다.\r\n";
		GuideItem item5 = new GuideItem();
		ArrayList<String> cclist5 = new ArrayList<String>();
		cclist5.add(str);
		item5.setGuide("1인 1단말기 안내", cclist5);
		glist.add(item5);

		GuideListAdapter gadapter = new GuideListAdapter(this, list, glist);

		list.setAdapter(gadapter);
	}

	public void putAll(Map<? extends String, ? extends Object> arg0) {
		map.putAll(arg0);
	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();
		
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		finish();
		return;
	}
}
