package com.joseilbo.edu;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.CUser;
import com.joseilbo.edu.common.Constants;
import com.joseilbo.edu.common.Preference;
import com.joseilbo.edu.common.Util;

public class PopWebViewActivity extends XActivity {
	Cursor cursor;
	String uid, _url, _title;
	WebView mWebView;
	private final Handler handler = new Handler();
	ImageButton btn_write, btn_help, btn_list;
	AlertDialog alert;
	private Map<String, Object> map = new HashMap<String, Object>();
	protected static WaitDialog progressDialog;
	PopWebViewActivity self = null;
	String menu;
	private String payUrl = "";
	private boolean isOnline = true;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self = this;
		Intent i = getIntent();
		setContentView(R.layout.titlewebview2);

		setTitleButton((ImageButton) findViewById(R.id.rightImageButton), R.drawable.btn_close);
		
		_url = i.getStringExtra("url");
		_title = i.getStringExtra("title");
		
		if(_title != null)
			setTitle(_title);
		else
			setTitle("상세보기");

		initWebview();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.rightImageButton:
			finish();
			break;
		}
		
		finish();
	}

	public void initWebview() {
		LinearLayout rootLayout = (LinearLayout) findViewById(R.id.root_layout);
		mWebView = new WebView(self);
		rootLayout.addView(mWebView);
		
		loadWebView(_url);
	}

	public void putAll(Map<? extends String, ? extends Object> arg0) {
		map.putAll(arg0);
	}

	private void loadWebView(String url) {
		mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.addJavascriptInterface(new AndroidBridge(), "android");
		mWebView.setHorizontalScrollBarEnabled(true); // 세로 scroll 제거
		mWebView.setVerticalScrollBarEnabled(false); // 가로 scroll 제거
		//mWebView.getSettings().setBuiltInZoomControls(true);
		//mWebView.getSettings().setUseWideViewPort(true);
		//mWebView.getSettings().setLoadWithOverviewMode(true);
		mWebView.setWebViewClient(new webViewClient());
		mWebView.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onJsConfirm(WebView view, String url, String message, final android.webkit.JsResult result) {
				new AlertDialog.Builder(self)
						.setTitle(getText(R.string.app_name))
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										result.confirm();
									}
								})
						.setNegativeButton(android.R.string.cancel,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										result.cancel();
									}
								}).create().show();

				return true;
			};

			public boolean onJsAlert(WebView view, String url, final String message, JsResult result) {
				final JsResult r = result;
				if (null == self)
					return false;

				new AlertDialog.Builder(self)
						.setMessage(message)
						.setPositiveButton(android.R.string.ok,
								new AlertDialog.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										if (message.contains("로그인") || message.contains("Session")) {
											CUser.mno = "";
											mWebView.stopLoading();
											logout();
										}
										r.confirm();
									}
								}).setCancelable(false).create().show();
				return true;
			}

			public void onProgressChanged(WebView view, int newProgress) {
			}
		});
		
		mWebView.setDownloadListener(new DownloadListener() {
			public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
				startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url)));
			}
		});

		mWebView.loadUrl(url);
	}

	private class webViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Util.debug(url);
			
			if (url.startsWith("http:") || url.startsWith("https:")) {
				if (url.contains(Constants.baseUrl) && !url.contains("mem_id") && !url.contains(CUser.userid)) {
					if(url.contains("?"))
						url += "&mem_id=" + CUser.userid ;
					else 
						url += "?mem_id=" + CUser.userid ;
				}
				
				_url = url;
				view.loadUrl(url);
				return true;
			}
			
			
			if (url.startsWith("tel:")) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(intent);
				return false;
			} else if(url.startsWith("market:")) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
				startActivity(intent);
				return false;
			} else if(url.startsWith("intent:")) {
				//ILK 용
	            if( url.contains( "com.lotte.lottesmartpay" ) )
	            {
	                try{
	                    startActivity( Intent.parseUri(url, Intent.URI_INTENT_SCHEME) );
	                } catch ( URISyntaxException        e ) {
	                    Log.d( "WebPageActivity", "[PayDemoActivity] URISyntaxException=[" + e.getMessage() + "]" );
	                    return false;
	                } catch ( ActivityNotFoundException e ) {
	                    Log.d( "WebPageActivity", "[PayDemoActivity] ActivityNotFoundException=[" + e.getMessage() + "]" );
	                    return false;
	                }
	            }
	            //ILK 용
	            else if ( url.contains( "com.ahnlab.v3mobileplus" ) )
	            {
	                try {
	                    view.getContext().startActivity(Intent.parseUri(url, 0));
	                } catch ( URISyntaxException        e ) {
	                    Log.d( "WebPageActivity", "[PayDemoActivity] URISyntaxException=[" + e.getMessage() + "]" );
	                    return false;
	                } catch ( ActivityNotFoundException e ) {
	                    Log.d( "WebPageActivity", "[PayDemoActivity] ActivityNotFoundException=[" + e.getMessage() + "]" );
	                    return false;
	                }
	            }
	            //폴라리스 용
	            else
	            {
	                Intent intent = null;
	                
	                try {
	                    intent = Intent.parseUri( url, Intent.URI_INTENT_SCHEME );
	                } catch ( URISyntaxException ex ) {
	                    return false;
	                }
	                
	                // 앱설치 체크를 합니다.
	                if ( getPackageManager().resolveActivity( intent, 0 ) == null ) {
	                    String packagename = intent.getPackage();
	                    
	                    if ( packagename != null ) {
	                        startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "market://search?q=pname:" + packagename ) ) );
	                        
	                        return true;
	                    }
	                }
	                
	                intent = new Intent( Intent.ACTION_VIEW, Uri.parse( intent.getDataString() ) );
	                
	                try{
	                    startActivity( intent );
	                }catch( ActivityNotFoundException e ) {
	                    Log.d( "WebPageActivity", "[PayDemoActivity] ActivityNotFoundException=[" + e.getMessage() + "]" );
	                    return false;
	                }
	            }
			}
			/*
			else if(url.startsWith("intent:")) {
				try {
					Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
					if (getPackageManager().resolveActivity(intent, 0) == null) {
						String packageName = intent.getPackage();
						startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "market://search?q=pname:" + packageName ) ) );
						return true;
					}
					
					
					startActivity(intent);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return true;
			} else {
				try {
					Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
					if (getPackageManager().resolveActivity(intent, 0) == null) {
						String packageName = intent.getPackage();
						startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "market://search?q=pname:" + packageName ) ) );
						return true;
					}
					
					startActivity(intent);
				} catch (Exception e) {
					e.printStackTrace();
				}
			
				return true;
			}
			*/

			return true;
		}

		public void onLoadResource(WebView view, String url) {
			try {
				if (progressDialog == null) {
					progressDialog = WaitDialog.show(self, "", "", true, true, null);
				}
			} catch (Exception e) {
			}
			(new Handler()).postDelayed(new Runnable() {
				public void run() {
					try {
						if (progressDialog != null && progressDialog.isShowing()) {
							progressDialog.dismiss();
							progressDialog = null;
						}
					} catch (Exception e) {
					}
				}
			}, 2000);
		}

		public void onPageFinished(WebView view, String url) {
			try {
				if (progressDialog != null && progressDialog.isShowing()) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (url.endsWith(".mp4")) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					Uri uri = Uri.parse(url);
					i.setDataAndType(uri, "video/mp4");
					startActivity(i);
				}
			} catch (Exception e) {
			}
			CookieSyncManager.getInstance().sync();
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Util.ToastMessage(self, getResources().getString(R.string.api_http_alert));
		}
	}

	public void onStart() {
		super.onStart();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		if (alert != null)
			alert.dismiss();
		
		super.onResume();
	}

	private class AndroidBridge {
		@JavascriptInterface
		public void callAndroid(final String arg) {
			handler.post(new Runnable() {
				public void run() {
					if (arg.equals("home")) {
						finish();
					}
				}
			});
		}
		
		@JavascriptInterface
		public void goLogin(final String url) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					payUrl = Constants.MobilePaymentUrl + url;
					/*
					Intent i = new Intent(self, LoginActivity.class);
					i.putExtra(Constants.REQUEST_FIELD, Constants.REQUEST_ONLINE_ACTIVITY);
					*/
					if (CUser.mno.equals("")) {
						Intent i = new Intent(self, LoginActivity.class);
						if (isOnline == true) {
							i.putExtra(Constants.REQUEST_FIELD, Constants.REQUEST_SEARCH_ONLINE_ACTIVITY);
							startActivityForResult(i, Constants.REQUEST_SEARCH_ONLINE_ACTIVITY);
						} else {
							i.putExtra(Constants.REQUEST_FIELD, Constants.REQUEST_SEARCH_OFFLINE_ACTIVITY);
							startActivityForResult(i, Constants.REQUEST_SEARCH_OFFLINE_ACTIVITY);
						}
					} else {
						Util.alertYesNo(self, R.string.app_name, R.string.app_logout, new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										finish();
										(new Handler()).post(new Runnable() {
											public void run() {
												CUser.username = "";
												CUser.userpw = "";
												CUser.mno = "";
												CUser.userid = "";
												logout();
											}
										});
									}
								}, null);
					}
				}
			});
		}
		
		@JavascriptInterface
		public void goPreview(final String url) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					startActivity(new Intent(self,Html5WebActivity.class).putExtra("url", url));
				}
			});
		}

		@JavascriptInterface
		public void telCounsel() { 
			handler.post(new Runnable() {
				public void run() {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.tel_guestcenter)));
				}
			});
		}
		
		@JavascriptInterface
		public void goOnLinePay(final String url) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					String moveUrl = Constants.MobilePaymentUrl + url;
					if (!moveUrl.contains("mem_id")) {
						moveUrl += "&mem_id="+CUser.userid;;
					}
					Util.debug("goOnLinePay : " + moveUrl);

					loadWebView(moveUrl);
				}
			});
		}

		@JavascriptInterface
		public void goOffLinePay(final String url) {
			handler.post(new Runnable() {
				public void run() {
					String moveUrl = Constants.MobilePaymentUrl + url;
					if (!moveUrl.contains("mem_id")) {
						moveUrl += "&mem_id="+CUser.userid;;
					}
					Util.debug("goOffLinePay : " + moveUrl);
					
					loadWebView(moveUrl);
				}
			});
		}
		
		@JavascriptInterface
		public void goHome(final String arg) {
			handler.post(new Runnable() {
				public void run() {
					finish();
				}
			});
		}
	}

	@Override
	public void onBackPressed() {
		if(mWebView.canGoBack()) {
			mWebView.goBack();
		} else {
			finish();
		}
		
		return;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		payUrl += "&mem_id="+CUser.userid;
		
		Util.debug("PopWebViewActivity.java - onActivityResult");
		Util.debug(payUrl);
		
		Preference pref = new Preference(this, Util.MT_PREFS);
		pref.write("direct_url", payUrl);
		
		switch(requestCode) {
		case Constants.REQUEST_SEARCH_ONLINE_ACTIVITY:
			//startActivity(new Intent(self, RootActivity.class).putExtra("menu",1)); // jose - OnlineClassActivity
			Util.debug("REQUEST_SEARCH_ONLINE_ACTIVITY");
			break;
		case Constants.REQUEST_SEARCH_OFFLINE_ACTIVITY:
			//startActivity(new Intent(self, RootActivity.class).putExtra("menu", 2)); // jose - OfflineClassActivity
			Util.debug("REQUEST_SEARCH_OFFLINE_ACTIVITY");
			break;
		}
//		mWebView.loadUrl(payUrl);
		finish();
	}
}
