package com.joseilbo.edu;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.impl.client.DefaultHttpClient;

import com.joseilbo.edu.R;
import com.joseilbo.edu.adapter.HttpHelper;
import com.joseilbo.edu.adapter.WaitDialog;
import com.joseilbo.edu.common.Constants;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.widget.ImageButton;

public class CallCenterActivity extends XActivity {
	Cursor cursor;
	String uid,_url;
	WebView mWebView;
	private final Handler handler = new Handler();
	ImageButton btn_write,btn_help,btn_list;
	AlertDialog alert;
	private Map<String, Object> map = new HashMap<String, Object>();
	protected static WaitDialog dialog;
	private ProgressDialog progressDialog;
	CallCenterActivity self=null;
	String menu;
	CookieManager cookieManager;
	HttpHelper hh=new HttpHelper();
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		self=this;
		Intent i=getIntent();
		setContentView(R.layout.callcenter);

		setTitleButton((ImageButton)findViewById(R.id.leftImageButton),R.drawable.btn_prev);
		
		setTitle("전화상담");

		CookieSyncManager.createInstance(this);
		cookieManager=CookieManager.getInstance();
		((ImageButton)findViewById(R.id.btn_guestcenter)).setOnClickListener(self);
		((ImageButton)findViewById(R.id.btn_studycenter)).setOnClickListener(self);
		((ImageButton)findViewById(R.id.btn_techcenter)).setOnClickListener(self);

		initialize();



	}
	@Override
	public void onClick(View view) {
		int id=view.getId();
		String tel="";
		switch(id)
		{
		case R.id.btn_techcenter:
			tel=Constants.tel_techcenter;
			break;
		case R.id.btn_guestcenter:
			tel=Constants.tel_guestcenter;

			break;
		case R.id.btn_studycenter:
			tel=Constants.tel_studycenter;
			break;
		}
		if(tel.length()>0)
		{
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tel));
		startActivity( intent );
		}
		
		if(id == R.id.leftImageButton)
			onBackPressed();
	}

	
	public void initialize() {
		// TODO Auto-generated method stub
		DefaultHttpClient client = new DefaultHttpClient();

	}



	public void putAll(Map<? extends String, ? extends Object> arg0) {
		map.putAll(arg0);
	}


	public void onStart() { 

		super.onStart(); 

		CookieSyncManager.createInstance(this);

	}   

	@Override

	public void onPause(){

		super.onPause();

		CookieSyncManager.getInstance().stopSync();

	}
	@Override
	public void onBackPressed() 
	{
						XActivityGroup parent = ((XActivityGroup)getParent());
						parent.group.back();
		return;
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		if(alert!=null)
			alert.dismiss();
		super.onResume();

		// TODO Auto-generated method stub

		super.onResume();
	}

}
