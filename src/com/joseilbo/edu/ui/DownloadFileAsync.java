package com.joseilbo.edu.ui;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

public class DownloadFileAsync extends AsyncTask<String, String, String> {

	private DownloadFileAsyncAction mAction;
	public interface DownloadFileAsyncAction {
		void onDownloadEnd(String fileKey, String filePath);
	}
	public void setOnDownloadFileAsyncAction(DownloadFileAsyncAction action) {
		mAction = action;
	}
	
	private ProgressDialog mDownloadDialog;
	private Context mContext;
	
	private String fileKey, fileName, filePath;
	
	public DownloadFileAsync(Context context) {
		this.mContext = context;
	}
	
	@Override
	protected void onPreExecute() {
		this.mDownloadDialog = new ProgressDialog(this.mContext);
		this.mDownloadDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		this.mDownloadDialog.setMessage("Download");
		this.mDownloadDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "다운로드 취소", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				cancel(true);
			}
		});
		this.mDownloadDialog.setCancelable(false);
		this.mDownloadDialog.show();
		
		super.onPreExecute();
	}
	
	@Override
	protected String doInBackground(String... params) {
		int count = 0;
		
		try {
			
			Thread.sleep(100);
			URL url = new URL(params[0].toString());
			URLConnection conexion = url.openConnection();
			conexion.connect();
			
			fileKey = params[1].toString();
			fileName = params[2].toString();
			filePath = mContext.getFilesDir().getPath() + fileName;

			int lenghtOfFile = conexion.getContentLength();
			InputStream input = new BufferedInputStream(url.openStream());			
			OutputStream output = new FileOutputStream(filePath);

			byte data[] = new byte[1024];
			long total = 0;

			while ((count = input.read(data)) != -1) {
				if (isCancelled()) {
					output.flush();
					output.close();
					input.close();
					return null;
				}
				
				total += count;
				publishProgress("progress",""+(int) ((total * 100) / lenghtOfFile) , "Download");
				output.write(data, 0, count);
			}

			output.flush();
			output.close();
			input.close();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	@Override
	protected void onProgressUpdate(String... progress) {
		if (progress[0].equals("progress")) {
			this.mDownloadDialog.setProgress(Integer.parseInt(progress[1]));
			this.mDownloadDialog.setMessage(progress[2]);
		} else if (progress[0].equals("max")) {
			this.mDownloadDialog.setMax(Integer.parseInt(progress[1]));
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onPostExecute(String unused) {
		this.mDownloadDialog.dismiss();
		mAction.onDownloadEnd(fileKey, filePath);
	}

}
